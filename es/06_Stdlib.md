# Librería estándar

La librería estándar de un lenguaje es el conjunto de funcionalidades y
utilidades que se entregan con éste. Su objetivo suele ser el de compartir
código pre-diseñado para realizar tareas comunes de la mejor manera posible.

La librería estándar de C++, al igual que el resto del lenguaje, no está
definida de forma oficial, sino que se describe su comportamiento en el
estándar, que en ocasiones aporta sugerencias de implementación. Cada
compilador suele aportar su propia implementación de la librería estándar o una
implementación de terceros compatible.

La librería estándar de C++ aporta gran variedad de utilidades y
funcionalidades añadidas al lenguaje. La mejor recomendación que puede hacerse
respecto al uso de la librería estándar es que debe ser el código que mayor
prioridad debe tener a la hora de añadirse al programa, por delante del propio,
a no ser que se tenga una muy buena razón para no hacerlo.

Es difícil que proyectos independientes consigan un código más correcto que el
de la librería estándar, es por eso que debe seleccionarse antes que cualquier
otro. En ocasiones puede ser interesante evitar usar la librería estándar
(requerimientos de rendimiento muy estrictos, tamaño del programa compilado,
etc.) pero no son demasiado frecuentes y normalmente se resuelven con mayor
facilidad cambiando el código creado de forma manual o el diseño del programa.

La librería estándar de C++ está formada por diferentes cabeceras que en
general pueden agruparse por familias o por funcionalidades. Es interesante
saber que las cabeceras no siempre implican la existencia de una librería
asociada. Parte de los archivos de cabecera son simplemente eso, cabeceras, y
la funcionalidad se aporta directamente por el compilador, otros no hacen
referencia a una librería concreta, sino a una parte de una librería más
grande.

La librería estándar de C, que también se incluye en su mayoría en C++, es un
caso claro de esta organización. La mayor parte del código de la librería
estándar se almacena normalmente en la librería `libc`, mientras que otras
secciones se almacenan en sub-librerías como `libm`, para el soporte
matemático, etc. Esto reduce el tamaño de los programas compilados, ya que
permite incluir sólo las librerías necesarias.

## Implementaciones *freestanding* y *hosted*

Las implementaciones *freestanding* no están obligadas a incluir la librería
estándar completa por razones obvias. Pero sí que deben incluir un conjunto
mínimo de cabeceras.

A la hora de migrar código de una implementación *hosted* a una implementación
*freestanding* éste debe ser uno de los puntos que se compruebe, junto con
muchos otros dependientes de la plataforma con la que se va a trabajar.

## Organización de de la librería estándar

Las cabeceras de la librería estándar de C++ pueden agruparse por
funcionalidades.

### Soporte del lenguaje

La sección de soporte del lenguaje está muy estrechamente conectada con el
comportamiento del lenguaje y aporta funcionalidades relacionadas con éste.
Está separada en varias secciones.

La sección sobre tipos aporta tipos básicos adicionales, como `std::size_t`,
soporte para RTTI y los rasgos de los tipos analizados en la sección sobre
plantillas. La sección de control de memoria aporta los *smart pointers* y
funcionalidades de control de memoria estilo C. Las listas inicializadoras
también forman parte de este conjunto de herramientas bajo la cabecera
`<initializer_list>`.

Como se observa, todas las secciones de este bloque aportan soporte a
mecanismos primordiales del lenguaje, entregados a modo de cabeceras. Es un
apartado con varias sub-secciones con las que es interesante familiarizarse.

### Librería numérica

La librería numérica agrupa funciones matemáticas y tipos relacionados con
ellas. Desde una librería matemática básica a un conjunto de constantes y
funciones especiales han sido añadidas recientemente, así que hoy en día se
trata de una librería muy completa.


### Lectura y escritura

La sección de lectura y escritura (*input/output*) aporta funcionalidades
variadas para acceder diversas fuentes a modo de *stream* y su versión
equivalente estilo C.

Los *stream* son un concepto de programación, probablemente conocido por las
plataformas de *streaming* de audio y vídeo, en las que se emite a medida que
el vídeo o el audio van avanzando y de modo que pueden reproducirse según se
reciben. *Stream* en inglés significa «arroyo», «torrente» o, quizás la
traducción más adecuada para este contexto: «chorro».

El ejemplo más sencillo para entender la diferencia de uso entre la versión de
tipo *stream* de la versión estilo C es la de la lectura y escritura de un
archivo. El estilo C, debido a una herencia antigua, funciona a nivel lógico
como una cinta de cassette o la aguja de un disco de vinilo (los discos duros
tienen un funcionamiento similar), con funciones como `rewind` o `fseek` que
permiten rebobinar y cambiar la posición de lectura de la cinta o el disco y
funciones que permiten la escritura y lectura como `fwrite` y `fread`.

Los *stream* aportan un funcionamiento conceptualmente distinto. Interpretan el
flujo, nunca mejor dicho, de información como operaciones de inserción y
extracción, aplicables mediante los operadores `<<` y `>>`. Cada vía de lectura
y escritura se comporta como una tubería con las dos operaciones que puede
interpretarse de modo crudo, como bytes, o de modo formateado, ya sea mediante
strings u otros métodos. Las funciones `seekp` y `seekg` (`p` de *put*,
«poner» o «depositar») y (`g` de *get*, «obtener») permiten, en caso de que sea
posible, saltar a diferentes posiciones del *stream* de escritura o lectura,
respectivamente.

A nivel práctico, los dos estilos son similares, pero el segundo puede ser más
elegante porque trata cada inserción y lectura con su propia operación del
operador `>>` o `<<`, lo que facilita la sobrecarga y que los objetos puedan
serializarse[^serialization] para su inserción en el flujo de forma correcta.

Este método de *streams* puede aplicarse sobre cualquier sistema de lectura y
escritura, como una conexión de red, el acceso a un sistema de archivos, acceso
a bloques de memoria en crudo, acceso a datos de sensores u otros.

[^serialization]: La *serialización* es el proceso de convertir los objetos a
  un formato que pueda escribirse en un archivo o transmitirse.

No es mal momento para recordar que esta sección de la librería estándar ya se
ha utilizado durante el documento. Rescatando el primerísimo ejemplo que se ha
propuesto:

``` cpp
#include <iostream>

int main(int argc, char* argv[]) {
    using namespace std;
    cout << "Hello World!\n";
}
```

La cabecera `<iostream>` cuya `i` significa *input* y `o` significa *output*
aporta la funcionalidad para tratar con *streams* de lectura y escritura. En
este caso se aplica para escribir sobre `cout`, un *stream* creado
automáticamente, que resulta en la escritura en la pantalla de la terminal,
también conocida como salida estándar o `stdout` en entornos UNIX.

Del mismo modo, la librería de lectura y escritura puede usarse para leer desde
la entrada estándar, `stdin`, (el *buffer* de teclado de la terminal), mediante
la operación `>>` de `cin`:

``` cpp
cin >> variable
```

La misma estrategia puede usarse para infinidad de fuentes y destinos.

### Strings y caracteres

La sección sobre strings y caracteres aporta los strings avanzados de los que
ya se ha hecho uso durante el documento, `std::string`, disponible en
`<string>`, a modo de plantilla para que puedan construirse con el tipo de
caracter que interese. A parte de esto, aporta soporte para expresiones
regulares en `<regex>` y las típicas funciones de soporte de strings tipo C
como `strcpy` bajo la cabecera `<cstring>`.

### Diagnóstico

La sección de diagnóstico añade el concepto de excepción (`<exception>`) y un
conjunto de excepciones básicas (`<stdexception>`) de las que es posible crear
excepciones derivadas mediante la herencia y capacidades de manipulación de
errores del sistema.

### Localización

La localización sirve para representar diferencias culturales entre los
distintos pueblos: formas de representación de fechas, idiomas, alfabetos, etc.

### Contenedores

Los contenedores son clases que definen tipos agregados de diferentes
características: vectores (`<vector>`), arrays avanzados (`<array>`), maps
(`<map>`), sets (`<set>`), colas (`<queue>` y `<deque>`), etc. Todos ellos
existen en la librería estándar de C++, cada uno con sus pros y sus contras.

Hacen un uso avanzado de plantillas por lo que son flexibles y funcionan muy
bien emparejados con la sección de algoritmos.

### Algoritmos

Los algoritmos (`<algorithm>`) aportan algoritmos básicos aplicables a
cualquier secuencia. El uso de las plantillas en esta cabecera permite la
definición de estos algoritmos básicos de búsqueda, ordenación, copia, etc. de
forma genérica para que sean compatibles con distintos tipos de colección.

### Concurrencia

La concurrencia es probablemente el contexto más difícil de manejar en la
programación. Los programas multihilo y multiproceso corren el riesgo de
corromper su estado compartido si no se tratan con el cuidado que merecen. La
librería estándar de C++ provee de un conjunto de herramientas variadas para el
control de programas concurrentes.

En primer lugar, la cabecera `<atomic>` aporta operaciones y tipos atómicos, es
decir, que sus operaciones de escritura y lectura no pueden interrumpirse por
otros hilos o procesos, para facilitar la sincronización en entornos
concurrentes.

La cabecera `<thread>` aporta soporte para hilos (*thread*) y mecanismos de
exclusión mutua (*mutual exclusion* o, coloquialmente, *mutex*), mientras que
cabeceras adicionales aportan simplificaciones del uso de hilos mediante
herramientas como las *futures* o las promesas (*promise*).

Por otro lado, cabe mencionar el especificador `thread_local`, disponible desde
C++11, que facilita el uso de almacenamiento local en los hilos de modo que
puedan mantenerse aislados. A pesar de no haberse introducido en detalle
durante el documento, incluirlo en esta sección es casi una obligación ya que
quien desee aplicar la concurrencia acabará de un modo u otro necesitándolo.

La programación concurrente es toda un área de estudio que requiere de un
especial cuidado tanto para evitar interacciones descontroladas entre los hilos
y procesos como para que no se bloqueen mutuamente llegando a *deadlocks*. Es
tarea de quien programa, y no de la librería estándar, asegurar que todo
funciona correctamente, pero la librería estándar aporta lo necesario para que
pueda realizarse.

### Otras utilidades

Algunas cabeceras de la librería estándar son difíciles de catalogar como un
conjunto de funcionalidades. Se trata de cabeceras aisladas como
`<functional>`, que aporta objetos-función y soporte para funciones hash, o la
cabecera de iteradores `<iterator>`, entre otras. Todas ellas son útiles y
deberán estudiarse a medida que se necesiten.

Por otro lado, los estándares más recientes aportan funcionalidades útiles ya
mencionadas como la sección de *concepts* (a partir de C++20) u otras no
mencionadas como soporte para el sistema de archivos (desde C++17) añadido al
lenguaje tomando como referencia una librería externa extremadamente popular
llamada Boost[^boost].

Es necesario indicar que la distribución de la librería estándar propuesta en
este apartado no es más que una guía conceptual. Diferentes referencias y
documentos pueden organizar la librería estándar de formas diferentes.

[^boost]: La librería Boost es ampliamente conocida, hasta tal punto que
  históricamente se ha usado como una extensión de la librería estándar por
  infinidad de proyectos. Puede encontrarse en <https://www.boost.org/>


### Librería estándar de C

Tal y como ya se ha comentado, C++ es relativamente retrocompatible con C, y
soporta la mayor parte de su librería estándar. Las cabeceras de la librería
estándar de C se pueden traducir al estilo C++ sustituyendo `.h` por `c` al
inicio del nombre:

``` c
#include<string.h>
```

Se traduce a:

``` cpp
#include<cstring>
```

Además, estas librerías, incluidas con el estilo de C++, incluyen siempre sus
funcionalidades en el *namespace* `std`, aunque tienen permitido replicarlas
también en el *namespace* global, de modo que lo que en C era:

``` c
#include<string.h>
// ...
strcmp( /* ... */ );
```

Pasa a ser:

``` cpp
#include<cstring>
// ...
std::strcmp( /* ... */ );
```

En caso de incluir las librerías con el estilo de C, que también es posible
aunque no recomendable, esto no ocurre y se incluyen bajo el *namespace*
global.

En resumen y para que sirva de ejemplo, la cabecera `string.h` incluye
`::strcmp` mientras que `cstring` incluye `std::strcmp` aunque también tiene
permitido incluir `::strcmp`.

#### Casos especiales

Algunas librerías de C no resuelven a ninguna cabecera concreta, sino que
cargan otras cabeceras de la librería de C++. `complex.h` y `tgmath.h`, y sus
contrapartidas tipo C++ (retiradas en C++20), cargan directamente `complex` y
la pareja `complex` y `cmath` respectivamente.

Otras cabeceras de C no existen o no están soportadas en C++. `stdatomic.h`,
`threads.h` y `stdnoreturn.h` no existen en C++. Mientras que `ciso646`,
`cstdalign` y `cstdbool` no tienen sentido en C++ porque las funcionalidades
que aportan ya son parte del lenguaje. Incluir éstas últimas añade unas macros
sencillas para aportar compatibilidad.
