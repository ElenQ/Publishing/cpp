# Detección y corrección de fallos

No todos los errores de los programas pueden detectarse en el momento de
compilación. Los fallos o *bug* («bicho» en inglés) pueden deberse a muchas
causas: algoritmos mal implementados, mal control del *free-store*, problemas
inesperados producidos por interacciones con el exterior, etc.

Todos estos problemas tienen solución, pero muchos nunca se detectan. De hecho,
suele decirse que no existe el software sin *bugs*.

Este capítulo introduce dos herramientas muy importantes como referencia, GDB y
Valgrind, pero no con el fin de conocerlas en particular, que también es
interesante, sino como excusa para presentar el tipo de herramienta que son y
su utilidad en el desarrollo. Una vez conocidas de forma superficial, queda en
manos de cada cual escoger las herramientas que mejor se adecuen a su trabajo,
pero estas herramientas particulares sirven a la perfección para presentar las
respectivas modalidades a las que pertenecen.


## Sobre la compilación

No todas las compilaciones son iguales. Los compiladores suelen presentar un
amplísimo set de opciones que incluye gran cantidad de optimizaciones posibles.
En función de las optimizaciones que se escojan en el proceso de compilación,
el binario de código máquina resultante tendrá diferentes características:
primará la velocidad al tamaño o el tamaño a la velocidad, ofuscará el código
resultante, colocará en línea funciones que puedan ser optimizadas, reordenará
órdenes para aprovechar la segmentación de instrucciones[^pipelining]...

[^pipelining]: «La segmentación de instrucciones es una técnica que permite
  implementar el paralelismo a nivel de instrucción en un único procesador. La
  segmentación intenta tener ocupadas con instrucciones todas las partes del
  procesador dividiendo las instrucciones en una serie de pasos secuenciales
  que efectuarán distintas unidades de la CPU[...]» - Segmentación de
  Instrucciones \[Wikipedia\].  Consultado 2020-07-04.

Cada forma de compilar afecta al programa de modo que incluso puede que muestre
algunos errores en un modo que no muestre en otros. Esto se debe a muchas
razones, por ejemplo que diferentes opciones pueden suponer diferente velocidad
de ejecución y problemas que no salen a la luz debido a una posible
sincronización accidental del programa tienen menos posibilidad de
sincronizarse a velocidades más altas. En cualquier caso, se trata de fallos en
la programación ya que los programas deben funcionar independientemente de las
características con las que se ejecuten.

A la hora de compilar un programa en su versión de desarrollo, suelen reducirse
las optimizaciones para que la compilación sea más rápida, lo que reduce el
tiempo de espera de realizar un cambio y ver el resultado, y se suelen añadir
lo que se conocen como *debug symbols*, notas adicionales (símbolos en
realidad: *symbol*) que se añaden al resultado de la compilación para facilitar
la depuración (*debug*, de *de-* y *bug*, significa limpiar de errores o
depurar).  Además, suelen añadirse comprobaciones adicionales, todas las
posibles, para que el compilador avise de cualquier posible error que pueda
intuir.

Una vez probado el programa con toda la ayuda posible del compilador, el
programa pasa a producción, compilado con todas las optimizaciones posibles y
sin símbolos adicionales, de modo que el resultado es más pequeño y mucho más
rápido.

Aunque no es intención de este apartado, ni del documento, detallar todas las
opciones que pueden activarse en tiempo de compilación, sí que interesa mostrar
el caso de los *debug symbols* ya que usarlos aporta un cambio sustancial en el
uso de las herramientas que aquí se presentan.

### Símbolos de depuración

En GCC, el compilador del proyecto GNU, los *debug symbols*, o símbolos de
depuración, se añaden con la opción `-g`:

``` bash
$ g++ -g codigo.cpp
$
```

Añadir los símbolos de depuración construye binarios notablemente más grandes
pero con una característica especial: sus órdenes añaden referencias a su
código fuente. Este detalle es de vital importancia porque permite que tanto
GDB como Valgrind (u otras herramientas) analicen los binarios resultantes y
puedan avisar de errores presentando la línea de código fuente en la que se
encuentran, cosa imposible de no activar los símbolos de depuración.


## Valgrind

Valgrind es una herramienta muy poderosa.  Su funcionalidad más conocida es la
de comprobar el control del *free-store*, siendo capaz de comprobar fugas de
memoria (*memory-leaks*), uso de memoria no inicializada, dobles eliminaciones
o eliminaciones prematuras.

En realidad, Valgrind es un conjunto de herramientas que hacen uso de una base
común: un entorno de ejecución controlado analizable. Esto significa que su
funcionamiento se basa en ejecutar código dentro de su propia plataforma en
lugar de en la propia máquina de forma directa, donde se analiza en busca de
posibles problemas.

La herramienta de análisis de memoria es sólo una de las que presenta, teniendo
otras disponibles como el análisis de código multihilo, o el análisis de
rendimiento.

Su uso puede reducirse a lo más mínimo siendo todavía útil, ejecutar un
programa compilado a través de Valgrind sin configuración ni control adicional
aporta un análisis básico del control de memoria que puede ser de gran
utilidad para corregir errores con el *free-store*, que normalmente son
difíciles de encontrar y corregir.

Por ejemplo, lanzar Valgrind sobre el programa básico presentado al comienzo
del documento, el hola mundo, aporta una pista superficial de su comportamiento
como herramienta de análisis de posibles corrupciones de memoria:

``` bash
$ g++ -g hello.cpp
$ ls
a.out  hello.cpp
$ ./a.out
Hello World!
$ valgrind ./a.out
==5641== Memcheck, a memory error detector
==5641== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==5641== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==5641== Command: ./a.out
==5641==
Hello World!
==5641==
==5641== HEAP SUMMARY:
==5641==     in use at exit: 0 bytes in 0 blocks
==5641==   total heap usage: 2 allocs, 2 frees, 73,728 bytes allocated
==5641==
==5641== All heap blocks were freed -- no leaks are possible
==5641==
==5641== For lists of detected and suppressed errors, rerun with: -s
==5641== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
$
```

El mensaje de resultado lo aporta en inglés pero pueden observarse ciertos
detalles como «2 allocs» indican que han ocurrido dos alquileres de memoria,
dos llamadas a `new`, y «no leaks are possible» indica que no hay posibilidad
de fugas de memoria.

Este ejemplo no es demasiado representativo, porque no implica fallos, pero
tratar de compilar la siguiente pieza de código sí que encontrará fallos:

``` cpp
// archivo: bad_free_store.cpp
int main(){
    int *a = new int {2};
    int *b = new int {2};
    int *c = new int {2};

    // Doble eliminación
    delete a;
    delete a;

    // `b` y `c` no se liberan
}
```

El resultado de la compilación del programa:

``` bash
$ g++ -g bad_free_store.cpp
$ valgrind ./a.out
==5840== Memcheck, a memory error detector
==5840== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==5840== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==5840== Command: ./a.out
==5840==
==5840== Invalid free() / delete / delete[] / realloc()
==5840==    at 0x4838FE6: operator delete(void*, unsigned long)
(in /lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5840==    by 0x401191: main (bad_free_store.cpp:9)
==5840==  Address 0x4d4ac80 is 0 bytes inside a block of size 4 free'd
==5840==    at 0x4838FE6: operator delete(void*, unsigned long)
(in /lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5840==    by 0x40117B: main (bad_free_store.cpp:8)
==5840==  Block was alloc'd at
==5840==    at 0x4837DFA: operator new(unsigned long)
(in /lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==5840==    by 0x401133: main (bad_free_store.cpp:3)
==5840==
==5840==
==5840== HEAP SUMMARY:
==5840==     in use at exit: 8 bytes in 2 blocks
==5840==   total heap usage: 4 allocs, 3 frees, 72,716 bytes allocated
==5840==
==5840== LEAK SUMMARY:
==5840==    defbad_free_storeely lost: 8 bytes in 2 blocks
==5840==    indirectly lost: 0 bytes in 0 blocks
==5840==      possibly lost: 0 bytes in 0 blocks
==5840==    still reachable: 0 bytes in 0 blocks
==5840==         suppressed: 0 bytes in 0 blocks
==5840== Rerun with --leak-check=full to see details of leaked memory
==5840==
==5840== For lists of detected and suppressed errors, rerun with: -s
==5840== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```

El resultado muestra que han ocurrido cuatro alquileres y sólo tres
eliminaciones, lo que ha provocado pérdidas de memoria, así como ciertas notas
de dobles eliminaciones. Es evidente que este ejemplo no es el más
representativo de la utilidad del programa, pero es una pista suficiente para
comprender el tipo de problemas que puede encontrar y el poco esfuerzo
necesario para hacerlo.

Buscando en detalle en el resultado y como confirmación del apartado previo,
pueden encontrarse referencias al código fuente como `bad_free_store.cpp:3` que
indican dónde se encuentran los errores encontrados, marcando el archivo donde
se encuentran y la línea correspondiente. Estas notas no estarían disponibles
de no haber aplicado `-g` a la compilación, lo que complicaría la tarea
sobremanera.


## GNU Debugger

El GNU Debugger, o GDB, es una herramienta de depuración de código capaz de
funcionar con variedad de lenguajes, entre ellos C y C++. Casi todas las
herramientas de depuración de código son similares, así que conocer las
características básicas de GDB es útil para cualquier otra.

Muchos entornos de desarrollo de código incluyen interfaces gráficas o
integraciones a herramientas de depuración como GDB, que por defecto operan
sobre la terminal de comandos. Los ejemplos de este apartado se ilustran
mediante la interfaz de comandos, pero no debe desanimar a quien prefiera las
interfaces más modernas, porque es posible usarlas si se desea, manteniendo el
concepto subyacente, que es lo más importante, intacto.

Lejos de servir como un manual, esta sección tiene como objetivo el mostrar la
capacidad de navegar dentro de un programa en ejecución tal y como GDB y otros
depuradores habilitan.

### GDB como interfaz de comandos

GDB es un programa interactivo que corre como una interfaz de comandos. El
comando `gdb` ejecuta el depurador, que queda esperando a que se introduzcan
los comandos que se desee:

``` bash
$ gdb
GNU gdb (GDB) 9.2
Copyright (C) 2020 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "x86_64-unknown-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word".
(gdb)
```

Tal y como indica, introducir `help` aporta ayuda y `apropos` seguido de la
palabra que se desee busca secciones de la ayuda en la que se hable del tema
seleccionado. Al introducir `help` el programa muestra los diferentes tipos de
comandos de los que dispone:

``` bash
(gdb) help
List of classes of commands:

aliases -- Aliases of other commands.
breakpoints -- Making program stop at certain points.
data -- Examining data.
files -- Specifying and examining files.
internals -- Maintenance commands.
obscure -- Obscure features.
running -- Running the program.
stack -- Examining the stack.
status -- Status inquiries.
support -- Support facilities.
tracepoints -- Tracing of program execution without stopping the program.
user-defined -- User-defined commands.

Type "help" followed by a class name for a list of commands in that class.
Type "help all" for the list of all commands.
Type "help" followed by command name for full documentation.
Type "apropos word" to search for commands related to "word".
Type "apropos -v word" for full documentation of commands related to "word".
Command name abbreviations are allowed if unambiguous.
(gdb)
```

GDB es una herramienta con más de 30 años de historia, así que navegar por
todos los tipos de comando que aporta es una tarea que sobrepasa los objetivos
de este documento pero un conocimiento mínimo de GDB es más que suficiente para
trabajos de relativa complejidad.

### Ejecución de programas

Lanzar un programa desde GDB puede realizarse dos formas igualmente sencillas.
Añadiendo el programa como argumento de entrada a la ejecución de `gdb` o desde
la interfaz de `gdb` cargando el programa mediante `file` junto con el nombre
de programa y ejecutando `run`. Probando con el binario creado para el ejemplo
anterior desde `bad_free_store.cpp`:

``` bash
(gdb) file ./a.out
Reading symbols from ./a.out...
(gdb) run
Starting program: a.out ./a.out
free(): double free detected in tcache 2
(gdb)
```

Una vez lanzado y terminado el programa, las siguientes llamadas a `run`
permiten ejecutarlo de nuevo desde el principio. Al ejecutar `run` GDB avisará
que el programa ya terminó y preguntará si se desea volver a comenzar.


Se observa que el depurador también es capaz de encontrar dobles eliminaciones
(*double free*), tal y como hacía Valgrind en el apartado anterior. Cualquier
fallo de ejecución del programa será detectado y expuesto. Eso sí, si el
programa no implementa un algoritmo tal y como se esperaba no se puede
pretender que un depurador sepa detectarlo por sí mismo. Esto no significa que
un depurador no sea útil para encontrar casos de algoritmos incorrectos, en
realidad sí que lo es, pero no se puede esperar que lo haga solo.

### *Breakpoints* y navegación

Los *breakpoints* son puntos en los que se pausa la ejecución del programa para
poder analizar su estado. En `gdb` se insertan mediante el comando `break`
seguido de archivo de código fuente y la línea en la que se desean añadir. Si
se lanza el programa, éste se pausará en el lugar del *breakpoint*.

Los *breakpoints* también pueden añadirse en llamadas a funciones introduciendo
`break` seguido del nombre de la función donde se quiere pausar la ejecución.
Siempre que se llame a la función la ejecución se pausará en su llamada.

``` bash
(gdb) break bad_free_store.cpp:2
Breakpoint 1 at 0x40112a: file bad_free_store.cpp, line 3.
(gdb) run
Starting program: a.out

Breakpoint 1, main () at bad_free_store.cpp:3
3	   int *a = new int {2};
```

Es posible continuar la ejecución desde el *breakpoint* mediante el comando
`continue` o seguir paso a paso mediante el comando `step` que salta a la
siguiente orden y vuelve a pausarse. El comando `next` salta a la siguiente
línea sin considerar el código de las posibles subrutinas implicadas.
El comando `finish` termina la ejecución de la función actual, muy útil para
cuando desea verse el estado antes y después de ejecutar una función.

Para obtener información sobre los *breakpoints* activados puede usarse el
comando `info breakpoints`:

``` bash
(gdb) info breakpoints
Num     Type           Disp Enb Address     What
1       breakpoint     keep y   0x00040112a in main() at bad_free_store.cpp:3
	breakpoint already hit 1 time
(gdb)
```

Para borrar *breakpoints* se utiliza el comando `delete breakpoints` seguido
del número del *breakpoint* a eliminar (`1` en el ejemplo). No incluir ningún
número elimina todos los puntos.

#### *Breakpoints* condicionales

Es posible además añadir *breakpoints* sólo en caso de que una condición se
cumpla mediante el uso de *breakpoints* condicionales (*conditional
breakpoints*). Se incluyen igual que los *breakpoints* descritos previamente
seguidos de la condición que se desee.  Usarlos ahorra mucho tiempo de
navegación a través del programa así que se anima a aprender a usarlos mediante
`help break`.

### Localización en el programa

En programas grandes es fácil perderse. Los comandos `where` (dónde) y
`backtrace` entregan la posición actual en el programa y cómo se ha llegado a
ella mostrando la pila de llamadas a funciones (*call-stack*).

### Análisis del estado

En cualquier punto del programa pueden visualizarse variables y sus contenidos
mediante el comando `print`. Su versión `print/x` muestra el valor en
hexadecimal.

``` bash
Breakpoint 1, main () at bad_free_store.cpp:3
3	   int *a = new int {2};
(gdb) print a
$9 = (int *) 0x0
(gdb) print *a
Cannot access memory at address 0x0
(gdb) next
4	   int *b = new int {2};
(gdb) print a
$10 = (int *) 0x416eb0
(gdb) print *a
$11 = 2
```

El ejemplo muestra cómo intenta accederse a la variable `a`, un puntero, que
aún no está asignada. Así que se salta a la siguiente orden con `next` y en
ella ya es posible comprobar `a`. Como `a` es un puntero, su valor ha de
de-referenciarse mediante el `*`, como en C++, así que se aplica y se observa
que su valor es `2`, tal y como se esperaba.

El comando `set variable` puede asignar valores arbitrarios a variables del
programa para probar posibles estados manualmente, incluso aquellos a los que
es imposible llegar con el comportamiento habitual del programa. Es una
herramienta útil, pero que debe usarse con cuidado.

#### Observación de variables

Un método para no depender de una buena selección de los lugares donde se
pausa la ejecución del programa es poner una variable bajo observación mediante
`watch` seguido de su nombre. De esta forma la ejecución se pausará cuando la
variable cambie de valor.

### Otras herramientas

GDB es una herramienta tan compleja que su manual tiene más de ochocientas
páginas. Dispone de funcionalidades muy avanzadas para análisis de código
multihilo, decompilación y un larguísimo etcétera que no tiene sentido listar
aquí. Esta breve y sencilla introducción tiene como fin romper la barrera de
entrada al uso de este tipo de herramientas y animar a quien tenga interés a
seguir estudiándolas.

Poder acceder a un programa en ejecución y comprobar los valores de sus
variables debería ser suficiente incentivo para motivar el uso de herramientas
de depuración. Aunque la verdad es que todo el resto de funcionalidades que
aportan, o al menos aporta GDB, son, si cabe, aún más asombrosas desde el punto
de vista técnico.


## Recapitulación

Es costumbre habitual de quien se dedica a la programación tratar de depurar
los programas mediante lo que se conoce coloquialmente como la técnica del
`printf`. `printf` es una función de C para visualizar resultados en pantalla.
Es tan conocida que la técnica ha tomado su nombre independientemente del
lenguaje en el que se aplique. La técnica radica en llenar el programa de
sentencias de impresión, como `cout` (de `<iostream>`) o el propio `printf`
(aportado por la cabecera de compatibilidad con C `<cstdio>`) en C++, de modo
que se visualicen las variables a las que se les desea hacer un seguimiento.
Después, el programa se compila y se ejecuta para visualizar el resultado de
las impresiones.

Para programas pequeños en entornos que no disponen de herramientas de
depuración de código fuente es una técnica justificable y sorprendentemente
efectiva. En programas grandes con largos tiempos de compilación (hay
compilaciones que pueden prolongarse horas) no es una técnica factible.

Valgrind, presentado de forma tan simple que difícilmente le hace justicia a un
programa tan interesante, muestra lo sencillo que puede ser hacer una
comprobación de la memoria del programa. La herramienta es capaz de encontrar
fallos de control de memoria y realizar un análisis de la memoria utilizada.

GDB, el depurador del proyecto GNU, por su parte, muestra un amplio set de
funcionalidades de navegación y análisis del estado de un programa en
ejecución. Todo esto mostrado en una sencilla interfaz de comandos.

Además, no es necesario usar la interfaz de comandos para realizar la
depuración si no se desea. Los entornos de desarrollo integrados suelen
disponer de la opción de crear *breakpoints* en el propio texto del programa y
navegar a través de él como si se estuviese interpretando directamente desde el
entorno de desarrollo mientras que muestran una tabla con las variables y sus
valores.

Ambas herramientas han sido muy influyentes en el sector de modo que muchas
otras tienen comportamientos similares. Como siempre, seleccionar las
herramientas que mejor se adaptan al modo de trabajo es primordial para sentir
comodidad en el proceso y centrar la atención en lo importante.

Herramientas como las propuestas en este apartado son verdaderas obras de
ingeniería y su inclusión, aunque tímida, al final del documento, tiene como
finalidad que cada cual se recuerde cuando se vea usando la técnica del
`printf` que existen otras formas y que está, en definitiva, perdiendo el
tiempo.

