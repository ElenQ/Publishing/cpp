# Lo necesario

Antes de comenzar a programar es necesario detallar varios conceptos y aclarar
las ideas.

Para comenzar a programar en C++ se necesita lo siguiente:

- Un editor de texto plano
- Un compilador de C++

El editor permite editar el código fuente, mientras que el compilador se
encarga de traducir ese código fuente a un lenguaje que la máquina sea capaz de
procesar.

## Editores

Cualquier editor de texto plano resuelve esta tarea correctamente, pero es más
adecuado usar un editor con resaltado de sintaxis de C++ y herramientas que
facilitan el desarrollo como pueden ser:

- Buscar y reemplazar
- Plegado y desplegado del texto entre llaves (`{}`) y comentarios
- Inserción automática de parejas de apertura y cierre para los paréntesis
  (`()`), corchetes (`[]`) y llaves (`{}`)
- Control de sangría
- Inserción y edición en bloque

En general, cualquier editor de texto plano que esté diseñado para código
fuente aportará estas herramientas. Su uso o necesidad depende del gusto
personal y de la necesidad.

Entre los editores, destacan los clásicos Vim y Emacs pero existen infinidad de
editores con similares características que cumplen con la tarea de la mejor de
las maneras.

## Compiladores

C++ es un estándar (ISO) de un lenguaje de programación. Otros lenguajes, en
cambio, se implementan y se considera esa implementación la referencia
principal del comportamiento. En C++, la referencia de comportamiento es el
estándar, y no existe una implementación de referencia.

Existen muchas implementaciones distintas, eso sí, que cumplen el estándar en
mayor o menor detalle, siendo las dos principales GCC y Clang. Desarrollados
por el proyecto GNU y por LLVM respectivamente.

El estándar C++ se actualiza a medida que se requieren nuevas funcionalidades y
existe un grupo de mejora del lenguaje que se encarga de esta tarea. Estas
actualizaciones provocan que las implementaciones con menos recursos de
desarrollo queden atrasadas respecto a la última publicación del estándar. Las
implementaciones GCC y Clang son, con diferencia, las que más actualizadas se
mantienen y menos tiempo necesitan para actualizarse con las nuevas
publicaciones.

A la hora de elegir el compilador, es necesario tener en cuenta para qué
plataforma se programa ya que el código máquina que genere será diferente en
función del dispositivo para el que se programe (*compilation target*). No es
lo mismo programar un microcontrolador PIC16, por ejemplo, que un
microprocesador Intel x86 y, por supuesto, el código máquina creado para uno no
sirve para el otro.

Cuando el objetivo de la compilación se trata de un dispositivo distinto al que
se usa para compilar, se dice que el compilador es un *Cross Compiler*: un
compilador con capacidad para producir código máquina para una arquitectura
diferente a la suya propia.

Independientemente del *compilation target* y de si el compilador es un *cross
compiler* o un *compiler* a secas, el comportamiento del lenguaje será idéntico
desde el punto de vista de quien programa [^quien-programa] a pesar de que el
lenguaje máquina generado por el compilador sea distinto.

[^quien-programa]: Esto significa que el algoritmo funcionará de la misma
  manera, pero es posible que detalles no especificados en el programa se
  resuelvan u optimicen de formas distintas debido a diferencias en la
  arquitectura subyacente.

### Freestanding vs Hosted

Desde el estándar C++11 se definen dos tipos de implementación de C++:
*Freestanding* y *Hosted*.

La implementación *Hosted* está pensada para generar programas que funcionan
sobre un sistema operativo mientras que la implementación *Freestanding* está
enfocada para sistemas, normalmente embebidos, que pueden no tener un sistema
operativo.

Esta diferencia conceptual sirve para separar los requerimientos de la
implementación. No se le puede pedir lo mismo, en términos de funcionalidad, a
un programa que funcionará sobre un sistema operativo (*Hosted*) que a uno que
funcionará por su cuenta (*Freestanding*).

Por ejemplo, las implementaciones *Hosted* requieren que el programa defina una
función `main` que será la que el sistema operativo ejecute cuando el programa
se lance. Sin embargo, el uso de una función principal `main` en las
implementaciones *Freestanding* no es obligatorio sino que depende de la
plataforma.

Esta peculiaridad puede verse claramente en Arduino[^arduino]. Como
implementación *Freestanding* que es, Arduino requiere el uso de dos funciones
principales: `setup` y `loop`. Pero no requiere un `main` ya que no existe
sistema operativo que lo llame. Sin embargo, el sistema está preparado para
llamar en primer lugar a la función `setup` y después ejecutar la función
`loop` en bucle.

Existen otro tipo de diferencias de requisitos adicionales entre las
implementaciones *Freestanding* y *Hosted*, pero principalmente se resumen en
que las implementaciones *Freestanding* están obligadas a incluir menos
cantidad de librerías por defecto, tienen unos requisitos más laxos y más
libertad para definir comportamientos propios.

En caso de trabajar usando un compilador *Freestanding* es necesario tener su
documentación específica a mano, ya que muchos detalles serán exclusivos de esa
implementación concreta.

[^arduino]: Ver https://arduino.cc

## Herramientas extra

Aunque es posible programar C++ con las herramientas mencionadas, existen
varias limitaciones que pueden solventarse con algunas herramientas
adicionales.

La primera de ellas, es que el proceso de compilación de proyectos grandes
puede ser largo y requerir muchos recursos de la máquina de desarrollo.

Además, los compiladores son herramientas complejas con muchas opciones
diferentes y el proceso de compilación en muchas ocasiones requiere compilar el
programa por partes que luego se unen en el resultado final (*linking*),
complicando aún más las llamadas al compilador.

Para resolver estas dos limitaciones existen herramientas de automatización de
compilación (*build automation* o *build system*) que se encargan de analizar
qué partes del programa han sido compiladas previamente y de gestionar las
llamadas al compilador mediante una receta previa. De este modo, simplifican el
proceso de ejecución del compilador y reducen el tiempo de compilación en
proyectos grandes, evitando recompilar todo el proyecto completo si sólo ha
sufrido cambios una parte de éste. *Make* es una herramienta de *build
automation* ampliamente conocida y usada, con varias implementaciones, siendo
la más conocida la del proyecto GNU: *GNU-Make*.

A pesar de que Make solventa muchos problemas, algunos proyectos requieren de
medidas adicionales. Cuando se debe tratar con dependencias externas o se
pretende realizar código portable entre sistemas, el proceso se complica
sobremanera: los diferentes sistemas usan nombres distintos para las librerías,
el sistema de búsqueda de dependencias es diferente, etc. Para simplificar
estas tareas existen herramientas que añaden una capa de abstracción adicional.
En ésta se describen unas reglas sencillas para el proceso de compilación y
ellas se encargan de gestionar las reglas del sistema de automatización de
compilación. Existen muchas herramientas diferentes para esta tarea y ninguna
destaca especialmente (*GNU Build System*, conocido popularmente como
*Autotools*; *CMake*; *Meson*...)[^autotools-cmake] aunque cada una tiene sus
peculiaridades y limitaciones.

Cabe destacar que CMake es capaz de gestionar diferentes de herramientas de
gestión de compilación, como *Make* o *Ninja*, siendo este segundo un sistema
alternativo a Make pensado únicamente para ser automatizado y no para ser usado
manualmente.

[^autotools-cmake]: El proceso habitual de uso de GNU Build System o CMake
  puede ser conocido por cualquiera que tenga cierta experiencia instalando
  proyectos de software ya que aparecen en infinidad de ellos: `./configure`,
  `make` y `make install` en el primero y `cmake .`, `make` y `make install` en
  el segundo.  Los tres pasos preparan el entorno de compilación asegurándose
  de encontrar las dependencias adecuadas, compilan el proyecto y lo instalan,
  respectivamente.

Por otro lado, el proceso de compilación es capaz de encontrar errores en el
código (tipos incorrectos, fallos de sintaxis...), pero no es capaz de
encontrarlos todos ya que algunos sólo se manifiestan en tiempo de ejecución o
deben ser comprobados por un humano (algoritmo incorrecto...). Para encontrar
estos fallos se pueden utilizar herramientas de análisis o *debuggers*, capaces
de ejecutar el programa paso a paso haciendo referencia al código fuente de
éste y que permiten acceder al estado del programa en ejecución. Uno de los
*debuggers* más conocidos y usados es, de nuevo, el de GNU: *GDB* (*Gnu
DeBugger*).

## Herramientas integradas

Como el proceso de compilación, automatización e incluso análisis es complejo y
va de la mano con la edición del código fuente, se han creado herramientas de
desarrollo integradas o IDE (del inglés *Integrated Development Environment*)
que agrupan todos los puntos abarcados anteriormente.

Ejemplos de ello son:

- KDevelop: el editor integrado de C++ del proyecto KDE. Un editor muy completo
  preparado para trabajar con el estándar de calidad de KDE.
- QtCreator: el editor integrado de Qt, diseñado para trabajar con proyectos
  que hacen uso de la librería gráfica Qt, aunque también para otros.
- Code::Blocks: un editor integrado desarrollado pensando en la flexibilidad y
  la configurabilidad que dispone de un sistema automatización de compilación
  propio (también permite usar Make). A diferencia de los dos anteriores, éste
  está basado en wxWidgets en lugar de en Qt.

Existen infinidad de IDEs aunque todos son similares. Conocer cada uno de los
pasos para una correcta compilación es, en realidad, más importante que
acostumbrarse a una herramienta concreta ya que, al simplificar tanto la tarea,
permiten a quien las usa olvidarse de los detalles importantes, facilitando así
la aparición de problemas.

Esto no implica que los IDEs sean malos por definición: la dejadez lo es. Usar
un IDE puede hacer la vida más fácil, pero, igual que al pilotar un avión, es
necesario recordar quién está al mando y saber recuperar el control cuando
el piloto automático no es suficiente.


## Dónde aprender

No es posible discutir sobre informática sin hacer referencia al software libre
y al proyecto GNU por su aporte al patrimonio tecnológico universal y a la
liberación del conocimiento en su lucha por crear un Sistema Operativo
Libre[^gnu-libre].  El proyecto GNU dispone de uno de los compiladores de C y
C++ más avanzados disponible, `gcc`, y multitud de herramientas relacionadas
con el proceso de compilación, ya que son las propias herramientas necesarias
para desarrollar el sistema operativo GNU.

Como sistema operativo libre y transparente que es, dispone de muchos recursos
de cómo funcionan estas herramientas y es posible leer cómo han sido
desarrolladas. Se recomienda encarecidamente visitar el proyecto GNU como
referencia tanto a nivel de software como de documentación.

Durante el documento se tratarán varias herramientas de GNU y se hará
referencia a muchos de sus manuales debido a lo recién comentado.

[^gnu-libre]: Lucha, que por cierto, resultó fructífera hace años con la
  inclusión de Linux como kernel, llegando así a crear un sistema operativo
  completo, GNU/Linux, poco después de la liberación de Linux como software
  libre a principios de los años 90. Desde entonces, el tándem GNU/Linux goza
  de una salud excelente, tanto en el escritorio como en el servidor.

Por otro lado, existen variedad de sitios web de consulta. El más destacable es
sin duda <https://cppreference.com>. Usado como referencia para este mismo
documento, C++ Reference incluye gran cantidad de información sobre el
lenguaje, descrita a nivel estrictamente técnico pero claro, y traducida,
parcial o totalmente, a varios idiomas.

A nivel bibliográfico, cualquier libro escrito por Bjarne Stroustrup[^bjarne],
autor de C++, es una referencia adecuada para comenzar y profundizar en el
lenguaje, siendo el propio estándar de C++ el documento de consulta adecuado si
se pretende realizar una implementación del lenguaje.

[^bjarne]: Su página web lista sus libros: <http://stroustrup.com/>
