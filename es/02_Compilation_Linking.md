# Compilación y Enlazado

Previamente se ha tratado la compilación de forma superficial, pero no se ha
detallado de qué trata el proceso ni cómo se realiza, sirva este capítulo para
resolver esos detalles.


## Nota: Freestanding vs Hosted

Tal y como se menciona en el capítulo previo, existen dos formatos de
implementación diferentes: Freestanding y Hosted. En todo este documento se
tratan principalmente las Hosted, que requieren un sistema operativo, ya que
tiene intención de tratar temas recurrentes y comunes repetibles de una
aplicación a otra. El conocimiento obtenido podrá usarse parcial o totalmente
en una aplicación Freestanding, pero cada aplicación Freestanding debe
analizarse de forma independiente. Debido a los detalles de implementación de
cada plataforma es inútil centrarse en ellas en este punto, aunque se tratará
de mencionar algunos ejemplos cuando sea interesante.

Las implementaciones Hosted, sin embargo, comparten mucho más entre ellas y el
conocimiento obtenido al estudiarlas como aquí se estudian es útil de un modo
más general que alcanza más lejos del propio lenguaje C++. En particular, en
este capítulo, aunque también en los que vienen después, se visitan, de forma
lateral, conceptos de arquitectura de ordenadores, sistemas operativos e
incluso electrónica digital pero, principalmente, se describe cómo se comportan
los programas: un conocimiento absolutamente fundamental para *cualquier*
programador de *cualquier* lenguaje de programación que es extrapolable a
prácticamente *cualquier* plataforma.


## La compilación

Un compilador, como ya se avanzó en el capítulo previo, es un programa que es
capaz de convertir código fuente de un lenguaje a otro. Normalmente, la palabra
compilador se utiliza para denominar a los compiladores que traducen de un
lenguaje de alto nivel a uno de bajo nivel, y se reservan otras palabras como
*transpilador* para los compiladores que convierten entre lenguajes del mismo
nivel o *decompilador* para los que parten de un lenguaje de bajo nivel y
llegan a uno de alto nivel.

Cuando se habla de alto o bajo nivel, se trata del nivel de abstracción del
lenguaje, a más alto más abstracto, lo que implica que es capaz de describir
procesos más complejos de forma similar a la que un humano lo haría. Y, por
aclararlo, se considera que C++ es un lenguaje de alto nivel.

Los compiladores que se tratan en este documento convierten C++ a lenguaje
máquina pues éste es el proceso más común con el que cualquiera podrá trabajar.
Aunque existen otros compiladores, que ni siquiera mencionaremos, el interés
principal de C++ es que es posible compilarlo a lenguaje máquina, por lo que
cualquier otra aplicación será una excepción o será, simplemente, innecesaria.

### Lenguaje máquina

El lenguaje máquina es el nivel más bajo disponible: es el lenguaje que el
procesador utiliza para realizar sus acciones. Aunque no es necesario conocerlo
para programar C++, es interesante saber qué es y para qué sirve.

El lenguaje máquina es un conjunto de órdenes binarias que se procesan por una
unidad central de procesamiento (CPU). Una CPU es un dispositivo electrónico
con una arquitectura interna que le hace responder de una u otra manera en
función de unos estímulos recibidos. Estos estímulos son los que llamamos
«órdenes». La realidad es que estas órdenes, que parecen comandos de un
lenguaje, son el resultado de una intrincada arquitectura física interna la
cual está diseñada para recibir señales de forma consecutiva e ir respondiendo
a ellas ordenadamente, dando así una ilusión de ejecución. Todo son pequeños
interruptores activándose y desactivándose en función de unas señales de
control que llegan de forma ordenada.

El lenguaje máquina, entonces, son las señales de control y sus posibles
combinaciones, ordenadas en lo que conocemos como «programa».

Aunque esta visión simplista pueda sugerir que el lenguaje máquina no se
comporta como un lenguaje, ocurre justamente lo contrario. A pesar de ser
señales (binarias) de control, las órdenes del lenguaje máquina están diseñadas
para tener una lógica y ser comprensibles, pero esto puede, a veces, tener
implicaciones en el diseño de la circuitería interna de los dispositivos, por
lo que no todas las máquinas usan el mismo lenguaje.

Por tanto, cuando se habla de lenguaje máquina, la primera pregunta que
es necesario responder es «¿De qué máquina?», ya que no todas usan el mismo ni
tiene las mismas características. Un programa en lenguaje máquina puede ser
ejecutado por un microprocesador o por un microcontrolador, pero tendrán
características diferentes puesto que cada uno se diseñó con un fin distinto.

Existen diferentes arquitecturas (MIPS, ARM, x86, RISC-V...) para los
dispositivos, muchas veces propias de un fabricante. Algunas de ellas son tan
comunes que existen compiladores independientes capaces de generar código
máquina para ellas, pero otras, menos comunes, requieren de compiladores
específicos. Es por eso que en el capítulo previo se habla de *compilation
targets*, *cross-compilation*, etc.

Desde nuestro punto de vista, el lenguaje máquina será nuestro objetivo y es
por eso que tenemos que conocer su existencia aunque podamos abstraernos más
tarde. Cuando programemos, nuestro programa compilado será un fichero de
contenido binario que contendrá las órdenes de lenguaje máquina de nuestro
programa y será éste el que finalmente se ejecutará y no nuestro código fuente.

### El código portable

Visto lo que se acaba de tratar, se entiende que el código generado para un
dispositivo no podrá ejecutarse en dispositivos con arquitecturas distintas.
Sin embargo, C++ se ha vendido en más de una ocasión como código portable. ¿A
qué se debe ésto?

Como se ha dicho anteriormente, C++ es un estándar, lo que implica que
cualquier implementación de éste debe comportarse de igual modo
independientemente de su máquina objetivo (*compilation target*). Por tanto, el
mismo código C++ puede compilarse a diferentes máquinas, haciendo que el código
fuente, y no el código máquina, sea portable.

Esto es la teoría. La realidad es que algunas máquinas tienen peculiaridades de
implementación  y que algunos sistemas operativos se comportan de un modo
peculiar y complican las cosas. En este documento sobrevuela estos detalles
como si casi no existieran pero quien lleve años programando será consciente de
ellos y sabrá que es necesario tener cuidado. Es algo que la práctica y el
estudio profundo resuelve y queda lejos del objetivo que aquí se trata.

Sí que existe, sin embargo, un caso en el que la portabilidad se complica por
definición: la diferencia entre Freestanding y Hosted, que ya se ha mencionado
previamente. Al ser implementaciones con requisitos distintos, muy
probablemente el código no pueda extrapolarse de una a otra con total libertad.
Es muy posible, por otro lado, poder migrar partes del código, aunque la
interacción con el sistema y con algunas librerías no será coherente aunque
ambas implementaciones cumplan con el estándar en su totalidad.


### Un programa sencillo

Para ver el proceso de compilación no hay mejor manera que comenzar con un
programa sencillo. El típico programa «Hola mundo»:

``` cpp
#include <iostream>

int main(int argc, char* argv[]) {
    using namespace std;
    cout << "Hello World!\n";
    return 0;
}
```

Guardando este programa en un fichero llamado `hello.cpp` es momento de
compilarlo. Para ello se usará a lo largo del documento GCC, el compilador de C
y C++ del proyecto GNU, aunque otros compiladores llegarían al mismo resultado.
Ejecutando el siguiente comando en la shell de sistema en la carpeta donde
se guardó el fichero `hello.cpp` [^dollar]:

``` bash
$ g++ hello.cpp -o hello
$
```

La ejecución ha retornado sin mostrar absolutamente nada, pero se aprecia que
un nuevo fichero ha aparecido: `hello`. Al ejecutarlo, muestra «Hello World!»
en la terminal.

``` bash
$ ./hello
Hello World!
$
```

Por supuesto, el proceso es idéntico al usar otro compilador, como Clang.
Uniendo todos los pasos:

``` bash
$ clang++ hello.cpp -o hello
$ ./hello
Hello World!
$
```

[^dollar]: el símbolo del dólar (`$`) marca el *prompt*, el símbolo de la shell
  que indica que está a la espera de que se le introduzca un comando. Se
  incluye en los ejemplos para indicar claramente que esa línea ha sido
  introducida manualmente. Las líneas sin prompt indican que han sido escritas
  por el propio programa. Evidentemente, el `$` no debe copiarse a la hora de
  tratar de replicar estos ejemplos.

Lo prometido en el apartado anterior es cierto entonces, el compilador ha
creado un archivo binario, `hello`, con las órdenes en lenguaje máquina que
puede ser ejecutado por el sistema.

A pesar de lo simple que pueda parecer este ejemplo, tanto con un compilador u
otro, la cruda realidad es mucho más compleja de lo que parece y es ahora
cuando debemos afrontarla para tenerla dominada en el resto de capítulos.

### Pasos de la compilación

El fichero C++ ha sido compilado a un ejecutable, pero la realidad es más
compleja que eso. La verdad es que ha habido varios procesos consecutivos que
el compilador ha ocultado porque el ejemplo era muy sencillo.

#### Preprocesado

El preprocesado es un proceso previo a la compilación que aporta cierta
flexibilidad en los ficheros de código fuente. Su labor es la de analizar los
ficheros de código fuente en busca de *directivas del preprocesador* y
alterarlos en consecuencia.

Este procesado previo trabaja directamente en el contenido del programa en C++,
en el propio texto, antes de realizar la compilación.

El *preprocesador* es quien se encarga de hacer esa comprobación inicial, y sus
labores pueden ser desde incluir ficheros a crear macros que alteran el
contenido del fichero y facilitan la escritura. El preprocesador usa un
lenguaje propio, muy reducido, para hacer su labor y no se pretende que haga
labores especialmente complejas con él, es más bien una ayuda al desarrollo.

En el ejemplo aparece una directiva del preprocesador, `#include`, cuya función
es incluir el contenido de un fichero, `iostream`, en el fichero actual. El
fichero `iostream` incluye definiciones que se usan en el código de ejemplo,
por eso es necesario incluirlo. Si se elimina esa línea, el proceso de
compilación falla de la siguiente manera:

``` bash
$ g++ hello.cpp -o hello
hello.cpp: In function ‘int main(int, char**)’:
hello.cpp:5:5: error: ‘cout’ was not declared in this scope
    5 |     cout << "Hello World!\n";
      |     ^~~~
hello.cpp:1:1: note: ‘std::cout’ is defined in header ‘<iostream>’; did you
 forget to ‘#include <iostream>’?
  +++ |+#include <iostream>
    1 |
$
```

Indicándonos que las declaraciones necesarias están en la cabecera (*header*)
`iostream`. Más adelante se estudia lo que eso significa, aunque, en resumen,
se puede entender que `iostream`, sea lo que sea, incluye definiciones que
nuestro programa necesita para funcionar y que éstas deben cargarse con una
*directiva del preprocesador*.

#### Compilación

Después de preprocesar todos los ficheros de código fuente, el compilador los
lee y construye el código máquina correspondiente para cada uno de ellos. Sin
embargo, ese código máquina no es un programa definitivo. El proceso de
compilación únicamente construye una versión en lenguaje máquina del archivo de
código fuente actual, sin llegar a prepararlo para su ejecución.

El ejemplo de compilación previo sí que construye un programa final, pero esto
se debe a que no se le ha pedido expresamente que compile, sino que se le ha
pedido que haga todo lo necesario para crear un programa.

Es posible, y será necesario más adelante, pedir al compilador que únicamente
construya una versión en lenguaje máquina del archivo de entrada. Para ello, en
GCC se debe incluir la opción `-c`:

``` bash
$ g++ hello.cpp -c
$
```
Lo cual produce un archivo binario llamado `hello.o`[^object].

El manual de GCC describe la opción `-c` de la siguiente
manera[^original-gcc-c]:

> -c  Compila o ensambla los archivos fuente, pero no enlaza (link). La etapa
> de enlazado simplemente no se realiza. El formato de salida es un archivo
> objeto para cada archivo fuente.

La diferencia entre el archivo `hello.o` y el ejecutable anterior es evidente
nada más ver su tamaño. El archivo ejecutable es unas 6 veces más grande que el
archivo objeto (*object file*) para este caso.

Aunque ahora puedan parecer inútiles, los archivos objeto son la unidad básica
con la que se construyen las diferentes salidas posibles de los programas C++.
Esta forma de trabajo, donde cada archivo puede compilarse independientemente
del resto, se conoce como *separate compilation* o compilación separada o
independiente y se trata de un mecanismo para facilitar la modularización del
código.

[^object]: En Windows pueden tomar la extensión `.obj` en lugar de `.o`.

[^original-gcc-c]: Es una traducción libre. El texto original en inglés es el
  siguiente:  
    *`-c`  Compile or assemble the source files, but do not link.  The linking
    stage simply is not done.  The ultimate output is in the form of an object
    file for each source file.*


#### Enlazado (linking)

El proceso de enlace es el encargado de ordenar y combinar los archivos objeto
producidos por el compilador.

El enlazador (*linker*) incluirá el código máquina necesario en el programa
final y lo ordenará de forma que pueda funcionar correctamente. Es por eso que
en el ejemplo el binario ejecutable es mucho más grande que el archivo
compilado a lenguaje máquina: el *linker* está añadiendo código máquina
adicional en el binario final.

GCC dispone de un opción, `-l`, para indicar qué librerías adicionales deben
enlazarse en el binario, en la primera llamada, a pesar de que no lo muestre,
está usándolas para incluir las librerías por defecto, usando `-lstdc++ -lm
-lc` que incluye la librería estándar de C++, la librería matemática y la
librería estándar de C respectivamente.

El enlazado se realiza por un programa independiente en muchos casos, aunque el
propio compilador, como GCC en este caso, se encarga de llamarlo en su último
paso. Las herramientas de GNU incluyen también, como es lógico, la herramienta
necesaria para el enlazado, llamada `ld` (o *the GNU Linker*), como parte del
proyecto *binutils* pero también es posible utilizar un *linker* externo si se
desea.


## Posibles resultados

Comprendidos los pasos del proceso de compilación, es necesario comprender qué
se desea obtener de éste y qué se necesita para cada una de los posibles
resultados. Los anteriormente mencionados *object files* o archivos objeto son,
como se adelantó, el ingrediente principal del que se obtendrán los diferentes
resultados posibles.

Recordar, antes de entrar a los posibles resultados del proceso, que éstos
dependen de la máquina objetivo (*compilation target*) y que debemos
seleccionar el compilador adecuado para la tarea, con las opciones adecuadas.

### Ejecutable

El resultado más evidente, el que se trata en el primer ejemplo, es el de
conseguir un programa ejecutable. Este programa está diseñado para ser lanzado
por la máquina en la que se encuentre.

En las implementaciones *Hosted*, el sistema operativo llamará a la función
`main` del programa al ejecutarlo. En las implementaciones *Freestanding* no
existe ningún requerimiento de este tipo, así que el proceso de ejecución
dependerá de la implementación concreta.

Lo que sí que es cierto en todas es que se puede entender el ejecutable como un
*object file* con aderezos, que le permiten ser ejecutado. Además, este
ejecutable final incluye las librerías que se hayan incluido en el proceso tal
y como se describe a continuación.

### Librerías

Las librerías son piezas de código que pueden utilizarse en otros programas y
están diseñadas no para usarse sueltas, sino únicamente con ese fin. En muchas
ocasiones es interesante crear una librería y distribuirla de forma
independiente para facilitar que otras personas la utilicen. En otras
ocasiones, a pesar de que el objetivo pueda ser crear un programa ejecutable,
puede ser interesante separar el programa en un bloque principal y librerías
añadidas, para poder probar éstas de forma independiente, por ejemplo,
incluyéndolas por separado en un programa de testeo.

En cualquier caso, las librerías forman parte fundamental del día a día de
quien se dedique a programar, o de cualquiera que utilice una computadora, pues
ellas son quien permite que el mundo de la informática crezca tan rápido
mediante la reutilización de código fuente.

Existen dos tipos de librerías y su proceso de construcción es diferente, a
continuación se describen ambos, con sus ventajas e inconvenientes principales.

#### Librerías Estáticas

Las librerías estáticas (*static library* o *statically-linked library*) son un
conjunto de *object files* que se agrupan en un único fichero (también conocido
como *archive*). A la hora de enlazar un programa, si éste hace uso de alguna
librería estática, el *linker* las buscará y las incluirá en su archivo
resultante. Es por eso que el ejecutable obtenido en ejemplo era más grande que
el *object file* obtenido de su compilación sin enlazado.

Como las librerías estáticas son únicamente un conjunto de *object files* que
se incluyen en el momento del enlazado, pueden crearse desde los archivos
objeto sin demasiado problema. El proyecto binutils de GNU dispone de la
herramienta necesaria para esta labor: `ar`[^ar].

Esta ejecución de ejemplo construye (la `c` significa `create`, construir, y la
`r` sirve para añadir los objetos al archivo) la librería
`libexample`[^static-lib-ext] desde los archivos de objeto `file1`, `file2` y
`file3`:

```
$ ar cr libexample.a file1.o file2.o file3.o
```

[^ar]: De la palabra _**ar**chive_.

[^static-lib-ext]: El ejemplo usa las extensiones de sistemas operativos tipo
  UNIX. En Windows, las extensión para librerías estáticas es `.lib` mientras
  que la de los *object files* es `.obj`.

#### Librerías Dinámicas

Las librerías dinámicas, librerías compartidas u objetos compartidos (*shared
object*, *shared library* o *dynamically linked library*) son un segundo tipo
de librería que hace uso de una funcionalidad aportada por el sistema
operativo, el enlazador dinámico, para poder compartirse entre varios
programas.

El enlazador dinámico (*dynamic linker*) es capaz de, mientras un programa se
está ejecutando, encontrar si necesita cargar una librería externa e inyectarla
al programa.  Permitiendo así al programa no incluir la librería dentro de su
ejecutable.

Este mecanismo permite a los programas compartir librerías de uso común,
haciendo posible que sólo exista una copia en el sistema y que todos la carguen
cuando la necesiten en lugar de llevarla incluida dentro de su código
ejecutable, reduciendo así el tamaño de los programas.

Aunque parezca muy ventajoso, esto también trae problemas añadidos, los más
evidentes son los siguientes:

- No se puede confiar que el sistema ya incluya la librería que el programa
  necesita.
- El sistema puede incluir una librería de una versión incompatible con la que
  el programa requiere y resultar en fallos de ejecución.
- La carga de la librería dinámica afecta negativamente al rendimiento del
  programa [^shared-object-load].

[^shared-object-load]: Generalmente, la carga se realiza en la primera ocasión
  que el programa necesita hacer uso de una función aportada por la librería,
  así que esta reducción en el rendimiento no es recurrente y no tiene tanto
  impacto en programas cuya ejecución se prolonga en el tiempo.

Para poder cargarse en momento de ejecución en *cualquier* programa, las
librerías dinámicas deben compilarse de forma especial facilitando así la tarea
del enlazador dinámico del sistema operativo. Este modo especial se conoce como
*position independent code* (código independiente de su posición). Con este
formato, el código máquina creado funciona correctamente independientemente de
la posición del programa en la que se cargue.

GCC, el compilador de referencia de este documento, es capaz de crear librerías
dinámicas. El siguiente ejemplo crea la librería dinámica `libexample` a partir
de los archivos objeto. La opción `-fPIC` activa la creación de código
independiente de la posición, y la opción `-shared` activa la creación de un
objeto compartido (*shared object*), es decir, de una librería
dinámica[^shared-object-compilation].

```
$ g++ -shared -fPIC -o libexample.so file1.o file2.o file3.o
```

[^shared-object-compilation]: Las extensiones utilizadas en el ejemplo son las
  usadas en sistemas operativos tipo UNIX. La extensión UNIX de las librerías
  dinámicas es `.so` que viene de la denominación _**s**hared **o**bject_. En
  Windows, sin embargo, al usarse la denominación *dynamically-linked library*
  y usar extensiones de tres caracteres, la extensión es `.dll`.


Cabe recordar que las librerías dinámicas hacen uso de una función del sistema
operativo en el que el programa está corriendo, con los problemas que ello
pueda acarrear. Algunos sistemas operativos no tan conocidos no disponen, por
diseño o por falta de desarrollo, de un enlazador dinámico por lo que las
librerías dinámicas no pueden utilizarse en esos entornos.

Ni que decir tiene que en contextos sin sistema operativo, entornos
*Freestanding*, es muy posible que no se pueda utilizar ninguna librería
dinámica, ya que no habrá nadie que la cargue en momento de ejecución, así que
todo deberá enlazarse estáticamente e incluirlo en el resultado.



## Recapitulación

Se ha descrito de forma superficial el proceso completo de conversión de un
programa a código máquina tanto para el caso de una librería como el de un
programa ejecutable.

Se ha descrito la compilación independiente y, con ella, los archivos objeto y
cómo son éstos los ingredientes necesarios para crear cualquier programa o
librería.

> En un proyecto grande, con más de cien archivos de C++ distintos (cosa
> bastante común), que se desee compilar, si se trata de compilar todos los
> archivos a cada cambio que se realice, el compilador tardará en construir la
> salida (de minutos a horas, en función de la complejidad del programa). Sin
> embargo, pueden compilarse por separado, obteniendo sus *object file*, y
> combinarlos después.
>
> De este modo, siempre que se haga un cambio en el proyecto que se desee
> probar, sólo hay que compilar los archivos que han sufrido cambios y combinar
> todos los archivos objeto para construir el nuevo resultado.  Del mismo modo,
> es posible reutilizar los compilados para crear tests, librerías u otros
> programas sin tener que partir del código fuente en C++.
>
> Este proceso es lo que programas como Make hacen con el fin de ahorrar tiempo
> y recursos.  Si el lenguaje no soportase la compilación separada forzaría a
> compilar el proyecto en su totalidad a cada cambio que sufriera provocando
> demoras insoportables.

Sin entender lo que es un archivo objeto es imposible conocer el funcionamiento
de las herramientas de compilación, lo que puede acarrear errores y el uso de
las herramientas con temor, por no conocer qué están haciendo en cada momento.
