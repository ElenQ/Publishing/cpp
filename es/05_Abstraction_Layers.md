# Capas de abstracción

C++ añade un rico set de funcionalidades adicionales a C para convertirlo en un
lenguaje de alto nivel de abstracción y, al mismo tiempo, evitando tener un
impacto demasiado negativo en el rendimiento. Algunas de estas funcionalidades,
como los *namespaces*, facilitan la escritura de código modular, ayudando a
aislar conceptos en proyectos grandes, mientras que otras, como las clases,
habilitan un paradigma de programación completo, la programación orientada a
objetos, con el fin de añadir robustez al código y aumentar la capacidad del
compilador para detectar errores en tiempo de compilación mientras que permiten
que quien programa se centre en conceptos más cercanos al pensamiento humano.

Como puede observarse, todos estos añadidos aumentan las capacidades de C, un
lenguaje que muchos consideran rudimentario, sin perder la esencia de éste,
basada en el acceso a características de bajo nivel, pero no todos son iguales:
algunos sólo facilitan técnicas de programación, como los *namespaces*,
mientras que otros permiten a quien programa pensar en nivel de abstracción más
alto. Este capítulo se centra en el segundo grupo, puesto que es realmente el
que diferencia a C++ de C.

En capítulos previos se han analizado conceptos que no forman parte de C, pero
que no suponen un salto conceptual importante. En esta ocasión los conceptos a
tratar son suficientemente complicados como para cambiar el lenguaje hasta
convertirlo en un lenguaje propio, por lo que requieren de mayor estudio y
comprensión.

Cuando C++ fue publicado por primera vez, se presentó como «C con clases» y,
aunque ya no se puede decir que esta presentación sea totalmente cierta, éste
puede ser un buen resumen de lo que es C++. Conceptos como la sobrecarga de
operadores y la programación genérica con plantillas, que se estudiarán en este
capítulo, o la gestión de excepciones, que ya se estudió en un capítulo previo,
están relacionados con las clases o dejan de tener sentido si éstas no existen,
es por eso que las clases son el centro de este capítulo, así como lo son del
propio lenguaje.

## Introducción a las clases

Las clases son definiciones de tipos, los cuales se comportan del mismo modo
que los tipos básicos aportados por el lenguaje.

Normalmente las clases sirven para representar alguna construcción de la mente
humana, ya sea algo que tiene una referencia del mundo real, como un cliente o
una factura, o algo que no la tiene, como una forma geométrica, un número
complejo, o una conexión de algún protocolo de Internet.

Las clases son un mecanismo complejo que cumple muchas funcionalidades de forma
paralela: aisla los detalles de implementación del tipo construido del resto
del programa (*encapsulación*), facilita la reutilización de código (*herencia*
y *polimorfismo*), facilita el pensamiento abstracto, etc. Todas estas
facilidades se estudiarán individualmente a su debido tiempo.

Por facilitar la comprensión, es posible partir con una estructura, ya que las
estructuras en C++ son realmente clases sencillas[^example-complex]:

``` cpp
struct complex{
    float real;
    float imag;
};

void conjugate(complex &a){
    a.imag *= -1;
}
complex add(complex &a, complex &b){
    complex c;
    c.real = a.real + b.real;
    c.imag = a.imag + b.imag;
    return c;
}
```

[^example-complex]: C++ ya dispone de una clase para gestionar números
  complejos (ver `<complex>`), el ejemplo sólo pretende ser una pieza de código
  fácil de comprender.

Las funciones `conjugate` y `add` están asociadas a `complex` por sus
argumentos de entrada, y éstas son capaces de leer el contenido de un valor
complejo o incluso de alterarlo. Sin embargo, no existe una asociación
explícita entre las funciones y el propio tipo `complex`, y es incómodo tener
que usar la función `add` para sumar dos números complejos en lugar de usar
simplemente el operador `+`.

El primer problema podría resolverse moviendo las funciones al cuerpo de la
estructura y añadiéndolas como miembros.

``` cpp
struct complex{
    float real;
    float imag;

    void conjugate(void){
        imag *= -1;
    }
    complex operator+(complex b){  // Sobrecarga el operador +
        complex c;
        c.real = real + b.real;
        c.imag = imag + b.imag;
        return c;
    }
};

complex a {1,-1};           // Crea `1-1i`
complex b = a;              // Copia `a` en `b`
b.conjugate();              // Conjuga `b` alterando su contenido
complex c = a+b;            // `2+0i`
```

Al ser miembros de la estructura, las funciones tienen acceso a los otros
miembros de ésta, así que no es necesario usar el primer argumento de entrada.
Ahora las llamadas se realizan mediante los operadores de acceso a miembros de
la estructura (`.` y `->`) como ya se describió al tratar sobre estructuras.

La función `operator+` permite sobrecargar el operador de suma, de modo que,
cuando éste se use con los argumentos del tipo declarado en la función será la
función la encargada de realizar la operación. Este concepto se trata más
adelante, pero sirva este ejemplo para mostrar que una clase (o estructura en
este caso) definida manualmente puede operar como cualquier tipo básico del
lenguaje sin restricciones de ningún tipo, siempre que se declare su
comportamiento.

Como se puede apreciar, agrupar la funcionalidad asociada a una estructura o
clase dentro de ella tiene ventajas: facilita la modularización del código,
como si se tratara de un *namespace*, y permite el acceso directo a los
miembros del objeto asociado.

Llevando estos conceptos un poco más hacia el extremo, es posible obtener una
definición resumida de las clases y explicar desde ésta describir todo el
ecosistema de funcionalidades que las rodean.

- Una clase es un tipo definido en el programa.
- Las clases disponen de miembros, siendo los más comunes los datos miembro
  (también conocidos como «propiedades») y las funciones miembro (también
  conocidos como «métodos»).
- Las funciones miembro conocen el objeto desde el que se llamaron.
- Las funciones miembro pueden definir como deben crearse, moverse, copiarse o
  destruirse los objetos del tipo definido por la clase.
- Los miembros se acceden mediante el operador punto (`.`), o flecha (`->`)
  cuando se trata de punteros.
- Es posible definir el comportamiento de operadores (tales como `+`, `-`,
  `[]` y `()`).
- Una clase es un *namespace* de sus miembros.
- Las clases disponen de un mecanismo de control de accesos, permitiendo
  definir sus miembros como privados (`private`), donde se recogen los detalles
  de implementación, o públicos (`public`), donde se describe la interfaz con
  el resto del programa.
- Las estructuras (`struct`) son una clase cuyos miembros son públicos
  (`public`).


Los miembros de la clase pueden definirse dentro de ella o en su exterior,
siempre que se declaren antes. Como diferentes clases pueden tener miembros del
mismo nombre, es necesario usar el nombre de la clase para identificar a cuál
pertenecen. El comportamiento es idéntico al de la definición de miembros de un
*namespace*, ya que las clases son un *namespace*.

``` cpp
class Complex{
    public:
        float real;
        float imag;

        void conjugate(void);
        Complex operator+(Complex b);
};

void Complex::conjugate(void){
    imag *= -1;
}

Complex Complex::operator+(Complex b){
    Complex c;
    c.real = real + b.real;
    c.imag = imag + b.imag;
    return c;
}
```

En el ejemplo se muestra cómo convertir el ejemplo anterior en clase realizando
el simple cambio de sustituir la estructura por una clase cuyos miembros son
públicos (ver *[Control de acceso]*) aunque la parte del ejemplo que se trata
en este apartado es la definición de funciones miembro fuera del cuerpo de la
clase.

Esta diferenciación permite declarar las clases en archivos de cabecera y
mover sus funciones a los ficheros de código fuente. Separando la funcionalidad
de la declaración.

> NOTA: Las clases, por convención, suelen usar nombres que comienzan en
> mayúscula, para así diferenciarlas de las variables, que suelen usar nombres
> en minúscula.


### Objetos

Los objetos, palabra que se ha repetido innumerables veces en este documento,
son instancias de las clases.

``` cpp
class Complex{
    // ...
};

Complex a {1,2};  // `a` es un objeto
```

Por explicarlo de otro modo, una clase es la descripción de cómo deben
comportarse las instancias de ésta. Los objetos son instancias de ella que se
comportan como la clase describe.

Al igual que las funciones sólo describen un comportamiento, las clases
describen nuevos tipos, que pueden utilizarse posteriormente. Las variables
creadas con los tipos en cuestión son lo que se llama objetos.

Como las clases son un tipo más, los variables creadas a partir de los tipos
básicos del lenguaje también son objetos.

Cuando se llama a una función miembro de un objeto, ésta es capaz de alterar el
objeto al que pertenece así que tiene constancia de cuál es el objeto al que va
asociada.

La palabra `static` es capaz de declarar miembros de clases que no están
asociados a ningún objeto particular, tal y como se verá más adelante. Por
defecto, los miembros siempre están asociados a un objeto.

### Miembros

El acceso a los miembros se realiza mediante `.` o `->`, al igual que en las
estructuras (las estructuras son clases).

Las clases pueden usar todo tipo de miembros, siendo los más habituales las
funciones y los datos. No es infrecuente encontrar alias como miembros, o
incluso clases, que en ambos casos se comportarán como en el caso de pertenecer
a un *namespace*, ya que las clases son *namespaces*, y aplicarán las reglas de
*scope* asociadas a éstos.

#### Datos miembro

Los datos miembro, como los campos de una estructura cualquiera, almacenan la
información asociada al objeto. Estos miembros pueden alterarse por cualquier
función miembro del mismo objeto y las funciones miembro siempre tendrán
acceso.

#### Funciones miembro

Las funciones miembro son funcionalidades asociadas a los objetos que pueden
llamarse para un objeto en particular. Rescatando un ejemplo anterior:

``` cpp
class Complex{
    public:
        float real;
        float imag;

        void conjugate(void);
        Complex operator+(Complex b);
};

void Complex::conjugate(void){
    imag *= -1;
}

Complex b {0, 10};
Complex a {1,-20};
a.conjugate()       // Ahora `a.imag == 20`
```

La llamada a `a.conjugate()`, como es lógico, aplica `conjugate` a su objeto
asociado, `a`, dejando `b` inalterado. Este concepto se conoce como
auto-referencia (*self-reference*).

#### Auto-referencia

El cuerpo de las funciones miembro puede acceder a los otros miembros de forma
directa. En algunas ocasiones es necesario acceder al objeto como un todo, en
lugar de a sus miembros. La palabra reservada `this` puede usarse con este fin.

`this` es una expresión *prvalue* cuyo valor es la dirección del objeto cuya
función (no-estática) fue llamada. Recuperando el ejemplo:

``` cpp
void Complex::conjugate(void){
    imag *= -1   // Equivalente a `this->imag *= -1;`
}
```

Un ejemplo del interés para usar `this` son los casos en los que los
argumentos de entrada de la función escondan (*shadow*) los miembros de la
clase:

``` cpp
Class complex {
    float real;
    float imag;

    void set_imag(float imag){
    //                  ^^^^  Esconde el miembro `imag`
        this->imag = imag;
    //  ^^^^^^  Es la única forma de especificar el miembro del objeto
    }
};
```

#### Encadenado (*chaining*)

Una técnica de programación muy común es el encadenado (*chaining*) que para
permitir la ejecución encadenada de alteraciones en el mismo objeto hace uso
del puntero `this`.

``` cpp
// Llamada a funciones miembro que alteran
shape.rotate(-90);
shape.scale(0.5);
shape.move(10,10);

// Versión con encadenado (chaining)
shape.rotate(-90).scale(0.5).move(10,10);
```

Es evidente que el segundo caso es mucho más legible y sencillo, pero requiere
un poco más de trabajo preparar la clase para poder usarla de ese modo: es
necesario que cada función miembro en la cadena retorne el propio objeto, cosa
que se puede conseguir mediante `return *this`.

``` cpp
class Shape {
    // ...
    Shape rotate(float angle);
    Shape scale(float factor);
    Shape move(float x, float y);
};

Shape Shape::rotate(float angle){
    // Aplicar la rotación
    return *this;
}
//...
```

#### Miembros estáticos

Una clase puede definir miembros que en lugar de estar asociados con sus
instancias (objetos) están asociados con la propia definición de la clase, y se
comparten con todas las instancias de ésta. Este concepto se conoce como
miembro estático, y se declara añadiendo la palabra reservada
`static`[^static-member] a las declaraciones de los miembros.

[^static-member]: Fuera de una clase, `static` tiene un significado distinto.
  Ver *[Enlazado avanzado]*.

Hay dos formas disponibles para acceder a un miembro estático. La primera se
basa en que las clases son *namespaces* así que aplica el operador `::` al
nombre de la clase, la segunda, usa la metodología habitual de acceso a
miembros (`.` y `->`) de una instancia de ella. Los miembros de la clase tienen
acceso a miembros estáticos como si de cualquier otro miembro se tratara.

``` cpp
// Declaración
struct A {
    static int base;
    static int modulo(int num);
};
A::base = 2;
int A::modulo(int num){
    return num % base;
}

// Uso como namespace
A::modulo(19);  // == 1

// Uso a través de instancia
A instance;
instance.modulo(10)  // == 0
```

##### Datos miembro estáticos

Los datos miembro estáticos no están asociados a ningún objeto y existen aunque
no exista ningún objeto de la clase en la que se declararon. Sólo existe una
instancia de estos objetos por todo el programa[^static-member-lifetime] y su
vida útil en el programa será estática, es decir, se construyen una única
ocasión y su dirección permanece inalterada durante todo el programa.

Los miembros estáticos, al no estar asociados a ningún objeto, no pueden
declararse como `mutable`. Sin embargo, pueden declararse `const`, `constexpr`
o `inline` (desde C++17), lo que les permite a inicializarse en su declaración,
lo que no está permitido de otra forma[^init-static-member].

[^static-member-lifetime]: Van asociados a la definición de la clase, y sólo
  puede haber una definición de la clase en el programa.

[^init-static-member]: `constexpr` requiere de inicialización en el momento de
  la declaración.

##### Funciones miembro estáticas

Las funciones miembro estáticas no están asociadas ningún objeto así que no
reciben el puntero al objeto actual `this`. Además, no pueden ser `volatile`,
`virtual` ni `const`, ya que son conceptos que están asociados con la
existencia de objetos.

#### Funciones miembro constantes

Las funciones miembro declaradas `const` reciben el objeto actual, `this`,
calificado de la misma forma. Esto significa que no puede ser alterado por la
función.

Las funciones miembro declaradas como `const` aseguran que el contenido del
objeto asociado no cambiará de modo que pueden usarse de forma segura. No es
necesario declararlas `const`, pero hacerlo aporta, por un lado, la pista a
quien use la función de que su objeto permanecerá inalterado y, por otro, todo
el poder del compilador para comprobar que quien está desarrollando la función
no ha cometido un error y ha alterado el objeto al que pertenece.

El objetivo de las funciones miembro constantes, más allá de dejar el objeto
inalterado en su totalidad es el de asegurar que el estado del objeto no se
altera. Esto trae consigo una cuestión filosófica: ¿son todos los campos de un
objeto parte de su estado?

Aunque la respuesta pueda parecer un sí rotundo, la realidad es que, en
diferentes implementaciones, lo objetos pueden disponer de campos no
relacionados con su estado que sirven exclusivamente de ayuda a la computación.
Una caché, por poner el ejemplo más evidente, puede ser un caso de este tipo.

Suponiendo una clase encargada de hacer peticiones a un servidor o de realizar
cálculos complejos tiene un estado que define a quién debe hacer las peticiones
o qué tipo de cálculos realiza. Almacenar temporalmente el resultado de sus
acciones no está relacionado con el funcionamiento de la clase, es simplemente
una ayuda a su ejecución, para ahorrar tiempo cuando se pida una nueva
ejecución.

Partiendo de estos ejemplos puede entenderse que las funciones `const`, que
evitan alterar el estado de un objeto, puedan tener acceso a este tipo de
campos auxiliares, que no forman parte del estado del objeto. Estos campos se
conocen como miembros mutables.

##### Miembros mutables

Los miembros mutables, declarados como `mutable`, pueden ser alterados por
funciones `const` y sirven precisamente para almacenar valores que no están
relacionados con el estado lógico del objeto.

Un objeto puede cambiar un miembro mutable y aun así mantener el mismo estado
desde el punto de vista lógico (del pensamiento humano). Los miembros mutables
se usan para esos detalles de implementación que se necesitan para aplicar el
pensamiento humano en una computadora, pero que no tienen relación con éste.


#### Control de acceso

A diferencia de las estructuras, las clases pueden definir el tipo de acceso
que permiten a sus miembros.

``` cpp
class Complex{
    private:
        float real;                       // Privado
        float imag;                       // Privado

    public:
        void conjugate(void);             // Público
        Complex operator+(Complex b);     // Público
        float get_real(void);             // Público
        float get_imag(void);             // Público
};

void Complex::get_real(void){
    return real;
}
void Complex::get_imag(void){
    return imag;
}

// ...

Complex a {1,2};
a.imag;                // ERROR: acceso a miembro privado
a.get_imag();          // `2`
```

Las palabras clave `public`, `protected` y `private` definen los permisos sobre
los diferentes miembros.

- Los miembros públicos (`public`) definen la interfaz de la clase con el
  exterior, así que pueden accederse desde cualquier parte del programa.
- Los miembros protegidos (`protected`) definen la interfaz de la clase con sus
  clases derivadas[^derived-classes].
- Los miembros privados (`private`) definen la interfaz de la clase consigo
  misma, es decir: su implementación.

Como se aprecia en el ejemplo, afectan a todos los miembros declarados tras su
inclusión y no sólo al primero de ellos.  A diferencia de las estructuras,
donde los miembros son por defecto públicos, los miembros de una clase son
privados a no ser que se especifique lo contrario.

El control de acceso es opcional, pero fundamental en el código C++ debido a
que es la base de la *encapsulación*. Un diseño mediocre puede acabar
definiendo todos los miembros de una clase como públicos independientemente de
su funcionalidad y el compilador no se quejará por ello. Es responsabilidad de
quien programa asignar los permisos adecuados para proteger el acceso a los
miembros cuyos valores no se espera que se editen desde el exterior. El caso
más comprensible en este momento es el de campos que deben alterarse de manera
controlada, como el mes del año en una fecha. El hecho de aislar el acceso a
los campos y controlar cómo se alteran asegura que la clase se mantenga siempre
en un estado correcto. La *encapsulación* es esta forma de aislar los campos
relativos a la implementación y de controlar el acceso al estado de las clases.

[^derived-classes]: Las clases derivadas son aquellas que se crean mediante el
  mecanismo de herencia, reutilizando la definición de una clase para crear una
  nueva, normalmente con un nivel de concreción mayor.

#### Funciones amigas (*friend*)

Las funciones amigas (*friend*), son funciones externas declaradas dentro de
las clases con el especificador `friend` que permiten acceder a miembros
privados (`private`) y protegidos (`protected`) de la clase a pesar de no ser
parte de ella.

Las funciones `friend` no pertenecen al *scope* de la clase (están fuera de
ella) pero pueden acceder a sus detalles internos. Esto es valioso sobre todo
en casos en los que se necesita interacción directa entre dos clases, y donde
crear funciones de acceso a los datos podría ser demasiado tedioso.

``` cpp
class A{
    // ...
    friend void alter(A& input);
    private:
        int value;
};

// Es necesario declarar la función fuera de la clase
void alter(A& input){
    input.value = 10;           // Tiene acceso porque es amiga
}
```

Las funciones `friend` pueden declararse tanto como privadas como públicas sin
afectar a su comportamiento.

También es posible declarar todas las funciones miembro de una clase amigas de
otra:

``` cpp
class A {
    friend class B;
    // ...
};
```


## Ciclo de vida de los objetos

La sección *[Modos de creación y vida útil]* introduce los conceptos *rvalue*,
*lvalue*, punteros y vida útil que en este apartado se explotarán hasta la
extenuación, así que es interesante conocerlos en detalle antes de afrontar
este apartado.

El ciclo de vida más habitual de los objetos es el siguiente:

Al definirse se crean (se les asigna un espacio en la memoria). La creación
puede ir acompañada de una inicialización. En caso de incluir un valor inicial,
se asigna el valor al objeto o se copia desde otro objeto del mismo tipo.
Durante su vida, un objeto puede copiarse a otro mediante una asignación,
entrega como argumento de entrada de una función, mediante una sentencia
`return` o mediante el lanzamiento y captura de excepciones (`throw` y
`catch`).  Cuando el objeto queda fuera de *scope*, se elimina de la memoria.

En la descripción de la vida del objeto hay varios puntos críticos: la
construcción, el copiado y la eliminación. C++ permite que las clases definan
su propio comportamiento en los tres casos mediante lo que se conocen como:
constructor (*constructor*), constructor-copia (*copy constructor*),
asignación por copia (*copy assignment*) y destructor (*destructor*).

Sin embargo, esos no son los únicos ciclos de vida posibles. Los *rvalue*
tienen un ciclo de vida más corto, eliminándose en el momento en el que dejan
de ser necesarios, normalmente al final de cada sentencia. Los *rvalue* pueden
aprovecharse mediante las *move semantics* para optimizar los programas
realizando movimientos en lugar de copias. Cada clase puede definir cómo deben
moverse sus instancias mediante el constructor de movimiento
(*move-constructor*) y la asignación de movimiento (*move-assignment*).

El *free store*, o la memoria dinámica, permite la creación de objetos de
gestión manual, que no se crean por los mecanismos normales y no se eliminan
automáticamente al salir de *scope*. La memoria dinámica permite, entre otras
cosas, crear objetos con la capacidad para crecer, o dicho de otra forma, cuyo
tamaño se calcule en tiempo de ejecución. En estos objetos el control del ciclo
de vida es fundamental para asegurar un correcto control de los recursos.


### *Free store*

Aunque se mencionó en su momento, el *free store*[^free-store-vs-heap] no se ha
tratado más allá de una explicación simple que dice que los objetos en el *free
store* se crean y eliminan manualmente mediante los operadores `new` y
`delete`.

[^free-store-vs-heap]: Comúnmente se conoce el *free store* como *heap* y en
  otros documentos así se nombra. El nombre *heap* es una herencia histórica de
  cuando el *free store* se implementaba utilizando una estructura de datos en
  forma de árbol conocida como *binary heap*. Hoy en día, esa implementación no
  es tan común, pero la verdad es que su nombre, *heap*, se usa incluso de con
  más frecuencia que *free store*, aunque el correcto sea este último.

El *free store* es un almacenamiento de control manual, que permite la
construcción de variables de forma dinámica, en cualquier lugar del programa.
Por ejemplo, permite construir *arrays* de un tamaño calculado en tiempo de
ejecución.

C++ introduce los operadores `new` y `delete` como parte del lenguaje para
realizar las operaciones de alquiler y liberación de memoria que en C suelen
realizarse con las funciones `malloc` y `free` o similares.

Las llamadas a `new` alquilan una sección de memoria del tamaño del tipo
seleccionado y devuelven su dirección. Para alquilar *arrays* puede usarse
`new[]`.

``` cpp
int* num = new int;           // Alquila un `int`
int* arr = new int[10];       // Alquila un array de 10 `int`
```

Las llamadas a `new` pueden inicializarse mediante inicialización directa (`()`
o `{}`).

``` cpp
int *num = new int {10};    // Alquila un nuevo int de valor 10
int *num = new int = 10;    // ERROR: Inicialización por copia
```

Para liberar el espacio de memoria alquilado por `new`, basta con aplicar
`delete` al puntero recibido o `delete[]` en caso de que se trate de un
*array*.

``` cpp
delete num;
delete[] arr;
```

`delete` sólo puede aplicarse a memoria previamente alquilada mediante `new` o
al `nullptr`. C++ define que la aplicación de `delete` a `nullptr` no debe
tener ningún efecto.

#### Control de memoria dinámica

El inconveniente del *free store* es que, al no ser *automatic*, su memoria no
se limpia automáticamente al salir de *scope* y debe limpiarse de forma manual
mediante el operador `delete`. Esto suele conllevar errores, los más habituales
los siguientes:

- Fuga de memoria: Objetos creados con `new` y nunca eliminados con `delete`.
  En inglés se conoce como *memory leak*.
- Eliminación prematura: Objetos eliminados con `delete` que son usados más
  adelante como si no hubieran sido eliminados.
- Eliminación doble (o múltiple): Objetos que se eliminan en más de una ocasión
  con `delete`.

Las fugas de memoria pueden hacer que un programa deje de tener memoria
disponible, debido a que nunca libera memoria. La eliminación prematura es un
problema grave también, ya que accede a secciones de la memoria ya no
disponible que incluso ha podido usarse con otros fines, pudiendo llegar así a
corromper otros objetos. La eliminación doble (o múltiple), es similar a la
eliminación prematura, ya que trata de ejecutar código sobre un objeto
inexistente, pero en este caso el código es destructivo. El comportamiento de
las dobles eliminaciones no está definido, pero normalmente suelen acabar en
errores desastrosos y difíciles de descifrar, sobre todo en programas grandes.

Muchas veces los errores llegan por un mal control de los punteros alquilados,
como puede apreciarse en el ejemplo:

``` cpp
auto a = new shape[10];
auto b = a;              // Copia el puntero
// ...
delete[] b;  // Libera la memoria a la que apuntan `a` y `b`
delete[] a;  // Eliminación doble
```

Estos errores, aunque son evitables, son extremadamente comunes en lenguajes
que no aportan un mecanismo automático para resolver las eliminaciones. En C++
el mecanismo de alquiler y liberación de memoria es mucho más inteligente que
en C, aprovechando el RAII, lo que implica que la probabilidad de aparición de
estos errores puede reducirse usando las herramientas de alto nivel que C++
aporta, pero para que sean efectivas hay que conocerlas al detalle y hacer un
buen uso de ellas.

La primera forma de evitar errores generados por el *free store* es no usarlo,
o limitar su uso a donde sea estrictamente necesario. En muchas ocasiones una
variable local es suficiente para resolver el problema.

La segunda forma recomendada, la usada por las clases `std::string`,
`std::vector` y similares, se basa en el uso de `std::unique_ptr` o
`std::shared_ptr`, definidos en `<memory>`. Estos clases se conocen como *smart
pointers* (punteros inteligentes) y son un caso avanzado del uso del RAII,
donde se transfiere el control de un objeto a otro, que es capaz de eliminarlo
correctamente cuando sale del *scope*.

Por otro lado, muchos casos que históricamente se realizaban mediante el *free
store* pueden resolverse hoy en día con *move semantics*, la capacidad de C++
(a partir de C++11) de mover objetos temporales y cambiar su dueño para ahorrar
memoria y tiempo de ejecución.

### *Smart pointers*

Los *smart pointers* (en español «punteros inteligentes»), introducidos en
C++11, son punteros que facilitan el uso de RAII en los programas que hacen uso
del *free store*. Pueden encontrarse en la librería estándar del lenguaje, en
`<memory>`.

Cuando se trata con memoria alquilada de forma dinámica, es difícil recordar
quién debe liberar qué puntero puesto que éstos no tienen ningún responsable
asignado. Los punteros inteligentes capturan objetos y se convierten en los
responsables de eliminarlos cuando sea necesario.

Los punteros inteligentes almacenan un objeto y su eliminador (por defecto
`delete`) y son capaces de eliminar el objeto cuando su *scope* termine, o el
puntero inteligente sea reasignado a otro objeto.

Los punteros `unique_ptr` (puntero único) sirven para controlar secciones de la
memoria sólo accesibles desde un lugar, mientras que los `shared_ptr` (puntero
compartido), con la ayuda de `weak_ptr` (puntero débil), controlan memoria en
uso desde diferentes lugares.

El `weak_ptr` no captura el objeto, sino que permite acceder a un objeto
controlado por un `shared_ptr` emulando así un acceso temporal, aunque también
son útiles para controlar casos de referencias circulares en punteros
compartidos que podrían resultar en una fuga de memoria.

Todos estos objetos hacen un uso avanzado de la sobrecarga de operadores,
redefiniendo sus operadores de asignación para eliminar la memoria asociada al
objeto al reasignarse, y sus operadores de eliminación, que destruyen el objeto
asociado a ellos de forma ordenada.

### *Move semantics*

*Move semantics* son el conjunto de capacidades del lenguaje para mover objetos
en lugar de realizar copias.

Por defecto, las asignaciones en C++ copian un objeto a otro, lo que conlleva
la copia de todos los elementos. En caso de que el objeto a copiar sea muy
complejo o muy grande el proceso es lento y/o requiere de gran cantidad de
memoria.

Desde C++11, con las referencias a *rvalues* y la capacidad de mover objetos,
este proceso puede optimizarse. En casos en los que el objeto a copiar fuera a
ser desechado al terminar la instrucción (*rvalues*) éste puede mantenerse en
la memoria, pero asignado al identificador al que se quiere copiar, consumiendo
únicamente el tiempo necesario para asignar una referencia (una operación
sencilla) y aprovechando la memoria del *rvalue* que de otro modo iba a ser
destruido.

No es posible mover *lvalues* debido a que éstos están asociados a un nombre, y
el movimiento es una operación destructiva que dejaría el nombre sin una
variable asociada. Sin embargo, tampoco es necesario, ya que es posible usar
referencias para este fin, lo que ahorraría copias excesivas.

En cualquier caso, estos conceptos de *move semantics* sólo tienen sentido
cuando se conocen los constructores en detalle. Sirva esto como primer
acercamiento a su funcionamiento.

### Construcción y destrucción

Casi todas las operaciones de gestión de recursos en C++ se basan en la pareja
constructor-destructor, siendo el primero el encargado de crearlos o
adquirirlos y el segundo el encargado de destruirlos o liberarlos.

#### Constructores

El trabajo de los constructores es el de definir cómo deben crearse instancias
de la clase, sirviendo para inicializar todo lo necesario y obtener sus
recursos.

Para declarar un constructor, es necesario declarar una función miembro cuyo
nombre sea el mismo que el de la clase y no retorne ningún
valor[^void-constructor].

[^void-constructor]: Los constructores no usan `void` como tipo de salida, ya
  que no tienen como fin retornar nada. Es el único lugar donde se permite no
  añadir tipo de retorno.

``` cpp
class Date{
public:
    Date(int year, int month, int day);
private:
    int day, month, year;
    // ...
};

// Inicialización
Date yesterday {2020, 6, 14};   // `yesterday` se inicializa en el constructor
```

Además, los constructores aseguran que una clase siempre se construye en un
estado válido y esa es gran parte de su función. La clase declarada podría
asignarse con el constructor por defecto (que funciona como en una estructura,
asignando los miembros directamente) pero éste no comprueba los datos de
entrada de ninguna manera. Comprobaciones que en el ejemplo, que trata de una
fecha, serían, si el día es menor de 31, 30, 29 o 28 en función del mes y del
año, que el mes sea menor que 12, etc. Estas limitaciones definidas por el
diseño de las clases se llaman comúnmente *class invariants* y es
responsabilidad de la clase mantenerlas y asegurar mediante su interfaz que no
es posible violarlas.

Es habitual encontrar clases con varios constructores distintos. La sobrecarga
de funciones juega un papel crucial en este punto, definiendo diferentes tipos
desde los que inicializar el objeto que se desea crear.

``` cpp
#include <ctime>

class Date{
public:
    Date();
    Date(std::time_t timestamp);
    Date(int year, int month, int day);
private:
    int day, month, year;
    // ...
};

// Inicialización
Date someday{};                     // `someday` se inicializa con el primer
                                    // constructor

Date today { std::time() }          // `today` se inicializa en el segundo
                                    // constructor

Date yesterday {2020, 6, 14};       // `yesterday` se inicializa con el tercer
                                    // constructor

Date failday {1, "hola"};           // ERROR: No hay constructor adecuado
```

También es frecuente el uso de parámetros por defecto en los constructores,
permitiendo obviar algunos argumentos para simplificar las construcciones de
los objetos.

#### Destructores

Los destructores son el inverso de los constructores, su labor es la de liberar
los recursos de los objetos. La declaración del destructor es similar a la del
constructor, pero usa el nombre con el símbolo de complemento (la vírgula, `~`)
como prefijo:

``` cpp
class Date{
public:
    Date(int year, int month, int day);
    ~Date();
private:
    int day, month, year;
    // ...
};
```

#### Constructores y destructores con herencia y clases miembro

Las clases descritas son relativamente sencillas, por lo que el mecanismo de
construcción y destrucción podía explicarse en pasos únicos.

Sin embargo, a la hora de añadir herencia (se estudia más adelante) o clases
miembro, el proceso de construcción-destrucción debe realizarse por pasos.

El proceso de construcción es el siguiente:

1. Se lanza el constructor de la clase base
2. Se lanzan los constructores de las clases miembro
3. Finalmente, se ejecuta el constructor de la clase actual

El proceso de destrucción es el opuesto:

1. Se ejecuta el cuerpo del destructor actual
2. Se ejecutan los destructores miembro
3. Se lanza el destructor de la clase base

Este orden asegura que los objetos no se encuentran en estados indeterminados o
incoherentes.


#### Llamadas a destructores y constructores

Normalmente no se realizan llamadas directas ni a uno ni a otro, aunque las
llamadas a los constructores son más comunes porque están asociadas a la
inicialización.

Los destructores se ejecutan automáticamente como proceso de limpieza de los
objetos cuando éstos salen de *scope* o deben limpiarse por la razón que sea.
El único lugar donde tiene sentido llamarlos es cuando se desean eliminar
elementos de un contenedor que los almacene como `std::vector` o `std::array`.
En cuyo caso la llamada se realiza como a un miembro cualquiera.

Los constructores son un poco más complejos en ese sentido. Antes de C++11, los
constructores se llamaban de forma explícita para crear nuevas instancias de
los objetos:

``` cpp
Date a = Date(2020, 06, 23);

Date b (2020, 06, 23);          // Versión reducida
```

Desde C++11 con la introducción de la lista inicializadora, este tipo de
llamadas directas son menos frecuentes:

``` cpp
Date a {2020, 06, 23};
```

En realidad, ambos casos ejecutan el constructor pero en el primero de ellos se
ejecuta una llamada directa y explícita a éste. El segundo caso, aunque resulte
en la misma ejecución (pero sin aplicar *narrowing*) la mayoría de las veces,
no es siempre así.

#### Tipos de inicialización y constructores

A lo largo del documento se han usado varios tipos de inicializaciones pero no
se han explicado en detalle ya que la explicación requería del conocimiento de
los constructores. Se han analizado inicializaciones:

- Por miembro, suponiendo una estructura o clase de tipo `T`:

    ```cpp
    T a {1, "hola", 2};
    ```
- Por copia, suponiendo un tipo `T` y una variable `b` de tipo `T`:

    ```cpp
    T a = b;
    ```
- Por defecto, suponiendo un tipo `T`:

    ```cpp
    T a;
    ```

También se ha mencionado que en las inicializaciones por defecto en variables
`static` se inicializa a ceros.

Cuando se ha tratado sobre inicialización directa, se ha considerado que estos
dos casos son equivalentes:

- `T a {argumentos}`
- `T a (argumentos)`

Esto no es del todo cierto. La lista inicializadora también se conoce como
inicialización *universal* debido a que puede resultar en diferentes tipos de
inicialización, como en una inicialización por miembro, como en el primer
ejemplo de la lista con la que se abre la sección. Por otro lado, la
inicialización directa por llamada al constructor sólo puede resolverse a una
llamada a un constructor.

#### Constructores y destructores por defecto

Todas las clases creadas se rellenan automáticamente por el compilador,
añadiendo constructores, constructores copia y destructores por defecto. Éstos
actúan como en los tipos básicos, de la forma más sencilla posible.

En caso de que se defina un constructor, por muy concreto que sea, el
constructor por defecto deja de estar disponible y ya no es posible inicializar
por miembro. El constructor copia actúa por su cuenta en este caso, así que
definir un constructor no sobreescribe el constructor copia.

En las clases definidas manualmente, el constructor por defecto se ejecuta en
diferentes casos:

``` cpp
class C {
    //...
};
C a;        // Inicialización por defecto
C a();      // Inicialización directa por llamada de constructor
C a{};      // Inicialización directa por lista inicializadora
```

Los tipos básicos de C++ disponen de un constructor por defecto, pero éste no
se ejecuta en el primero de los casos por lo que su valor no es predecible.

``` cpp
int a;      // No ejecuta el constructor por defecto
```

En caso de que se declaren `static` el constructor sí que se llama,
inicializando las variables a cero o a un valor equivalente, como `nullptr` en
el caso de los punteros[^static-init-except].

[^static-init-except]: Esta excepción se debe a que sería muy peligroso
  permitir la creación de variables estáticas a valores indeterminados.

Si no se define un destructor, se considera que la clase define un destructor
que no realiza ninguna acción.

#### Constructores de lista

En algunos casos es interesante recibir en un constructor una lista completa de
elementos, tal y como `std::vector` es capaz de hacer. Para ello, es posible
definir un constructor que reciba `std::initializer_list` (es necesario incluir
`<initializer_list>`):

``` cpp
#include <initializer_list>

class C{
    C(std::initializer_list);
};
```

Pero esto conlleva un problema: las inicializaciones mediante la lista
inicializadora a los otros constructores no pueden realizarse con normalidad,
ya que el sistema de resolución de sobrecargas no sabría a cuál llamar. Para
resolver esto C++ dispone de las siguientes reglas:

- El constructor por defecto tiene preferencia
- Para que los constructores no se llamen en función de los elementos de la
  lista, siempre prefiere el constructor por lista a los constructores por
  argumentos.

Si en algún momento se requiere la inicialización por argumentos, ésta debe
llamarse directamente:

``` cpp
#include <initializer_list>

class C{
    C(int arg);
    C(std::initializer_list);
};

C a {1};    // Llama al constructor por lista
C b (1);    // Llama al constructor por argumentos
C c {};     // Llama al constructor por defecto
```

Este caso, de nuevo, recalca la diferencia entre la inicialización mediante
lista y mediante una llamada directa del constructor.

#### Conversiones mediante constructor

Los constructores también actúan como una forma de convertir un dato a otro
tipo:

``` cpp
std::string( "string" )  // convierte de `const char*` a `std::string`
```

Este tipo de conversiones se realizan de forma automática en C++ en infinidad
de ocasiones:

``` cpp
string a = "texto"     // Conversión implícita
string a = string("a") // Conversión explícita
```

Los constructores se tratan como constructores de conversión automáticamente, y
se usarán por el lenguaje siempre que sea posible aplicarlos
[^converting-constructors-c++11]. Un caso llamativo de este tipo de
conversiones es el de la resolución de sobrecarga de funciones:

``` cpp
void funcion (std::string){
    // ...
}

funcion("a");  // Conversión implícita a `std::string`
```

Este tipo de constructores se conocen como constructores-conversores
(*converting constructors*).

Si no se usan con cuidado, los constructores-conversores pueden llevar a
comportamientos indeseados. Marcar los constructores como explícitos, evita
este problema.

[^converting-constructors-c++11]: En C++11 los constructores conversores sólo
  podían tener un argumento de entrada. En versiones más modernas del estándar
  pueden tener cualquier cantidad.


##### Constructor explícito

Los constructores marcados con el especificador `explicit` no realizan
conversiones implícitas.

Los constructores `explicit` sólo se consideran durante inicializaciones
directas, pero no en inicializaciones por copia, como las aplicadas en llamadas
a funciones o en el uso del operador de asignación `=`.


#### Inicialización de miembros en constructor

Aunque se ha tratado mucho sobre constructores aún no se ha explicado cómo
deben definirse. A parte de su propia funcionalidad como función que son, los
constructores pueden añadir una lista de inicialización de miembros, que indica
los valores a asignar a sus datos miembro después de la lista de argumentos de
entrada:

``` cpp
class Date{
    int day, month, year;
    Date(int y, int m, int d) : year{y}, month{m}, day{d} {
        // ...
    }
};
```

Estas inicializaciones se ejecutan antes que el cuerpo del constructor.

Si alguno de los miembros no se inicializa, no es necesario añadirlo a la
lista.

La lista de miembros puede incluir también una clase base.

Es posible además usar este método para crear un constructor que delegue la
construcción en otro (conocidos como *forwarding constructor* o *delegating
constructor*).

``` cpp
class Date{
    int day, month, year;
    Date(int y, int m, int d) {
        //...
    };
    Date(int m, int d) : Date{2020, m, d} { } // Delega en el constructor más
                                              // completo
};
```

Los constructores delegados no pueden ser recursivos, así que la lógica debe
mantenerse sencilla.

### Copia y movimiento


La copia es una operación que clona un objeto sobre otro, por defecto en C++ se
copia el objeto a nivel binario, creando dos espacios idénticos en la memoria.
A nivel lógico, una copia tiene las dos siguientes propiedades:

- Equivalencia: Implica que los objetos copiados son iguales (o equivalentes) y
  que una comprobación de comparación (`a == b`) será verdadera.
- Independencia: Implica que los cambios efectuados en uno de los objetos no
  afecta al otro y viceversa. La falta de independencia implica que los objetos
  comparten elementos, o dicho de otra forma, que no se han copiado
  completamente.

La falta de independencia es un error habitual en C++ y en C:

``` cpp
struct intptr{
    int* value;
};

int *p {new int};
intptr a {p};      // a.value == p
intptr b = a;      // b.value == p
```

En el ejemplo, tanto `a.value` como `b.value` hacen referencia al mismo entero
final, debido a que lo que se ha clonado es únicamente el puntero. Cambiar el
valor de `*p` alteraría el entero al que tanto `a` como `b` se refieren. Puede
entenderse entonces que no son independientes.

Las copias son un proceso habitual en C++, todas las asignaciones que no son
inicialización directa se realizan mediante copias. Las llamadas a funciones,
ya sea en los argumentos de entrada como en el valor de retorno, aplican
copias, el uso de la asignación mediante el operador `=` también, e incluso las
excepciones usan copias para asignar la excepción a la sentencia `catch`
correspondiente.

El movimiento es un proceso distinto, en lugar de mantener la variable desde la
que se mueve inalterada, el movimiento transfiere el control del valor de la
variable a la variable objetivo, dejando la variable original en un estado
vacío o *movido*, pero no erróneo, para que el resto del programa libere sus
recursos.

El movimiento es un recurso efectivo para optimizar los programas ya que
permite reutilizar variables o valores que de otro modo fueran a destruirse
ahorrando tiempo y memoria en el proceso de copiado.

Históricamente el movimiento no ha estado disponible más que como optimización
del compilador, que era aplicada en contadas ocasiones cuando se realizaba una
copia que lo permitiera, normalmente en las sentencias `return`. Desde C++11,
con la introducción de lo que se conoce como *move semantics* el comportamiento
del movimiento puede definirse de forma manual, permitiendo que el programa
ejecute movimientos de forma controlada y efectiva.

#### Copia

Las copias pueden definirse de dos modos. Suponiendo el tipo `T`:

- Mediante un constructor copia: `T(T&)`
- Mediante un operador de asignación: `T& operator=(T&)`

La diferencia entre ellos radica en que el operador puede aplicarse sobre
objetos que ya tengan valores asignados y puede que haya adquirido recursos
previamente, mientras que el constructor copia sólo sirve durante la
construcción:

``` cpp
int a = 1;   // Construye desde un int

a = 2        // Copia desde un int, pero `a` ya dispone de un valor asignado
```

Sólo tiene sentido redefinir estas operaciones en casos en los que la operación
de copia por defecto, la copia elemento por elemento, no funcione como se
espera. El caso más habitual es el de los contenedores, como `std::array`, que
requieren de una gestión manual de todos los elementos, alquilando la memoria
correspondiente de cada uno, asegurándose así que son independientes.

En caso de tratarse de clases heredadas, es necesario copiar sus bases dentro
del constructor de copia.

Como constructor que es, el constructor copia puede definir una lista de
inicialización de miembros.

#### Movimiento

El movimiento, de forma similar a la copia, se puede describir de dos modos.
Suponiendo el tipo `T`:

- Mediante un constructor de movimiento: `T(T&&)`
- Mediante un operador de asignación por movimiento: `T& operator=(T&&)`

La diferencia entre ellos es idéntica al caso de la copia. Puede apreciarse
además que sus argumentos de entrada son referencias a *rvalue* en esta
ocasión, ya que el movimiento sólo puede realizarse en elementos que fueran a
destruirse posteriormente debido a que es una operación destructiva.

La operación de movimiento debe traspasar las referencias al objeto creado o
asignado y dejar el objeto de origen en un estado válido, a poder ser vacío,
para que la operación de eliminación funcione satisfactoriamente y en el menor
tiempo posible.

``` cpp
struct string {
    char * value;

    // Constructor de conversión, desde un string tipo C
    string(const char * s){
        size_t len = std::strlen(s) + 1; // Calcula la longitud completa del
                                         // string tipo C, incluyendo '\0'
        value = new char[len];
        for(int i = 0; i != len; i++){
            value[i] = s[i];
        }
    }

    // Destructor, elimina los datos cargados dinámicamente
    ~string(){
        delete[] value;
    }

    // Constructor copia
    string(string &s){
        // Copia los elementos del string uno por uno
        size_t len = std::strlen(s.value) + 1;
        value = new char[len];
        for(int i = 0; i != len; i++){
            value[i] = s.value[i];
        }
    }

    // Constructor de movimiento
    string(string &&s){
        // Mueve el puntero
        value = s.value;

        // Asigna un estado "movido" al original, permitiendo que pueda
        // borrarse. No efectuar esta acción llevaría a una eliminación
        // prematura.
        s.value = nullptr;
    }
};

// Construcción por conversión
string a {"texto"};

// Construcción mediante copia
string b {a};  // Equivalente a `string b = a;`

// Creación desde rvalue, no aplica movimiento por defecto, sino copia
string c { string("texto") };

// Movimiento
string d { std::move(string("texto")) };
```

El ejemplo muestra el uso de `std::move`, que sirve para especificar que en esa
ocasión puede aplicarse un movimiento en lugar de una copia.

El resto del ejemplo, muestra los diferentes modos de copiar y crear el objeto.
Se aprecia que tanto en el constructor copia como en el constructor de
conversión se copia el objeto original elemento por elemento, mientras que en
el de movimiento se captura el contenido, dejando el original vacío.

En caso de no vaciar el objeto original asignando `nullptr`, éste se llevaría
consigo el contenido al ser destruido, causando una eliminación prematura.


### Operaciones por defecto y eliminadas

En ocasiones es interesante mantener constructores por defecto a pesar de que
se hayan definido otros. Aunque el comportamiento clásico de C++ es eliminar
los constructores por defecto siempre que uno haya sido creado, es posible
declararlos en el cuerpo de la clase añadiendo el pseudoinicializador `=
default`. Esto hace que no sea necesario definir los constructores por defecto
de forma manual en caso de que se defina un constructor, evitando la
posibilidad de definirlos de forma errónea.

También es posible deshabilitar operaciones mediante `= delete`. Cualquier
función miembro marcada como `= delete` lanzará un error al tratar de
ejecutarse. El uso más útil de la eliminación es el de deshabilitar el
movimiento o la copia de una clase.


## Sobrecarga de operadores

Al igual que las funciones, los operadores pueden sobrecargarse. La sobrecarga
de operadores se realiza mediante funciones de nombre `operator` seguido por el
operador que desea sobrecargarse. No es posible crear operadores nuevos, sólo
sobrecargar operadores ya aportados por el lenguaje.

No todos los operadores pueden sobrecargarse. Los operadores `::`, `.` y `.*`
no permiten la sobrecarga. Tampoco pueden sobrecargarse los operadores como
`sizeof`, que aportan información importante y que no tiene sentido alterar. El
operador condicional (`X ? Y : Z`) tampoco puede sobrecargarse.

Además, sobrecargar operadores añade ciertas restricciones. Los operadores que
aplican evaluación de cortocircuito (*short-circuit evaluation*) como `&&` y
`||` dejan de aplicarla si se sobrecargan.

Como detalle a remarcar, los operadores `new` y `delete`, y sus versiones para
*arrays*, pueden sobrecargarse.

### Operadores Independientes

Algunos operadores pueden sobrecargarse de forma independiente a modo de
funciones sobrecargadas. La resolución de funciones será la encargada de
seleccionar el operador adecuado.

``` cpp
struct el{
    int value;
};

el operator+(el a, el b){               // Sobreescribe la suma
    return el {a.value - b.value};      // En lugar de sumar resta
}

el a{10};
el b{10};

a + b;                      // == el{0}, porque ejecuta `operator+`
```

No todos los operadores pueden sobrecargarse de este modo, porque muchos de
ellos dependen de que exista un objeto que pueda alterarse.


### Operadores miembro

Los operadores pueden definirse en el interior de una clase, como miembros de
ésta. Éstos asignan los argumentos de entrada de forma diferente, pero son
igualmente sencillos de definir:

``` cpp
struct el{
    int value;
    el operator+(el b){                   // Sobreescribe la suma
        return el {value - b.value};      // En lugar de sumar resta
    }
};

el a{10};
el b{10};

a + b;             // == el{0}
```

A diferencia de los operadores definidos de forma independiente, los operadores
miembro tienen acceso al objeto (son funciones miembro) y definen sus
argumentos de diferente modo, en este caso siendo el primer argumento el primer
operando de la suma.

### Asignación de argumentos

La asignación de argumentos a las funciones que definen la sobrecarga es
bastante natural, pero en algunos casos puede resultar menos evidente. La
siguiente tabla resume el comportamiento de los operadores más comunes,
suponiendo un operador imaginario `@`, que debe sustituirse por el operador
unario o binario correspondiente.

| Operador    | Operadores miembro     | Operadores independientes   |
|-------------|------------------------|-----------------------------|
| `@a`        | `(a).operator@()`      | `operator@(a)`              |
| `a@b`       | `(a).operator@(b)`     | `operator@(a, b)`           |
| `a=b`       | `(a).operator=(b)`     | No es posible               |
| `a(b...)`   | `(a).operator()(b...)` | No es posible               |
| `a[b]`      | `(a).operator[](b)`    | No es posible               |
| `a->`       | `(a).operator->()`     | No es posible               |
| `a@`        | `(a).operator@(0)`     | `operator@(a, 0)`           |

### Operadores especiales

La tabla muestra algunos casos singulares:

- Operador de ejecución de función, `()`, sirve para llamar a un objeto como si
  se tratara de una función. Crea objetos-función, como pueden ser las
  funciones lambda. Los objetos que tengan definido este operador son
  candidatos a ser descritos por el tipo `std::function`.
- Operador de acceso (*subscript operator*), `[]`, permite a un objeto
  comportarse como un *array*.
- Operador de de-referencia, `->`, define el acceso a un miembro de un puntero
  por lo que es extremadamente útil a la hora de crear *smart-pointers*. Es
  necesario recordar que se trata de un operador unario, independiente del
  identificador del que venga acompañado: `a->b` es equivalente a
  `(a.operator->())->b`.

#### Operadores de *free store*

Otros operadores especiales no mostrados en la tabla son `new`, `new[]`,
`delete` y `delete[]`, que también tienen un comportamiento especial. Todos
estos operadores se comportan como miembros estáticos (`static`), por lo que no
tienen acceso a `this`.

``` cpp
void* operator new(size_t);
void* operator new[](size_t);

void operator delete(void*, size_t);
void operator delete[](void*, size_t);

// NOTA: size_t es un entero sin signo, el tipo de valor retornado por `sizeof`
```

Los operadores `new` y `new[]` alquilan la memoria del tamaño recibido y
devuelven un puntero a ésta. Mientras que operadores `delete` y `delete[]`
liberan la memoria alquilada mediante los operadores sobrecargados `new` o
`new[]` de la misma clase a partir de su puntero y su tamaño.

Es necesario destacar que estos operadores no son lo mismo que el constructor y
el destructor, que se seguirán llamando en cualquier caso.

También es posible sobrecargar estos operadores de forma independiente, lo que
sobreescribiría los operadores para cualquier clase. No se recomienda, sin
embargo, porque alguna clase puede suponer que estos operadores se comportan de
la forma habitual y sus suposiciones ya no serían ciertas, lo que podría
conllevar comportamientos inesperados.

#### Definición de literales

Tal y como el literal `10ULL` es un `unsigned long long`, desde C++11 es
posible crear literales nuevos que tengan un comportamiento similar. La
sobrecarga del operador literal permite la construcción de otros tipos de
datos a partir de literales definidos manualmente o añadir funcionalidades
adicionales a tipos ya conocidos.

``` cpp
// Literal en grados para expresar radianes
long double operator"" _deg ( long double deg )
{
    return deg * 3.14159 / 180;
}

180_deg; // == 3.14159
```

En el ejemplo se construye un tipo de literal nuevo `_deg` capaz de convertir a
radianes. En este caso se trata de un operador independiente que convierte a
`long double`, aunque también puede usarse en un operador miembro para
construir una clase del tipo adecuado.

Del mismo modo, pueden crearse *strings*:

``` cpp
#include <string>

std::string operator "" _s(const char* string, long unsigned size){
    return std::string(string);
}

"hola"_s; // std::string ("hola");
```

En ambos ejemplos se aprecia el uso de la barra baja (`_`) para añadir el
sufijo literal. El uso de sufijos sin barra baja está reservado para futuras
estandarizaciones.


## Herencia

La herencia es un proceso mediante el que se puede crear una clase partiendo de
la definición de otra u otras. La clase creada se conoce como clase derivada
(*derived class*) y la clase o las clases de origen se conocen como clases base
(*base class*).

Este proceso ayuda a la reutilización de código, pero también a una correcta
conceptualización de los tipos creados. El siguiente ejemplo muestra un ejemplo
de esto: los rectángulos (`Rectangle`) son polígonos (`Polygon`), y los
triángulos (`Triangle`) también son polígonos. A nivel conceptual un polígono
puede o no tener sentido en nuestro programa como una entidad propia, pero sí
que puede servir como base para describir como deben comportarse los polígonos
de forma general, aunque cada caso concreto, como el triángulo o el rectángulo,
se comporten cada uno de su manera específica.

``` cpp
class Polygon {
    // ...
};
class Rectangle : Polygon {
    //          ^^^^^^^^^  Hereda de Polygon
    // ...
};
class Triangle : Polygon {
    //         ^^^^^^^^^   Hereda de Polygon
    // ...
};
```

Las clases derivadas no son más que una extensión de las clases base, por lo
que no hay ningún tipo de coste adicional de recursos por usarlas y se permite
apuntar con punteros de la clase base a instancias de la clase derivada. El
*polimorfismo* permite el uso de clases base y derivadas de forma que cada una
de ellas actúe a su modo, pero tratándolas de forma uniforme a través de
punteros a la clase base, lo que aporta una flexibilidad muy interesante.

El conocimiento profundo de la herencia es primordial para poder realizar
diseños elegantes en un lenguaje tan estricto como éste. Este apartado trata de
introducir todos los conceptos básicos necesarios para conocer la herencia en
suficiente profundidad como para adquirir un conocimiento intuitivo sobre el
uso de clases, pero los patrones de diseño y la capacidad de comprensión de
programas complejos sólo pueden conseguirse mediante la práctica.

La herencia sólo puede realizarse en caso de que la clase base se encuentre
definida en el *scope* de la clase derivada. No es suficiente con que la clase
esté declarada. Es por eso que los archivos de cabecera suelen incluir las
definiciones de clases, aunque después definan su funcionalidad en el archivo
de código fuente.

El control de acceso (ver *[Control de acceso]*) afecta de forma directa en la
herencia:

- Los miembros `public` pueden accederse desde la clase derivada y desde el
  exterior.
- Los miembros `private` no pueden accederse desde la clase derivada ni desde
  el exterior.
- Los miembros `protected` pueden accederse desde la clase derivada, mediante
  miembros o amigos (`friend`), pero no desde el exterior.

Cabe destacar que el uso extendido de miembros `protected` suele indicar un mal
diseño, ya que cualquiera que desee acceder esos miembros no tiene más que
crear una nueva clase derivada. Estos miembros están diseñados exclusivamente
con el objetivo de definir interfaces entre las clases base y las derivadas.
Muchos casos de miembros `protected` deberían declararse en realidad `private`.

La propia herencia también puede disponer de control de acceso, pero ese caso
se explica más adelante (ver *[Control de acceso a la clase base]*), porque
requiere de conceptos adicionales para poder comprenderse en detalle.

### Constructores en herencia

Cuando se realiza una herencia, es posible construir las clases base o los
miembros de la clase derivada, pero no es posible inicializar los miembros de
la clase base. El objetivo es el de delegar la construcción al constructor
correspondiente, y no interferir en él ya que interferencias de este tipo
pueden alterar el comportamiento esperado de las clases base.

Los destructores actuarán del mismo modo, destruyendo lo que les corresponda,
pero sin afectar de forma directa a miembros de las clases base.

### Sobrecarga a través del *scope*

Como las clases son *namespaces* sus miembros no son capaces de traspasar el
*scope*.

``` cpp
struct B {
    //...
    void func (void);
};
struct D : public B {
    //...
    void func (int);
};

D d;
d.func(); // ERROR: no hay sobrecarga, intenta llamar a `D::func(int)`
```

#### Alias como miembro

Es posible traer miembros al *scope* de la clase mediante `using` para
poder sobrecargarlos (*overload*) de forma correcta.

``` cpp
struct B {
    //...
    void func (void);
};
struct D : public B {
    //...
    using B::func;
    void func (int);
};

D d;
d.func(); // Llama a `B::func(void)`
```

No es posible usar `using` de modo que extraiga todos los miembros de la clase
base, es necesario aplicar `using` de forma individual por nombre. Eso sí, cada
nombre podría hacer referencia a muchas funciones debido a la sobrecarga.

Este método puede usarse para traer constructores de la clase base a la clase
derivada. Es útil en casos en los que la clase derivada no añade nuevos datos
miembro pero peligroso en caso contrario.

### Funciones miembro virtuales

Al declarar una función como `virtual`, ésta puede sobreescribirse en la clase
derivada con un comportamiento distinto. Posteriormente, si se llama a la
función virtual mediante un puntero o referencia a la clase base, la llamada es
capaz de resolver a la función sobreescrita si el objeto al que se hace
referencia es de la clase derivada.

``` cpp
class B {
    //...
    public:
    virtual void func (void);
};
class D : public B {
    //...
    public:
    void func (void);
};

B base;
base.func();          // Llama a B::func()

D deriv;
B &base_ref = deriv;
base_ref.func();      // Llama a D::func()
```

Para llamar a la versión de la clase base es necesario usar su nombre completo,
siguiendo con el ejemplo previo:

``` cpp
base_ref.B::func();   // Llama a B::func()
```

El hecho de que el lenguaje es capaz de encontrar la función adecuada
independientemente del tipo real asignado a la referencia o puntero se conoce
como *polimorfismo*. Las clases clases con funciones virtuales se conocen como
tipos con polimorfismo en tiempo de ejecución (*run-time polymorphic type*) ya
que el proceso de selección de la función adecuada ocurre en momento de
ejecución.

Para poder aplicar el polimorfismo en tiempo de ejecución el compilador
necesita conocer el tipo de los objetos, lo que suele requerir de un campo
adicional en cada uno de ellos (normalmente de pequeño tamaño, pero depende de
la implementación). Una vez conocido el tipo, el compilador debe crear una
lista de funciones virtuales (conocida comúnmente como `vtbl`) para cada clase
virtual. Esto provoca una ligera pérdida de rendimiento que no es especialmente
significativa para programas con requerimientos normales[^perf-virtual].

[^perf-virtual]: En casos con requerimientos especiales hay muchas más cosas
  que comprobar antes que tratar de optimizar un mecanismo básico del lenguaje.

A la hora de crear clases base, llamar a funciones virtuales desde
constructores o destructores es generalmente una mala idea, aunque es posible y
no supone ningún aviso por parte del compilador. Una clase derivada podría
sobreescribir las funciones virtuales y deformar el comportamiento de éstos
haciéndolas fallar y en la etapa de compilación no se recibirían avisos de este
posible comportamiento erróneo.

#### Sobreescritura de funciones

Las funciones que sobreescriban funciones virtuales se crean automáticamente
como virtuales aunque no se especifique `virtual` en su declaración. No se
recomienda indicarlas como `virtual` de todas formas, ya que podrían dar a
parecer que es la primera vez que se declaran.

No es necesario sobreescribir todas las funciones virtuales de una clase base
si no se necesita. Puede que algunas cumplan la funcionalidad correctamente o
que no vayan a usarse, en esos casos, basta con no sobreescribirlas, con lo que
se llamaría siempre la de la clase base, o eliminarlas con `=delete`, lo que
lanzaría un error al intentar llamarlas.

Para que una función se sobreescriba, debe ser igual a la declarada en la clase
base, es decir: mismo nombre y mismos argumentos. Si no lo fuera, no se
sobreescribiría sino que la clase derivada tendría dos funciones distintas. El
compilador no avisará sobre ello, porque el comportamiento es correcto.

Para resolver ambos problemas puede usarse el especificador `override` (desde
C++11) que al indicar que con la declaración se pretende sobreescribir una
función virtual permite al compilador comprobar si realmente aplica una
sobreescritura y avisar si no es así y, por otro lado, indica a otras personas
que trabajen en el programa que la función es también `virtual`, al estar
sobreescribiendo otra función.

El especificador `final` sirve para indicar que la función no puede seguir
siendo sobreescrita. Es útil pero abusar de él puede ser peligroso porque rompe
la interfaz hacia las posibles nuevas clases derivadas.

Ambos especificadores, `override` y `final`, son especiales. No forman parte de
la función y sólo pueden usarse en la declaración de ésta, después del
declarador, dentro de la clase. Son palabras reservadas contextuales que sólo
están reservadas en el cuerpo de las clases lo que permite su uso como
indicadores fuera de ellas aunque no se recomiende.

```cpp
class B {
    //...
    public:
    virtual void func_void (void);

    virtual void func_int (int);
            //             ^^^
};
class D : public B {
    //...
    public:
    void func_void (void) override; // Sobreescribe `func_void`

    void func_int (void) override;  // ERROR: no sobreescribe
            //     ^^^^ diferentes argumentos de entrada
};
```


##### Relajación del tipo de retorno

Como las funciones virtuales están diseñadas para poder sobreescribirse en las
clases derivadas, relajan su tipo de retorno para poder encajar de forma más
correcta.

Cualquier función virtual que retorne un puntero o referencia a la clase base,
puede ser sobreescrita por una función que retorne un puntero o referencia de
la clase derivada. Este concepto también se conoce como regla de retorno
covariante (*covariant return rule*)[^covariance].

```cpp
class B {
    virtual B& f(void);
//          ^^
};
class D : B {
    D& f(void);  // Sobreescribe `B::f` aunque el tipo de retorno sea `D&`
//  ^^
};
```

[^covariance]: La covarianza y la contravarianza, en el contexto de la
  programación, determinan cómo se puede sustituir un tipo por su tipo base o
  por su derivado.

Este tipo de sobreescrituras tiene sentido principalmente en clases que puedan
clonarse o crearse y se hereden. La sobreescritura podría seguir creando una
instancia del tipo correcto gracias a la relajación del tipo de retorno.

A las funciones virtuales capaces de crear una nueva instancia se las suele
conocer como constructores virtuales (*virtual constructor*) a pesar de que no
son realmente constructores. Los constructores no son funciones clásicas: no
pueden ser virtuales y no puede capturarse su dirección.

##### Clases abstractas

Las funciones miembro virtuales pueden declararse como virtuales puras mediante
el pseudoinicializador `=0`. Las clases que incluyen una función miembro
puramente virtual se consideran clases abstractas y no pueden crearse
instancias de ellas.

El objetivo de estas clases es el de definir interfaces sin necesidad de
implementarlas. Es especialmente útil para definir conceptos abstractos, de ahí
su nombre. Ejemplos de su uso pueden ser la definición de una clase abstracta
para representar una forma geométrica genérica que después se deriva para crear
polígonos, circunferencias, etc. en un programa de diseño asistido por
ordenador (CAD) o la creación de una clase abstracta para gestionar conexiones,
que pueda ser heredada más adelante para definir protocolos concretos.

Las clases derivadas que no sobreescriban todas las funciones abstractas de la
clase base también se consideran abstractas, habilitando la construcción de
interfaces de forma gradual.

```cpp
class B {                     // Clase abstracta
    virtual void f(void) =0;  // Función virtual pura
};
```

En clases abstractas tiene sentido declarar un destructor virtual, ya que las
clases abstractas están diseñadas para usarse como base para el *polimorfismo*.
Sin embargo, como no pueden crearse instancias a de clases abstractas, no tiene
sentido declarar un constructor en ellas.


### Control de acceso a la clase base

La creación de clases derivadas también dispone de un mecanismo de control de
accesos:

``` cpp
class D : public B { /*...*/ };
class D : protected B { /*...*/ };
class D : private B { /*...*/ };
```

En caso de tratarse de clases, las clases base se heredan por defecto como
`private`, mientras que en las estructuras se heredan como `public` en caso de
no indicar ningún control de acceso concreto. Tal y como ocurre con el control
de acceso en miembros.

Este control de accesos en la clase base altera el comportamiento de la
herencia del siguiente modo:

- `private`: los miembros `public` y `protected` de la clase base sólo pueden
  usarse por miembros o amigos (`friend`) de la clase derivada. Éstos son,
  además, los únicos capaces de convertir de la clase derivada a la base.
- `protected`: igual que `private`, pero la definición se extiende del mismo
  modo a las clases derivadas de la derivada.
- `public`: los miembros públicos (`public`) de la clase base pueden usarse
  libremente, incluso desde el exterior de la clase derivada. Los protegidos
  (`protected`) pueden usarse por clases derivadas de cualquier generación.
  La conversión a la clase base puede realizarse en cualquier lugar, sin
  restricciones.

Cada nivel de acceso aporta un control distinto que puede ser útil en
diferentes situaciones.

Estas indicaciones de control de acceso son perfectamente compatibles con la
herencia múltiple, pero deben indicarse por cada clase base.


## Herencia múltiple

Cada clase puede heredar desde varias clases, añadiendo la lista de clases de
las que heredar en la definición de la clase.

```cpp
class B1 {
    //...
};
class B2 {
    //...
};

class D: B1, B2 {
    // Hereda de `B1` y `B2`
};
```

En caso de que `B1` y `B2` hayan definido miembros con el mismo nombre, la
clase derivada tendrá que indicar a cuál se refiere concretamente usando el
nombre completo:

```cpp
class B1  {/*...*/};
class B2  {/*...*/};
class D: public B1, public B2 {/*...*/};

D d;
d.B1::member;  // Usa el miembro `member` de B1
d.B2::member;  // Usa el miembro `member` de B2
```

Acceder al miembro con nombre completo puede dar lugar a comportamientos
difíciles de controlar, como que, por ejemplo, se acceda a una clase base de la
clase base en casos de árboles de herencias muy profundos.

Para evitar problemas de este tipo se recomienda sobreescribir los campos que
tengan nombres idénticos mediante el mecanismo de funciones virtuales. Además,
este mecanismo asegura la sobreescritura de los miembros de todas las clases
derivadas que cumplan con la descripción de la función.

### Bases virtuales

Cuando el árbol genealógico de clases es muy profundo y se realizan herencias
múltiples, es posible que diferentes ramas de éste acaben llegando a tener la
misma clase base, quedando repetida entre las clases base. Declarar una
herencia como `virtual` evita replicaciones de la clase base en el árbol de
herencias y hace que cualquier referencia que se haga a ella sea siempre al
mismo elemento.

``` cpp
struct D1 : virtual B {/*...*/};
struct D2 : virtual B {/*...*/};
struct DD : D1, D2 {
    // La clase `B` aparece dos veces en la herencia, una por `D1` y otra por
    // `D2`. Al ser virtual sólo se construye una vez.
};
```

Para que las clases virtuales puedan funcionar correctamente, sus constructores
se ejecutan antes que ningún otro.

Por otro lado, la sobreescritura de miembros de la clase virtual puede resultar
extraña si no se realiza con cuidado, por ambas ramas (`D1` y `D2` en el
ejemplo), aunque esto ya queda en manos de quien está programando.

## Punteros a miembros

Los punteros a miembros son un modo de trabajar con miembros de forma genérica,
permitiendo indicar a los objetos a cuál de sus miembros acceder o llamar
mediante una variable.

El concepto es similar a cualquier otro puntero y su comportamiento es
parecido, pero aunque se llamen «punteros a miembros» no son técnicamente
punteros sino el salto desde el inicio del objeto al miembro al que se debe
acceder. Es decir, no indican una dirección de memoria, sino la distancia entre
la dirección de la clase y la del miembro concreto.

Para comprender el concepto se puede hacer una analogía con los *array*: en un
*array* de `char` de `10` elementos, `char ar[10]`, al acceder al elemento
número `5` no se indica la dirección concreta del elemento, sino la distancia
desde el inicio del *array*. La dirección del elemento `5` sería `ar + 5`,
mientras que la distancia es, simplemente, `5`. En caso de tener otros *array*
como éste, esa distancia (`5`) es perfectamente aplicable para cada uno de
ellos, porque no trata de una dirección absoluta, sino de un incremento.

Los punteros a miembros son exactamente el mismo caso, pero en datos con tipos
que no son uniformes, como son las clases.

Al no ser punteros reales, no se pueden asignar a variables declaradas `void*`,
pero soportan polimorfismo así que pueden asignarse a punteros a miembros de la
clase base y son capaces de llamar a funciones virtuales sobreescritas. No es
posible asignar punteros de la clase base a punteros declarados desde la clase
derivada, ya que la clase derivada puede tener miembros que la clase base no
tenga.

```cpp
class C { /*...*/ };

C::* member_ptr = &C::member; // Declaración, supone que C dispone de un
                              // miembro llamado `member`

C obj;
obj.*member_ptr;              // Acceso mediante objeto

C* objptr = &obj;
objptr->*member_ptr;          // Acceso mediante puntero a objeto
```

Puede observarse que en la declaración no se usa ningún objeto particular, sólo
la propia definición de la clase. Este es el verdadero interés de los punteros
a miembro. Pueden generarse punteros a miembros de forma completamente genérica
que luego funcionen en cualquier instancia de la clase (o de clases derivadas)
desde la que se crearon.

Es necesario recordar que en caso de que se trate de una función miembro, el
puntero a miembro debe ser declarado correctamente, indicando los argumentos y
tipos de retorno de la función (p.ej. `void (C::*)(int, float)`).


## Tipos y conversiones

C++ aplica, como ya se ha introducido, conversiones implícitas y explícitas.
Las primeras ocurren de forma automática, sin que deban expresarse de forma
directa en el código, mientras que las segundas deben ser especificadas
manualmente.

Ya se han presentado mecanismos para facilitar las conversiones implícitas y
explícitas: los constructores de conversión y los constructores `explicit`.
Pero C++ aporta otros métodos para la conversión explícita que no se han
visitado aún.

- La inicialización directa mediante lista inicializadora, ya estudiada.
- Las conversiones con nombre como `const_cast`, `reinterpret_cast`,
  `dynamic_cast` y `static_cast`.
- Las conversiones (*cast*) tipo C que no se han estudiado y tienen el
  siguiente aspecto:

    ``` cpp
    (int) 1.2;    // Convierte `1.2` a `int`, es decir: `1`
    ```
- Notación funcional para *cast*s tipo C:

    ``` cpp
    int(1.2);     // Convierte `1.2` a `int`, es decir: `1`
    ```

Las conversiones estilo C no son seguras porque puede realizar conversiones
peligrosas sin ser demasiado explícita en ellas, como convertir de tipos
`const` a tipos que no lo son o de punteros de una clase a una clase privada
del mismo tipo. Es por eso que no se recomiendan en C++, que dispone de
suficientes mecanismos de más alto nivel para realizarlas.

La notación funcional es similar a la notación tipo C, pero más fácil de leer,
debido a que el uso de los paréntesis es uniforme con el resto del lenguaje.
Para los tipos básicos es igual de insegura que la notación tipo C, pero para
tipos creados es realmente una llamada a su constructor, lo que puede ser una
operación segura si se hace correctamente. Se recomienda, como ya se ha venido
haciendo desde el inicio del documento, usar la inicialización mediante lista
inicializadora siempre que sea posible para evitar el *narrowing*, aunque, como
ya se detalla al tratar los constructores, no siempre es posible.

Las conversiones con nombre son un mecanismo poderoso. Son explícitas así que
muestran la intención de convertir un valor a otro tipo y describen diferentes
modos de conversión:

- `const_cast` es capaz de convertir entre tipos que sólo difieren en los
  calificadores `const` y `volatile`[^volatile].
- `static_cast` convierte entre tipos relacionados y también usa conversiones
  basadas en constructores.
- `dynamic_cast` convierte entre punteros y referencias en la jerarquía de
  clases en tiempo de ejecución.
- `reinterpret_cast` realiza conversiones entre tipos no relacionados (de
  punteros a enteros, de punteros a punteros de otros tipos no relacionados,
  etc).

[^volatile]: El calificador `volatile` no se ha estudiado durante el documento.
  En resumen, indica al compilador que los objetos volátiles realizan efectos
  secundarios. Esto es útil para el compilador, ya que en ocasiones reordena
  las órdenes para aumentar el rendimiento. Reordenar operaciones con efectos
  secundarios puede resultar catastrófico, así que `volatile` sirve como guía
  de qué órdenes no pueden reorganizarse con total libertad. Un caso
  catastrófico puede ser la escritura y lectura de un archivo, cambiar el orden
  de esas acciones cambia completamente el comportamiento del programa, pero el
  compilador no puede conocer esto de antemano así que es necesario
  indicárselo.

### Información de tipo en tiempo de ejecución (RTTI)

La información de tipo en tiempo de ejecución o RTTI (*run-time type
information*) permite obtener información sobre el tipo de las variables en
tiempo de ejecución aportando capacidades adicionales.


#### Conversión dinámica

`dynamic_cast` es un caso de RTTI que convierte de un tipo a otro en su
jerarquía de clases. Este tipo de conversiones suelen conocerse como *upcast*
cuando convierten de una clase derivada a una de sus bases, *downcast* cuando
se convierte de una base a una clase derivada o *crosscast* cuando se convierte
a clases en la misma altura en la jerarquía.

`dynamic_cast` es capaz de convertir punteros y referencias mediante los tres
tipos de conversiones, sin embargo, algunas de estas operaciones pueden fallar.
Un *upcast* siempre será posible debido al polimorfismo, pero un *downcast* no
siempre puede asegurarse. La conversión dinámica realiza una simple asignación
cuando aplica *upcasts*, pero requiere de más trabajo cuando hace otras
operaciones. En cualquiera de los dos casos, estas comprobaciones deben
realizarse en tiempo de ejecución, de ahí el interés de disponer de RTTI.

Las llamadas a las funciones de conversión con nombre son similares, en el caso
de `dynamic_cast` se entregan dos argumentos, el tipo al que se desea convertir
entre `<>` y el objeto que se desea convertir como argumento de entrada:

``` cpp
struct B {
    virtual void f(void);  // clase polimórfica
};
struct D : B {
    void f(int);
};

B* base_ptr = new B;
D* derived_ptr = dynamic_cast<D*>(base_ptr);
```

Cuando la conversión no funciona, cosa que puede ocurrir, `dynamic_cast` debe
retornar algún valor de error. En el caso de los punteros retorna `nullptr`,
pero en el caso de tratar de convertir referencias esto no puede ocurrir, así
que lanza una excepción.

El funcionamiento de `dynamic_cast` puede entenderse como una pieza de código
capaz de preguntarle a un objeto si dispone de una interfaz concreta.

#### Conversión estática

`static_cast` es mucho más parecido a un *cast* tipo C. Es capaz de usarse
sobre `void*`, cosa que `dynamic_cast` no puede hacer. Sin embargo,
`static_cast` no realiza chequeos en tiempo de ejecución y no es capaz de hacer
conversiones como las que `dynamic_cast` puede realizar. También es cierto que
eso implica que tiene menor impacto en el rendimiento del programa, al no
introducir código en el programa resultante.

La conversión estática confía en su buen uso así que debe usarse con cautela.

#### Otros modos de conversión

Tanto la conversión estática como dinámica respetan los calificadores como
`const`, así que deben usarse alguno de los otros métodos que el lenguaje
aporta si se desea alterar este calificador (`const_cast`).

### Identificadores de tipo

El operador `typeid()` es capaz de extraer información del tipo de los objetos.
Retorna un elemento del tipo `const type_info&`, definido en `<typeinfo>`.

Este operador puede aplicarse sobre cualquier expresión o nombre de tipo para
obtener su `type_info`.  En casos en los que se envíen valores inesperados
lanzará una excepción `std::bad_typeid`.

`type_info` almacena información interesante, pero debe usarse cuidadosamente
ya que se puede esperar que su función miembro `type_info::name()` retorne el
nombre del tipo tal y como se escribe en el programa y no es necesariamente
así.

### Recomendaciones

Estas utilidades son interesantes, pero no deben usarse más de la cuenta.
Confiar en el compilador es siempre más seguro y el lenguaje está diseñado con
esa idea.

Quien tenga costumbre de programar en lenguajes dinámicos puede sentir la
tentación de comprobar el tipo de los objetos para ejecutar diferentes
funciones en función de éste. Para eso existe el polimorfismo. En la mayor
parte de los casos, no hay ninguna necesidad de usar este tipo de funciones
para una labor como esa.



## Plantillas

Durante todo el documento y especialmente en éste último capítulo se han
tratado los tipos de forma concreta, lo que limita sobremanera la capacidad de
expresar código genérico. Se han visitado algunos conceptos capaces de
facilitar esta tarea, como son la sobrecarga de funciones y operadores o las
macros. Pero la sobrecarga de funciones sigue necesitando de una
reimplementación de las funciones y las macros no se recomiendan. Las clases
abstractas y el polimorfismo en tiempo de ejecución simplifican la tarea de
crear código genérico, pero siguen dependiendo de la jerarquía de clases
declarada, impidiendo mezclar tipos que han sido declarados de forma
independiente.

Las plantillas vienen a solucionar este problema, permitiendo crear clases o
funciones plantilla, cuya definición dependa de un tipo enviado en su
declaración. De este modo crear código genérico es tan sencillo como declarar
un tipo hipotético y a la hora de aplicar la plantilla designar para qué tipo
quiere aplicarse. Evidentemente, esto no soluciona todos los problemas, porque
de un tipo a otro la interfaz que éste exponga al exterior puede ser diferente,
pero son detalles menores una vez resuelto el problema principal.

Más allá de la programación genérica, las plantillas pueden entenderse como un
mecanismo de *metaprogramación*, concepto que se define como la programación de
la programación. Es decir, la capacidad de crear un programa mediante un
programa. Entendidas las plantillas como un lenguaje de programación que se
ejecuta en tiempo de compilación y explotando sus capacidades al máximo, es
posible crear programas extremadamente avanzados con ellas que incluso sean
capaces de comprobar los tipos correctamente.


### Clases plantilla

Las plantillas tienen un funcionamiento sencillo a simple vista:

``` cpp
template <typename T>  // Declaración de plantilla
class array{           // Clase plantilla
    T *contents;       // Uso del tipo de la plantilla

    array();
    void put(T el);
    T get(int pos);
};


array<int>   ar1;      // Instanciación de array, para `int`
array<float> ar2;      // Instanciación de array, para `float`
```

La plantilla declarada recibe un argumento de entrada declarado mediante
`typename T`, siendo `T` el nombre a usar para el tipo y `typename` una palabra
clave reservada para la declaración de argumentos de plantilla. En lugar de
`typename` es posible usar `class` y el comportamiento es completamente
equivalente. El argumento `T` se puede usarse en el contenido de la plantilla
como si de un tipo cualquiera se tratara.

A la hora de compilar el programa, el compilador analiza el código y descubre
que en `ar1` y `ar2` se están usando referencias a la plantilla `array`
enviando tipos distintos, `int` y `float`, como argumentos. Al ver esto,
construye dos clases `array` sustituyendo `T` por `int` y `float` en cada caso.

Este proceso, por tanto, genera código. Un código que posteriormente se compila
de modo seguro, certificando que los tipos son los correctos. En este caso
concreto, asegura que el `array` creado mediante `float` sólo puede ejecutar
`put` para introducir elementos `float` y que el `array` creado con `int` sólo
puede usar `int` de la misma forma.

Las funciones miembro pueden definirse tanto dentro como fuera de la clase
plantilla. Los miembros de las clases también son plantillas, por lo que deben
declararse como plantillas:

``` cpp
template<typename T>
class X{
    //...
    X();
}

template<typename T>
X<T>::X(){
    // Definición de la función
}
```

Es muy frecuente el uso de alias a plantillas como miembros de las clases, lo
que facilita el el uso de plantillas en entornos complejos:

```cpp
template<typename T>
class X{
    using type = T;
    using v = vector<T>;
    // ...
}
```

También es posible, y útil en muchos casos, crear miembros plantilla
simplemente anidando definiciones de plantillas. Pero no es posible declarar
miembros plantilla virtuales (`virtual`).

Si las funciones especiales para copia y movimiento (constructor copia,
asignación copia, etc.) tratadas previamente se definen como plantillas,
también se crearán las versiones por defecto, por lo que es interesante
definirlas de forma explícita.

### Funciones plantilla

Su uso en funciones es similar al de las clases, pero más sencillo si cabe,
porque los argumentos de la plantilla se deducen de los argumentos de entrada
de la función siempre que sea posible.

```cpp
template<typename T>
T inverse(T x){ /*...*/ }

float f;
inverse(f);    // Llama a inverse<float>(f)
complex cmpx;
inverse(cmpx); // Llama a inverse<complex>(cmpx)
```

En los casos en los que no sea posible deducir el tipo desde los argumentos de
entrada sí que se requiere la inclusión de los argumentos.

Es fácil asociar el funcionamiento de las plantillas al funcionamiento de la
sobrecarga de funciones. Al fin y al cabo, las plantillas generan funciones con
argumentos variados y de alguna forma deben resolverse. C++ define unos pasos a
seguir para seleccionar correctamente la función a ejecutar:

1. Construye las plantillas posibles mediante la instanciación.
2. Selecciona las plantillas más especializadas, es decir, las de tipos más
   concretos.
3. Aplica la resolución de sobrecargas al resultado del paso previo, incluyendo
   las funciones definidas fuera del mecanismo de plantillas. En caso de que
   exista conflicto, aplica funciones no definidas mediante plantillas.

Siempre que se requiera usar la función plantilla, es posible usar `<>` en la
llamada a la función, para especificar que no deben usarse funciones clásicas.

### Otros tipos de plantilla

A partir de C++20 se añaden plantillas a las funciones lambda y a familias de
variables. Queda fuera del objetivo de este documento profundizar en casos tan
concretos y modernos, basta, por el momento, con conocer las plantillas en los
casos presentados y extrapolar desde ellos más adelante a medida que se
necesite.

### Instanciación

La instanciación es el proceso por el que se construye el código de cada
plantilla. Este proceso ocurre de forma automática, en función del uso de las
plantillas.

Para que una plantilla se considere usada ocurre como con las clases. Para
generar punteros a ellas no es necesario que estén definidas en el programa
(pero sí declaradas, para que pueda conocerse el nombre) mientras que para
crear objetos de su tipo sí que es necesario conocer su definición. En las
plantillas ocurre del mismo modo. Es más, la generación de una clase plantilla
no implica la generación de todos sus métodos, que se irán generando a medida
que se requieran.

Como crean código, las plantillas pueden resultar en la creación de programas
más grandes si se hace un mal uso de ellas. Sin embargo, aunque no sea
intuitivo, el sistema de plantillas sólo construye las plantillas y miembros
que se usan durante el programa, así que usar plantillas también puede suponer
una reducción del código en algunos casos.

Por otro lado, es necesario tener en cuenta que todo el proceso de análisis
para la creación de todas las plantillas necesarias ocurre en tiempo de
compilación, lo que ralentiza el proceso de compilación de forma radical si no
se hace con cuidado.

Con el fin de optimizar este proceso y/o tener más control sobre él, es posible
requerir una instanciación manualmente mediante la palabra reservada `template`
seguida del nombre de la plantilla a instanciar, esta vez sin `<>`, porque eso
significaría la definición de una plantilla según la sintaxis del lenguaje:

``` cpp
template<typename T> class vector {/*...*/ } // Declaración de plantilla

template class vector<int>;                  // Instanciación explícita
```

También es posible pedir al compilador que no instancie una plantilla usando el
`extern template` de forma similar al ejemplo anterior:

``` cpp
extern template class vector<int>;           // No instanciar
```

#### Dependencia de nombres

Cuando una plantilla se construye se buscan las declaraciones de sus nombres
internos. Este proceso se conoce como *name binding*, un proceso relativamente
complejo ya que los nombres pueden aparecer en diferentes contextos que no se
pueden diferenciar.

- El contexto en el que se define la plantilla.
- El contexto de la declaración de los argumentos.
- El contexto de uso de la plantilla.

Para asegurar el control de los nombres del entorno y facilitar que sólo se
apliquen en la plantilla los nombres que se desean y no otros. Se definen dos
tipos de nombre:

- Los nombres independientes son nombres conocidos y declarados en el momento de
  definición de la plantilla, por lo que se puede realizar el *name binding* en
  el momento de definición de la plantilla.
- Los nombres dependientes, por contra, son aquellos que dependen de los
  argumentos de la plantilla de un modo u otro y no puede buscarse su
  declaración hasta que la plantilla no se instancie.

##### Nombres dependientes

Para que un nombre dependa de una plantilla pueden tratarse de una dependencia
directa que implica el uso de un tipo declarado en la plantilla, `T`, o una
dependencia indirecta que hace uso de cualquier tipo o llamada derivada desde
`T` como `T*` o `f(T)`.

Por defecto, los nombres dependientes se tratan como valores, por lo que es
necesario indicar que se trata de tipos:

``` cpp
template<typename T>
struct S{
    typename T::a *ptr; // T::a depende de T
};
```

No indicar que se trata de un tipo mediante `typename` provoca que `T::a *ptr`
se evalúe como una multiplicación. Un alias puede ayudar a resolver este tipo
de casos:

``` cpp
using ta<T> = typename T::a;
ta<T> *ptr;
```

Del mismo modo, a la hora de acceder a miembros mediante `.`, `->` y `::`, si
éstos son de tipo plantilla, es necesario añadir `template` para que no se
traten como un valor:

``` cpp
template <typename T>
struct S{
    template <typename U> fun(){};
};

template <typename T>
void f( ){
    S<T> s;
    s.template fun<T>(); // Miembro plantilla
}
```

No aplicar `template` en la llamada a `fun` haría que el caracter de apertura
de argumentos de plantilla se considerase un operador «menor que», `<`.

##### Punto de instanciación

Cuando el compilador encuentra una plantilla ésta busca sus nombres
dependientes e independientes.

En caso de que sean nombres independientes, se analizan en el momento de la
lectura de la plantilla y serán esos los que se usarán independientemente de si
se encuentran otros más adecuados más adelante. Las reglas de *scope* aplican.
En decir, los nombres independientes se tratan como si no se tratase de una
plantilla.

Si los nombres de ésta son dependientes, espera a su instanciación para buscar
las declaraciones de éstos, ya que no es posible realizar la comprobación si la
plantilla no se ha creado.

El contexto usado para determinar el significado de los tipos dependientes
depende de la plantilla. A este contexto se le conoce como el punto de
instanciación (*point of instantiation*). Cada uso de la plantilla con un
conjunto concreto de argumentos define un punto de instanciación.

En las funciones plantilla el punto de instanciación es dentro del *namespace*
más cercano, justo después del bloque de código que las usa.  En las clases es
similar, pero el punto de instanciación es justo antes que su uso en lugar de
después. La diferencia radica en que las primeras necesitan habilitar la
recursividad, mientras que las segundas deben estar definidas antes de poder
usarse.

Una plantilla usada en más de una ocasión con el mismo conjunto de argumentos
durante el programa puede tener múltiples puntos de instanciación. El programa
es erróneo si es posible que los nombres tanto dependientes como independientes
tengan interpretaciones distintas en función del punto de instanciación.

``` cpp
namespace N {
    class C { public: C(){} };
    void f(C, int);
}

template<typename T>
int ft(T t, double d){
    f(t, d);                 // Puede hacer referencia a `N::f(C, int)`
    return 1;
}

int a = ft(N::C(), 10.3);    // Llama a `N::f` porque `C` pertenece a `N`
                             // y aplica narrowing en `10.3` al convertir a
                             // `int`

namespace N {
    void f(C, float);        // Sobrecarga `f` reabriendo el namespace
}

int b = ft(N::C(), 10.3);    // Llama a `N::f` porque `C` pertenece a `N`.
                             // Esta vez la mejor opcion es `f(C, float)` por
                             // lo que el programa está mal formado.
```

Este programa es capaz de capturar el caso de error avisando que la llamada a
la función es ambigua pero en un programa más grande es posible que el error
pase inadvertido por el compilador, al que tampoco se le requiere ser capaz de
resolverlo.

##### Plantillas y *namespaces*

Cuando se llama a una función, su declaración puede no estar en el *scope*
actual si existe en uno de los argumentos de entrada de la llamada. Este es el
mecanismo habitual (*ADL*), introducido en la sección sobre *namespaces*, sin
embargo, en el caso de las plantillas toma vital importancia, ya que es el
mecanismo por el que se encuentran las funciones dependientes en tiempo de
instanciación. Los nombres se buscan en el *scope* donde se encuentra definida
la plantilla y en *namespace* al que pertenecen los argumentos de una llamada
dependiente.

##### Plantillas y herencia

Cuando una clase plantilla hereda de una clase base, puede acceder a sus
miembros como en cualquier otra herencia. De nuevo, los nombres de la clase
base pueden depender o no depender de la plantilla.

En caso de depender de la plantilla, pueden ocurrir cosas casos sorpresivos:

``` cpp
void f(int);
struct B {
    void f(char);
};

template<typename T>
class D : public T {
    //    ^^^^^^^^  Hereda del tipo declarado en la plantilla
    public:
    void g(){
        f('a');     // Llama a `f(int)`
    }
};

D<B> a;
a.g();
```

El ejemplo llama a `f(int)` en lugar de a `f(char)` como cabría esperar y esto
se debe a que durante la definición de la plantilla `f` no es dependiente, o no
lo parece a simple vista.

Para asegurar que se trate como merece, como miembro de `B`, se pueden realizar
tres diferentes opciones:

1. Usar `this`
2. Usar `T::`
3. Traer la declaración mediante `using T::f`

Cualquiera de estos métodos asegura que se trate el nombre como un nombre
dependiente y lo resuelve correctamente.

### Especialización

Los resultados del proceso de instanciación se conocen como especializaciones
ya que cada una de ellas define un caso especializado para la plantilla en
función de sus argumentos.

#### Argumentos de plantilla

Los argumentos de las plantillas se comportan de forma similar a los de las
funciones, permitiendo declarar varios argumentos seguidos por una coma. La
mayor diferencia reside en el tipo de argumentos que una plantilla puede
recibir.

##### Argumentos por defecto

Tal y como ocurre con las funciones, las plantillas pueden definir argumentos
por defecto, de modo que no es necesario indicarlos a la hora de usarlas:

``` cpp
template<typename C= char>
class my_string{
    C *contents;
    ///...
};

my_string<> s; // Equivalente a `my_string<char>`
```

En caso de las funciones plantilla, como no requieren la especificación de los
argumentos, sino que se deducen de la llamada a la función, es posible que sea
necesario indicarlos usando `<>` en sus llamadas porque el sistema de selección
de argumentos puede tener problemas para deducirlos en caso de usar argumentos
por defecto.

##### Argumentos tipo

Los argumentos tipo o, mejor dicho, de tipo tipo, son los casos que se han
tratado hasta ahora. Argumentos de plantilla que están diseñados para recibir
el nombre de un tipo concreto, como `int` o `char*`.

Los argumentos tipo se deben declarar con la palabras reservadas `typename` o
`class`, que son completamente equivalentes.

No existe ninguna limitación en el tipo a recibir, así que si desean hacerse
comprobaciones deben hacerse manualmente. La única limitación, lógica, por otro
lado, es que los tipos a enviar como argumento deben estar en *scope*.

##### Argumentos valor

Las plantillas también pueden recibir valores como argumentos, siendo el caso
más común el de recibir números enteros para definir la longitud en
colecciones. Para declararlos, es necesario usar el nombre del tipo del valor
que se desea usar:

``` cpp
template <typename T, int len>
                //    ^^^^^^^  Argumento valor
class vector_fixed{
    // ...
};

vector_fixed<complex<float>, 10> v;
                        //   ^^   Un valor
```

Los valores como argumento sí que tienen diversas limitaciones, ya que la
evaluación de las plantillas se realiza en tiempo de compilación:

- Deben ser tipos integrales `constexpr`
- Punteros o referencias a funciones u objetos con enlazado externo
- Punteros a miembros no sobrecargados
- `nullptr`

##### Argumentos operación

Los argumentos operación no son un tipo real de argumento definido por el
lenguaje, pero sí son algo que en ocasiones se deseará hacer, y es interesante
tener opciones para hacerlo.

La suposición de querer crear una plantilla capaz de aplicar una operación
sobre una colección es tentadora, pero la solución no es evidente.

Los argumentos valor resuelven el caso con relativa facilidad, crear una
función que después se envía como puntero a la plantilla es suficiente para
muchos casos. Incluso puede usarse una función *lambda* con este objetivo.

Sin embargo, para ello la plantilla debe definir el tipo de la función en
detalle, marcando sus posibles argumentos de entrada, lo que hace que la
plantilla dependa de éstos.

Otra solución interesante es la de enviar la función como argumento tipo, y
usarla en el cuerpo de la plantilla como un miembro de ésta:

``` cpp
template <typename T, typename FilterF>
class filter {
    FilterF f;
    filter(FilterF fil) : f {fil} {}
    // ...
};
```

Esta clase plantilla declara su función de filtrado (`FilterF`) como tipo y
luego la construye desde su constructor. De esta forma, puede enviarse
cualquier tipo como función reductora, incluso objetos-función, que pueden
incluso almacenar un estado.

```
// [...] define BiggerThan

BiggerThan filter_function {10};
filter<vector<int>, BiggerThan>(filter_function);
```

Este mecanismo aporta una generalidad muy interesante que facilita la creación
de algoritmos independientes de los tipos que tratan, es por eso que se trata
de un diseño muy frecuente en la librería estándar.

##### Argumentos plantilla

Las plantillas también pueden recibir plantillas como argumento. Para
declararlas como argumento necesitan declarar sus argumentos correspondientes:

``` cpp
template <typename T, template<typename TT, int L = 10>>
                //    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ Plantilla como argumento
```

Los casos de uso para un programa así son raros. Si se desea enviar un
contenedor como `vector` es más útil añadirlo como tipo y requerir su
especialización en el uso de la plantilla:

``` cpp
template<typename V, typename V2>
class whatever {}

whatever<vector<int>, vector<char>> v;
```

En el ejemplo `vector<int>` y `vector<char>` no son plantillas, sino tipos, ya
que están totalmente definidos, así que no es necesario usar plantillas como
argumentos de plantilla.

##### Errores de sustitución de argumentos

C++ introdujo el concepto SFINAE (*Substitution Failure Is Not An Error*), que
implica que los fallos de sustitución no suponen un error en el programa. Este
mecanismo puede usarse con diferentes fines, pero el más evidente y la razón de
su existencia es que se seleccionan las especializaciones de modo que no
existen fallos por sustituir los argumentos.

``` cpp
struct Test {
    using foo=int;
};

template <typename T>
void f(typename T::foo) {}      // Primera definición

template <typename T>
void f(T) {}                    // Segunda definición

int main() {
  f<Test>(10);        // Llama la primera definición
  f<int>(10);         // Llama a la segunda definición sin caer en error,
                      // ya que aplica SFINAE
}
```

Si la segunda definición no existiera, el compilador no sería capaz de llamar a
la primera implementación, a pesar de que vaya a recibir un `int`, porque trata
de encontrar el campo `::foo` del tipo que se le ha entregado a la plantilla.
Esto resulta en un fallo.

La existencia de la segunda definición de la función elimina este fallo gracias
a SFINAE, que simplemente descarta las especializaciones de las plantillas que
supondrían un fallo, siempre que haya otra una opción correcta a la que llamar.

Esto es especialmente interesante en algoritmos que hacen uso de los miembros
de los tipos a aplicarles, si éstos no disponen de ciertos miembros, siempre se
puede crear una especialización distinta que los capture y los trate de otro
modo sin incurrir en un error.


#### Especialización manual

Por defecto las plantillas definen comportamientos genéricos para la creación
del código, sin embargo, para casos concretos puede ser interesante definirlos
manualmente y aplicar optimizaciones manuales. Esto puede conseguirse mediante
la especialización manual (*user specialization* o *user-defined
specialization*).

La especialización puede ser concreta (*concrete specialization*), aplicando
tipos o valores finales (`int`, `float`, `void*`...), o parcial (*partial
specialization*), aplicando tipos más generales, pero más concretos que la
plantilla (`T*`). La diferencia entre ellas es que la segunda aún puede recibir
argumentos de diferentes tipos como `int*`, `float*` o `int*****`, mientras que
la primera sólo puede recibir el tipo escogido.

Además de asignar diferentes tipos, la especialización manual puede designar
valores para argumentos de tipo valor. Un caso de uso así podría tratar la
optimización de un contenedor para el caso de no tener elementos.

La forma de aplicar la especialización manual es aplicar una sub-plantilla más
específica que la plantilla madre o plantilla primaria (*primary template*).

``` cpp
template<typename T1, typename T2, int I>
class A {};             // Plantilla primaria

template<typename T1, typename T2, int I>
class A<T1*, T2, I> {}; // Especialización parcial donde T1 es un puntero

template<typename T>
class A<int, T*, 5> {}; // Especialización parcial donde T1 es `int` e `I` es
                        // `5`

template<>
class A<int, float*, 19> {};   // Especialización concreta
```

La plantilla primaria puede estar o no definida, pero siempre debe declararse
antes de generar especializaciones. En caso de que no se use la plantilla
primaria, no es necesario definirla, ya que nunca se instanciará.

La selección de plantilla siempre tratará de aplicar la más especializada, esto
es, la plantilla cuyos argumentos posibles puedan asignarse a los de otra, pero
no al revés.

### Plantillas y organización de proyectos

El proceso de instanciación de plantillas altera la organización de proyectos
propuesta previamente, donde se separaba el código de la definición de la clase
de la definición de sus miembros, o se llevaban las definiciones de funciones
al archivo fuente manteniendo sus declaraciones en el archivo de cabecera.

Las plantillas no siempre resultan en código, requieren de una instanciación
para poder generar código final, por lo que si se aislan sus definiciones en
archivos independientes y se compila por separado nunca se instanciarán porque
el compilador no es capaz de ver dónde se usan las plantillas y la definición
de éstas al mismo tiempo.

Esto resulta en que el enlazado falla, diciendo que no es capaz de encontrar
las especializaciones de plantilla usadas en el programa.

Suponiendo el siguiente programa, diseñado siguiendo el patrón erróneo que se
acaba de proponer:

```cpp
// archivo main.cpp
#include "container.hpp"
int main(){
    Container<int> c; // Instanciación de Container
    c.print();        // Uso de la función miembro de Container
}
```

```cpp
// archivo container.hpp
template<typename T>
class Container{
    void print();
};
```

```cpp
// archivo container.cpp
#include"container.hpp"
template<typename T>
void Container<T>::print(){
    // ...
}
```

Al realizar la compilación independiente, tanto `main.cpp` como `container.cpp`
se compilan por separado a sus propios archivos objeto. Al hacerlo, como
`container.cpp` no usa la plantilla en ninguna ocasión, ésta no se instancia y
su archivo objeto no incluye ninguna especialización.

En la etapa posterior de enlazado, al combinar `main.cpp` con `container.cpp`,
la especialización creada en el archivo `main.cpp` no se encuentra porque nunca
se creó.

Para evitar este problema las plantillas se definen en los archivos de cabecera
y se incluyen mediante `#include` en los archivos de código fuente donde se
quieran utilizar. En ocasiones se crean en archivos con extensión `txx` o
similar y se incluyen en el archivo de cabecera, consiguiendo el mismo
resultado.

En el ejemplo propuesto, mover el contenido de `container.cpp` a
`container.hpp` permite que el compilador encuentre las plantillas en el
momento de compilar `Container<int> c;` y que instancie correctamente la
plantilla.

Puede pensarse a primera vista que las plantillas rompen el principio de
independencia del código, pero eso no es lo que realmente ocurre. Las
plantillas no son código, sino un conjunto de reglas que definen cómo se crea
el código. El hecho de no incluirlas no permite la construcción correcta del
código, lo que resulta en fallos de enlazado.


### Programación genérica

La programación genérica es el uso más común de las plantillas, con ella es
posible trasladar un funcionamiento creado para un tipo concreto a un nivel de
abstracción mayor, permitiéndole operar para cualquier tipo. Este proceso de
generalización de un algoritmo se conoce como *lifting* y es, posiblemente, la
mejor manera de diseñar algoritmos genéricos en C++: partir de un algoritmo
aplicado a un tipo base, o varios, y, después de probarlo a conciencia,
generalizarlo a través de una plantilla.

Para que un algoritmo pueda aplicarse, en ocasiones requiere que los elementos
sobre los que se aplique cumplan ciertas características. Por ejemplo, un
algoritmo de búsqueda no tiene sentido en algo que no sea una colección. Estas
restricciones pueden ser mucho más complejas: que los operandos puedan sumarse,
que puedan convertirse a un valor numérico, que se les pueda aplicar una
función *hash*, etc. El conjunto de estas limitaciones se conoce como
*concept*.

Hasta C++20 los *concepts*[^concepts] era un patrón de diseño para limitar el
uso de los argumentos de entrada de las plantillas, pero a partir de C++20
forman parte del lenguaje. Aunque C++20 es un estándar muy moderno, aún no
publicado cuando se escriben estas líneas (y aún menos implementado por los
compiladores) es interesante analizar su sintaxis y funcionamiento, aunque sólo
sea de forma sencilla.

```cpp
// A partir de C++20

#include <string>
#include <cstddef>
#include <concepts>

// La plantilla requiere que, dado un objeto de tipo `T`, llamado `a`, la
// aplicación de `std::hash<T>{}` compile y resulte en un objeto convertible a
// `std::size_t`, un tipo entero positivo.
template<typename T>
concept Hashable = requires(T a) {
    { std::hash<T>{}(a) } -> std::convertible_to<std::size_t>;
};

struct S {};

template<Hashable T>
void f(T);               // Plantilla para tipos Hashable (existen formas
                         // alternativas)

int main() {
  f(std::string("abc")); // Correcto: std::string es Hashable
  f(S{});                // Error: S no es Hashable
}
```

El ejemplo, a pesar de ser complejo, describe a la perfección el
funcionamiento. El `concept` es una prueba de tiempo de compilación, capaz de
comprobar si la condición se cumple. De este modo, cuando las plantillas se
instancien se sabrá si la llamada a `f` es o no correcta.

Lamentablemente, este ejemplo tan elegante es cosa del futuro. Hasta que el día
en el que esto sea parte de los compiladores más comunes, es necesario usar
estas limitaciones de forma manual. Las `constexpr` pueden resolver esta tarea,
porque se ejecutan en tiempo de compilación y pueden usarse como argumentos a
las plantillas, y `static_assert` y `enable_if` son buenas ayudas para el
proceso. Pero, sea como fuere, difícilmente se aproximan a la declaración
estilo C++20.

Además de los *concepts*, que expresan las necesidades de un algoritmo, es
posible que la implementación requiera de algunas características o que dé por
hecho que recibirá argumentos sobre los que se pueden aplicar aplicaciones
concretas. Por ejemplo, operaciones de movimiento y copia, soporte de
operadores etc. Esto es, a nivel conceptual, independiente del algoritmo, ya
que éste puede programarse de modo distinto. Son meras asunciones, pero también
afectan a los posibles argumentos de entrada del algoritmo. Éstas
comprobaciones pueden generarse de un modo similar, pero habitualmente deben
esperar al tiempo de ejecución, así que deben implementarse mediante *tests*.

Con las herramientas descritas ya se tiene un conocimiento suficiente para usar
las plantillas para programación genérica, o al menos para saber a qué tipos de
problemas suele enfrentarse quien desarrolla código genérico en C++. La
librería estándar implementa varios algoritmos genéricos mediante estas
técnicas en `<algorithm>`.

[^concepts]: La traducción en español es «concepto» pero se usará la palabra en
  inglés para no mezclarla con el significado común de la palabra.


### Metaprogramación

La metaprogramación es otra técnica aplicable con el mecanismo de plantillas.
Las plantillas se definieron de forma muy genérica, tanto que constituyen un
lenguaje de programación funcional completo[^templates-fp] que se ejecuta en
tiempo de compilación. Esto permite un uso muy agresivo, que difiere del estilo
de programación aplicable a la programación genérica. La programación genérica
no es mucho más que la descripción de cómo un programa debe funcionar para
ciertos tipos o valores descritos como parámetros de tiempo de compilación,
pero la metaprogramación permite un nivel de expresión tal que requiere de
otros modelos mentales. Es por eso que diferenciar ambos conceptos es útil
desde el punto de vista del pensamiento humano, porque permite asociar las
herramientas mentales con uno u otro y facilitar el uso de patrones de
pensamiento que encajen mejor para la tarea que se desea realizar.

[^templates-fp]: Eso no significa que sea cómodo programar en él, sólo que es
  posible.

La metaprogramación se basa en uso de funciones aplicables evaluables en tiempo
de ejecución del estilo de `sizeof` o `decltype` pero que normalmente tienen
otro aspecto. Suelen recibir sus argumentos de entrada como argumentos de una
plantilla y retornar sus valores como miembros. Las funciones de tipos de la
librería estándar retornan sus tipos en su campo `::type` y en caso de retornar
un valor lo hacen a través de `::value`.

```cpp
// ... Declarar una condición constexpr llamada `condition`

std::conditional<condition, int, long>::type var;
```

El ejemplo aplica la función de tipos `std::conditional`, que selecciona entre
sus dos posibles ramas, en función de su condición. De esa forma, construye la
variable `var` como `long` o como `int` en función de la condición indicada.
Puede apreciarse que la función entrega el valor de retorno a través de
`::type` pero que también podría entregar otros a través de otros campos.

Por comprender cómo funcionan estas funciones internamente, es interesante
ver una posible implementación de `std::conditional`:

``` cpp
#include<iostream>

template<bool cond, typename A, typename B>
struct conditional{
    using type = A;
};

template<typename A, typename B>
struct conditional<false, A, B>{
    using type = B;
};

int main(){
    // Cambiar `true` por false declara `val` como float y lo visualiza con
    // cifras decimales
    conditional<true, int, float>::type val = 1.1;
    std::cout << val;
}
```

#### Selección e iteración

La función de selección descrita en el apartado superior, `std::conditional`,
es uno de los mecanismos de selección posibles. Otro de ellos es `std::select`,
que en lugar de seleccionar entre dos ramas posibles, es capaz de seleccionar
entre `N` ramas. Su funcionamiento es similar, pero necesita iterar por los
elementos para obtener un resultado. Para ello utiliza plantillas *variadic*,
es decir, con un número variable de argumentos, indicados por la elipsis
(`...`).

``` cpp
#include<iostream>

template<int N, typename...List>
struct Select;

template<int N, typename Head, typename...Tail>
struct Select<N, Head, Tail...>: Select<N-1, Tail...>{};

template<typename Head, typename...Tail>
struct Select<0, Head, Tail...>{
    using type = Head;
};

int main(){
    Select<1, int, float>::type val = 1.1;
    std::cout << val;
}
```

La iteración se realiza mediante la recursividad. Aunque quizás no es tan común
en C++, la recursividad es un mecanismo muy usado en otros lenguajes de
programación, como Haskell, Erlang o Lisp, todos ellos lenguajes de
programación funcional. Quien esté acostumbrado a usarlos entenderá por qué se
han seleccionado los nombres `Head` y `Tail` y probablemente tenga más
facilidad para comprender el funcionamiento del ejemplo. El ejemplo es un claro
uso del *pattern matching*[^pattern-matching] basado en la especialización:

[^pattern-matching]: El *pattern-matching*, o búsqueda de patrones, es un
  mecanismo para detectar patrones en conjuntos de elementos. Es muy popular en
  lenguajes de programación funcional. Puede entenderse como una sentencia
  `case` que en lugar de saltar a su rama en función de un valor, lo hace en
  función de un patrón más complejo.

> Construye una plantilla por defecto que recibe un entero para realizar la
> cuenta y una lista de elementos. Como no se tiene la intención de instanciar
> este caso, simplemente se declara sin definir. Después, se construye una
> especialización para cuando la lista disponga de al menos un elemento, `Head`
> (cabeza), seguido de una lista, que puede estar vacía, `Tail` (cola). Este
> caso sirve para realizar la recursividad, aplicando una herencia una
> estructura del mismo tipo que descarta la cabeza de la lista y reduce el
> contador `N`.
>
> Para que esta recursividad no sea infinita, y pare cuando `N` sea `0`, se
> especializa de nuevo la estructura para el caso en el que `N` sea `0`,
> entregando `Head` como el elemento seleccionado bajo
> `Select::type`[^select-vs-Select].
>
> La iteración funciona porque la lista a usar se ha ido recortando a medida
> que `N` se reduce, de este modo, cuando `N` es `0` se asegura que la cabeza
> de la lista es el elemento que se buscaba.

Si este mecanismo resulta demasiado complicado, también es posible usar
funciones `constexpr` para realizar este tipo de operaciones, con la ventaja de
que pueden usarse en tiempo de ejecución también si se requieren.

[^select-vs-Select]: Se usa el nombre `Select` en el ejemplo para evitar
  conflictos con otras definiciones aportadas por el lenguaje.

##### Selección de funciones

A veces cuando se trata con este tipo de condicionales se puede necesitar
seleccionar funciones. El truco para hacerlo es definir clases con el operador
`operator()` definido, para que puedan ejecutarse como funciones. Después,
seleccionar su tipo mediante una sentencia de selección permite crear un objeto
de este tipo y llamarlo. Agrupado en un ejemplo absurdo:

``` cpp
struct dec {
    int operator()(int a){
        return a-1;
    }
};
struct inc {
    int operator()(int a){
        return a+1;
    }
};

std::conditional<condition, dec, inc>{}( val );
```

#### Condiciones y comprobaciones

Aunque ya se ha descrito cómo seleccionar mediante `std::select` y
`std::conditional`, es necesario disponer de algún criterio sobre el que
hacerlo. La librería estándar aporta gran cantidad de ellos bajo `std::is_`
seguido de la condición que se quiera comprobar, disponibles en
`<type_traits>`. Por ejemplo `std::is_integral`, para comprobar si un tipo es
integral.

Estas comprobaciones son útiles también junto con `static_assert` para
comprobar si los argumentos de las plantillas cumplen con las condiciones
adecuadas para su uso. Puede ser útil para emular *concepts*:

``` cpp
template <template T>
T f(T i)
{
    static_assert(std::is_integral<T>::value, "Integral required.");
    return i;
}
```

Las comprobaciones de `<type_traits>` pueden llegar a ser muy avanzadas, tanto
que no sea posible implementarlas en código, porque comprueben detalles que
sólo el compilador conoce. En muchas ocasiones es el compilador el encargado de
resolverlas. A nivel práctico esto no aporta ninguna diferencia, pero sirve
como referencia para conocer el tipo de comprobaciones que pueden realizarse.
Se recomienda un vistazo rápido a las opciones aportadas por la librería.

Por otro lado, se recomienda, como siempre, confiar en la librería estándar
para estas comprobaciones. La librería estándar siempre va de la mano con el
compilador, así que conocerá en detalle la implementación concreta en la que se
trabaja. El caso de `std::is_integral` es muy claro en este sentido.

`std::is_integral` comprueba que el tipo a comprobar sea integral, es decir,
que en los estándares más recientes sea uno de estos: `bool`, `char`,
`char8_t`, `char16_t`, `char32_t`, `wchar_t`, `short`, `int`, `long` o `long
long`. Sin embargo, como las implementaciones pueden implementar tipos
integrales adicionales, comprobar manualmente si pertenece a uno de estos es
peligroso.  `std::is_integral` siempre hará la comprobación como es debido,
usando los tipos que esa implementación soporte.

#### Rasgos (*trait*)

Los *trait*, o rasgos, son características que cada tipo debe tener. La
librería estándar aporta, de forma distinta a `<type_traits>`, que aporta
funciones separadas, descrito en el apartado anterior, grupos de
características asociadas a diferentes tipos: `std::char_traits`,
`std::allocator_traits`, `std::pointer_traits` y otros.  Cada uno de ellos
define el conjunto de rasgos que el tipo indicado tiene. No son cómodos de
utilizar pero son un mecanismo poderoso que se recomienda investigar.


#### *Forwarding*

El *forwarding* es el caso de uso por excelencia de las *forwarding references*
o *universal references* descritas previamente (ver [Referencias a *rvalue*]).
El *forwarding* es una técnica de reenvío de argumentos de entrada de una
función a otra. Pero antes de explicar su funcionamiento más genérico mediante
plantillas *variadic*, se debe explicar su fundamento. Suponiendo el ejemplo:

```cpp
void process(const Value& lval); // procesamiento de lvalues
void process(Value&& rval);      // procesamiento de rvalues

template<typename T>
void log_and_process(T&& arg)
{
    std::cout << "Called!!" << std::endl;
    process(std::forward<T>(arg)); // std::forward es clave
}

Value w;
log_and_process(w);             // Procesa lvalue
log_and_process(std::move(w));  // Procesa rvalue
```

Puede observarse el uso de `std::forward`. Dentro de las funciones los
argumentos de entrada ya no son *rvalues* porque tienen un nombre asociado,
por esta razón si se quieren reenviar a otra función éstos siempre se enviarán
como *lvalues*. `std::forward` es una función que aplica una conversión
condicional en función del tipo de valor que se trate. Si el valor era un
*rvalue* en la llamada a la función actual, se convierte a *rvalue*, y si era
un *lvalue* se deja como estaba. De esta forma se consigue un reenvío real.

Sabiendo esto, construir funciones que hagan un reenvío de cualquier número de
parámetros es sencillo:

``` cpp
template<typename ... T>
void call(T&&... t)
{
    // Realizar procesamiento adicional

    process(forward<T>(t)...);
}
```

Es más, pueden definirse de modo que se les envíe incluso la función a la que
llamar:

``` cpp
template<typename F, typename ... T>
void call(F&& f, T&&... t)
{
    f(forward<T>(t)...);
}
```

Este último ejemplo además funciona a la perfección con todos los tipos
*llamables* que se puedan introducir en `F`: punteros a funciones,
objetos-función, etc.



## Recapitulación

En este capítulo se han descrito los mecanismos de más alto nivel que
fundamentan C++. Las clases son el más importante de ellos ya que, como se ha
podido apreciar, todos los conceptos adicionales las necesitan de una u otra
forma, e incluso son útiles para explicar muchos conceptos que se han
introducido en capítulos previos.

Es extremadamente difícil seleccionar el orden de los capítulos de un documento
como este igual que lo es poder aclarar los conceptos de forma autocontenida
sin tener especial dependencia en apartados futuros o previos. C++ es un
lenguaje muy complejo, en el que todos los temas tienen tal interdependencia
que es imposible separarlos por completo. El objetivo del documento ha sido el
de introducir los conceptos de forma más o menos profunda en cada sección en la
que se introducen de modo que la futura consulta sea sencilla, sin comprometer
la facilidad de lectura tradicional de principio a fin.

Esto no significa que el objetivo haya podido cumplirse en su totalidad.
Seguramente, un segundo vistazo a los apartados previos puede aclarar muchas
ideas que no llegaron a afianzarse en su momento. Lo importante en una primera
lectura es capturar una base suficiente para seguir construyendo, aunque no sea
una base demasiado sólida, de modo que la generalidad del lenguaje pueda
entenderse para poder llegar a este capítulo con el conocimiento suficiente
para comprenderlo.

El capítulo *[Introducción al lenguaje]*, por mucho que se prolongue, no se
puede decir que exprese la esencia de C++. La esencia se encuentra en éste
capítulo que acaba de introducirse, que, por otro lado, no tiene sentido sin el
capítulo mencionado ni, por supuesto, sin la práctica consciente que se debe
aplicar para comprender todos los conceptos que aquí se expresan.

Muchos conceptos, secciones interesantes de la librería estándar, notas,
ejemplos, actualizaciones del estándar, patrones de diseño y peculiaridades del
lenguaje, que lamentablemente son muchísimas, han quedado fuera del documento.
Quien investigue puede sentir que son demasiadas que jamás las conocerá o que
este documento ha hecho una labor pobre en transmitirlas.  Sin ánimo de
rechazar la responsabilidad, la opinión personal de quien escribe estas líneas
es que todos esos detalles son irrelevantes. El fondo es lo más importante,
pues es lo que permite comprenderlos a simple vista. Prolongar más este
capítulo para introducir más detalles es una labor tediosa e inútil, puesto que
siempre puede quedar algún detalle fuera del documento. A juicio de quién
escribe, lo más importante es aportar una base lo bastante sólida como para que
esos detalles parezcan tan insignificantes como realmente son.

Entrando ahora en detalle, este capítulo parte de un set reducido de
funcionalidades descritas en capítulos previos y construye las clases
combinándolas. Datos y funciones miembro y un concepto de auto-referencia son
lo que forma una clase sencilla. Desde ahí, se trata la *encapsulación*, uno de
los conceptos básicos de la programación orientada a objetos, mediante el
control de accesos.

Una vez conocidas las clases y los añadidos del lenguaje para manejarlas con
facilidad, como el RAII o la sobrecarga de operadores, se introduce la
*herencia*, otro de los conceptos básicos de la programación orientada a
objetos, que permite ahorrar código fuente y expresar modelos mentales
complejos mientras que habilita construcciones tan inesperadas como las
herencias recursivas propuestas en el apartado de metaprogramación. La
*herencia* combinada con las funciones virtuales habilita el *polimorfismo* en
tiempo de ejecución, otra de las técnicas básicas de la programación orientada
a objetos, que permite que clases derivadas puedan asignarse a punteros y
referencias de la clase base y puedan seguir ejecutando sus propias funciones
miembro.

A mitad de este capítulo, se aprovecha para tratar conceptos más generales: El
*free-store*, la capacidad de controlar la memoria manualmente, y las
*move-semantics* junto con los *smart-pointers*, o punteros inteligentes,
muestran los trucos de control de memoria y propiedad de los objetos que C++
añade sobre los mecanismos básicos que C aporta. Los punteros inteligentes y
las *move-semantics* son un avance tal que es capaz de marcar un antes y un
después en C++: la línea que separa el C++ clásico del moderno. Aunque la
explicación que se ha dado de estos conceptos es sencilla, es más que
suficiente para dominarlos. La práctica hará el resto.

Se ha terminado con las clases analizando un añadido fácil de entender y
extremadamente útil: los punteros a miembros. Éstos, a parte de ser un concepto
paradójico puesto que no son realmente un puntero sino un incremento, son un
buen ejercicio mental para comprender lo que es un objeto: un conjunto de datos
agrupados en la memoria.

A partir de ahí se tratan unas sencillas facilidades para la conversión de
tipos que no tienen demasiado interés a nivel conceptual más allá de la
diferencia de las conversiones estáticas, las que se ejecutan en tiempo de
compilación, y dinámicas, que permiten comprobaciones en tiempo de ejecución.
Es cierto además que los trucos que quien programa en C habitualmente
acostumbra a realizar, como las reinterpretaciones binarias basadas en
`union`s, dejan de ser necesarias en C++ gracias a estos detalles.

Para terminar se introducen las plantillas, un sistema que aporta polimorfismo
en tiempo de compilación, para permitir la definición paramétrica de clases y
funciones. Parece absurdo que un detalle tan sencillo haya llevado tantísimo
trabajo pero resulta que las plantillas, gracias a la generalidad con la que se
definieron, conforman un lenguaje de programación completo capaz de crear
código C++ en tiempo de compilación. Esto es extremadamente poderoso, aunque
difícil y abrumador para el ojo inexperto. Es por eso que se introducen lo
suficiente como para mostrar su poder sin detallar tanto que se pierda la
atención sobre el concepto.

Las plantillas son además una parte del lenguaje que ha recibido gran cantidad
de mejoras y añadidos los últimos años, hasta tal punto que existen libros
completos sobre ellas[^cpp-templates-guide]. No es sorprendente, puesto que el
apartado sobre metaprogramación ya muestra ejemplos de su poder. 

Como caso curioso, la metaprogramación, entendida como un concepto aislado, ya
es tan amplia y poderosa como para que puedan desarrollarse lenguajes de
programación completos sobre ella. El libro «Generative Programming: Methods,
Tools, and Applications» de Krysztof Czarnecki y Ulrich Eisenecker describe
cómo realizar una implementación de Lisp llamada METALISP usando únicamente el
sistema de plantillas de C++[^metalisp].

Más que con temor, estos casos paradigmáticos y extremos deben tomarse como un
desafío. El conocimiento adquirido durante este documento es más que suficiente
para afrontar las explicaciones que en ambos libros puedan encontrarse. De
todos modos, en el mundo real, en el día a día de quien se dedica a la
programación en C++, no se realizan este tipo de excesos habitualmente y se
trata de mantener el código legible y comprensible.

Para quien se pregunte el por qué de esta recapitulación tan intensa, que es
más cercana a una recapitulación del documento completo más que de éste
capítulo en particular, esta recapitulación puede considerarse la del documento
completo, porque lo que queda por añadir (e incluso parte de lo que ya se ha
contado) no es parte del lenguaje si no un añadido necesario para poder usarlo.
En este punto ya se han explicado todos los conceptos necesarios para
comprender C++ al nivel que este documento se proponía así que es posible dejar
de leer aquí, como si esto fuera el final del documento. Es recomendable, por
supuesto, leer hasta el final, porque a partir de éste momento se introduce la
librería estándar, con la que toda persona que programe C++ debe lidiar, y
algunos programas interesantes que hacen que la experiencia de programar C++
sea mucho más cómoda.


[^cpp-templates-guide]: Como «C++ Templates: The Complete Guide», por David
  Vandevoorde y Nicolai M. Josuttis.  ISBN-13: 978-0201734843

[^metalisp]: El proyecto Archive.org mantiene en su WaybackMachine una captura
  de la implementación anterior a la publicación del libro. Consultado
  2020-07-03:  
  <https://web.archive.org/web/20030204184347/http://www.prakinf.tu-ilmenau.de/~czarn/meta/metalisp.cpp>
