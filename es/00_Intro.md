# Introducción

C++ es un lenguaje de programación de propósito general creado por Bjarne
Stroustrup en los años ochenta como una extensión del lenguaje C. A menudo se
le describe como «C con clases», lo que implica que nace con la intención de
aportar Programación Orientada a Objetos al lenguaje C.

Por contextualizar, el lenguaje C  apareció a principio de los años setenta
con la intención de crear aplicaciones para el sistema operativo UNIX. En aquel
momento era muy necesario desarrollar un lenguaje de programación de alto
nivel que, al mismo tiempo, fuera fácil de convertir a lenguaje máquina para,
con ello, poder abandonar el lenguaje ensamblador por un lenguaje que expresase
con mayor facilidad el modo de pensamiento humano.

A final de la década de los setenta, se publicó la primera versión de C,
conocida como K&R C por el nombre de sus autores Brian Kernighan y Dennis
Ritchie[^kr]. Esta primera versión del lenguaje fue acompañada por el libro
«The C Programming Language», un hito en la historia de la informática que es
además un recurso excelente.

Posteriormente debido a su importancia y a su amplio uso, C fue estandarizado
por ANSI, el Instituto Nacional Americano de los Estándares, trayendo consigo
una nueva versión del libro. Este proceso es importante debido a que en ese
momento el estándar POSIX[^posix] aún estaba en desarrollo y fue incluyendo el
lenguaje C en el proceso, debido al peso que éste tenía en UNIX.

Poco más tarde, y tras unos cambios de edición en el estándar, C se estandarizó
en ISO, la Organización Internacional para la Estandarización. Desde ese
momento, existe un grupo de trabajo dedicado a la mejora del estándar y nuevos
estándares se publican en función de la necesidad. A pesar de que estos
estándares tienen su numeración oficial[^num], normalmente se los conoce por C
seguido por las dos últimas cifras del año de publicación: C89, C99, C11 y C18,
siendo C99 y C11 las más utilizadas hoy en día.

La historia de C++, por su lado, ha sido muy similar a la de C ya que ambos
lenguajes han vivido siempre en constante relación: C ha obtenido mejoras
inspiradas en C++, C++ trata de ser compatible con C, etc. El primer estándar
ISO de C++ llega en 1998 y a partir de ahí, tal y como ocurre con C, se
publican otros nuevos estándares tanto del lenguaje como de extensión de éste.
Del mismo modo que en el caso de C, los estándares de C++ toman el mismo tipo
de nomenclatura siendo, en este caso, los siguientes: C++98, C++03, C++11,
C++14, C++17 y C++20.

No todos los estándares de C++ son versiones mayores, algunas son
actualizaciones más reducidas. Las versiones C++98, C++11, C++17 se consideran
revisiones mayores. Existe un salto mucho mayor entre C++11 y su predecesora
que entre cualquier otra, principalmente debido al tiempo transcurrido entre
ambas.

Como puede verse, C ha sido, y es, uno de los lenguajes de programación más
relevantes de la historia de la informática llegando incluso a ser
estandarizado, cosa que pocos lenguajes consiguen, y C++, como extensión de
éste, hereda parte de esa responsabilidad. Diseñado para ser robusto y
eficiente tanto en entornos embebidos como bajo un sistema operativo, C++
aporta las capacidades de bajo nivel heredadas de C con una capacidad mayor
para abstraer conceptos complejos.

El hecho de que tanto C como C++ estén estandarizados es más que destacable, no
sólo porque sea un hecho histórico casi sin precedente, sino porque esto
permite a cualquiera crear su propia implementación del lenguaje. Tal y como se
verá más adelante, existen muchas implementaciones de C y C++, y ninguna de
ellas está considerada como la «correcta» tal y como en otros lenguajes sí que
ocurre, donde existe una implementación de referencia y el resto tratan de
parecerse al máximo a ella.

Aunque quien busca información sobre C++ o C lo hace por un fin y conoce su
propio contexto, el hecho de que C y C++ sean tan ampliamente utilizados hasta
el punto de que organizaciones internacionales decidan dedicarles el tiempo
para registrarlos como estándar no es, en absoluto, casualidad. Tanto uno como
el otro se manejan bien en registros que históricamente han generado interés.
En primer lugar por su posición en el entorno, producida principalmente por su
aparición en herramientas de UNIX, el padre de casi todos los sistemas
operativos actuales y el precursor de la informática moderna. En segundo, por
su flexibilidad para alcanzar detalles de muy bajo nivel, como la gestión
directa de memoria, y robustez, aportada en parte por los tipos estáticos, y
por la cantidad de herramientas de desarrollo disponibles para estos lenguajes.

Evidentemente, todo este interés debe traducirse en algo más, algo que sirva de
respuesta para quien lea estas palabras buscando responder a la pregunta «¿por
qué dedicar esfuerzo en aprender este lenguaje?». Y la respuesta no puede ser
más sencilla: a pesar de los cambios que la informática ha sufrido desde los
años setenta, donde se sitúa el inicio de la microelectrónica, C ha estado
presente en todos ellos. Ha sido enseñado en universidades desde entonces hasta
el punto que podría decirse que en algún punto de la historia *todas* las
personas involucradas en la informática conocían el lenguaje en detalle. Este
hecho ha alterado el mundo de la informática haciendo que casi todos los
lenguajes de programación de uso común inspiren su sintaxis en C, estén
escritos en él o extiendan conceptos de éste.

C++, por su lado, ha brillado especialmente en entornos de software empresarial
(*enterprise software*)[^enterprise-software], donde la robustez es importante
debido a los requerimientos del entorno. Esto lo ha hecho incluso aún más
influyente que C en su propio sector de excelencia donde ha impulsado la
creación de otros lenguajes que casi replican su comportamiento con algunos
detalles añadidos (Java, Go...), por lo que conocer C++ permite acudir a la
fuente de todo este conocimiento y abre la puerta a todo el conocimiento que
puede obtenerse desde C que, aun sin ser el mismo lenguaje, será mucho más
fácil de entender una vez dominado C++ o viceversa.


[^kr]: Brian Kernighan, a sus 78 años, y siendo historia viva de la
  informática, aún imparte charlas y lecciones y es colaborador habitual de
  programas relacionados con la programación como Computerphile. Dennis
  Ritchie, por desgracia, ya no nos acompaña.

[^posix]: POSIX es un estándar que tiene como intención mantener una
  compatibilidad entre diferentes sistemas operativos.

[^num]: Por ejemplo: [ISO/IEC 9899:1999](https://www.iso.org/standard/29237.html)

[^enterprise-software]: No sería justo olvidar que C++ tiene mucha presencia
  fuera del mundo empresarial. Es uno de los lenguajes más usados en el
  desarrollo de videojuegos, por ejemplo.


## C++ moderno

Este documento tiene como finalidad hacer hincapié en el C++ moderno. A pesar
de que la modernidad no tiene una definición estricta, en el caso de C++ es
sencillo trazar una línea.

C++11 fue un hito importante en la historia de C++, la inclusión de las
funciones lambda (aunque han sido extendidas más adelante), *move semantics*,
*smart pointers*, etc. Aportan mecanismos nunca antes conocidos en C++ y que
suponen un cambio radical en el lenguaje, que previamente era mucho más cercano
a C.

Es por eso que este documento, a pesar de estar escrito en 2020, se centra en
C++11 sin mencionar en detalle otros cambios que han ido sucediendo. Una sólida
base de C++11 es más que suficiente para comprenderlos, mientras que una sólida
base de C++98 podría estar demasiado lejos de ellos.

Por otro lado, los compiladores se construyen lentamente, ya que son programas
realmente complejos, sobre todo en el caso de C++. Existen compiladores para
los que los estándares más recientes aún no están soportados. C++11 es
suficientemente antiguo como para estar soportado correctamente por casi
cualquier compilador que merezca la pena utilizar.

Más allá de esto, muchos proyectos aún a día de hoy (2020) siguen usando
estándares antiguos, incluyendo C++98, por diferentes razones de diseño, por su
propia antigüedad o por simple costumbre. Los que pretenden actualizarse con
un lenguaje con características de más alto nivel, con buen soporte, y
fácil de aprender suelen optar por saltar a C++11 ya que el salto cualitativo
es grande pero manejable.
