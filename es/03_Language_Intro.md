# Introducción al lenguaje

Pese a todo lo discutido, la verdad es que se ha hablado poco del lenguaje de
programación en sí mismo y se han centrado los esfuerzos en la plataforma. Esto
es así porque es absolutamente fundamental comprender, no sólo en este lenguaje
de programación, que el contexto de herramientas y la implementación son casi
tan importantes como el propio lenguaje.

En este apartado se pretende, finalmente, introducir los primeros conceptos del
propio lenguaje que, lateralmente, traerán conceptos sobre arquitectura de
ordenadores en más de una ocasión.


## Sintaxis general

El lenguaje dispone de ciertos ingredientes que forman su esqueleto principal.
Estos ingredientes se expresan a continuación de forma general y simple. En
algunos casos sus descripciones requieren de más información y se irán
completando en el resto del documento, mientras que en otros la explicación
aportada será más que suficiente para entender a qué se refieren.

### Comentarios

El código fuente puede comentarse para facilitar la comprensión. Estos
comentarios (*comment*) se ignoran por el compilador, pero, usados
correctamente, aportan pistas interesantes para quien tenga que leer o editar
el programa.

En C++ existen dos tipos de comentarios.

Uno de ellos se indica con `//` lo que marca todos los caracteres restantes en
la línea como comentario.

El otro debe indicar tanto el inicio como el fin del comentario, pudiendo
abarcar secciones de una línea a multitud de ellas. La apertura debe indicarse
con `/*` y el cierre con `*/`.

Aunque no se añade ningún ejemplo aquí, habrá multitud de ellos por el
documento y se utilizarán con profusión para describir la funcionalidad de lo
que se muestre.


### Sentencias

Los programas de C++ están formados por sentencias (*statement*) que se
ejecutan de forma secuencial[^secuencial].

[^secuencial]: De arriba a abajo en el documento y de izquierda a derecha, como
  el orden de lectura y escritura del inglés, lenguaje en el que, por suerte o
  por desgracia, se basan la mayoría de los lenguajes de programación, que
  comparte orden de lectura con el español.

Las sentencias pueden tomar varias formas distintas, algunas muy complejas,
pero todas ellas quedan englobadas por lo que se acaba de describir: son piezas
de código que se ejecutan de forma secuencial. La mayoría de ellas terminan en
punto y coma (`;`) indicándole al compilador el fin de la sentencia a lo que
suele seguir un salto de línea para facilitar la lectura. Esto no es cierto en
todos los casos, ya que existen formas de agrupar sentencias o formar
sentencias complejas que serán estudiadas posteriormente.

Las sentencias pueden etiquetarse, aunque no sea muy común, añadiendo la
etiqueta que se desee seguida de dos puntos (`:`) antes de la sentencia. Cada
sentencia puede etiquetarse en una o más ocasiones. Estas etiquetas sirven para
hacer referencia las sentencias etiquetadas como se verá más
adelante[^label-goto]. Estas etiquetas se comportan como cualquier otro
identificador: las normas sobre nombres se les aplican, tienen un *scope*
asociado, etc.

[^label-goto]: Por ejemplo, para saltar a ellas mediante la sentencia `goto`.

Además de las etiquetas, las sentencias pueden recibir atributos, cosa aún más
infrecuente que las etiquetas. Los atributos fueron añadidos en C++11, con la
intención de aportar una forma estándar de usar atributos que diferentes
implementaciones usaban de forma independiente. Los atributos se indican entre
corchetes dobles: `[[ atributo ]]`. Y hacen referencia a la sentencia añadida
tras ellos. Las etiquetas también pueden recibir atributos si éstos se añaden
delante de la etiqueta misma.

A medida que se avance en el estudio se irán añadiendo nuevos tipos de
instrucciones y su sintaxis al vocabulario. De momento, con conocer a qué se
refiere el concepto es suficiente.


### Palabras clave

Los programas de C++ hacen uso de un conjunto de palabras clave o *keywords*
que indican que alguna acción especial debe tomarse. Estas palabras clave están
reservadas (también se conocen como *reserved keywords*) y no pueden
reutilizarse porque el lenguaje las necesita para sus funcionalidades propias.

A continuación una lista poco legible de palabras clave. El hecho de que sea
poco legible es intencionado. No es necesario estudiarlas de memoria para saber
programar.

``` cpp
alignas, alignof, and, and_eq, asm, atomic_cancel, atomic_commit,
atomic_noexcept, auto, bitand, bitor, bool, break, case, catch, char, char8_t,
char16_t, char32_t, class, compl, concept, const, consteval, constexpr,
constinit, const_cast, continue, co_await, co_return, co_yield, decltype,
default, delete, do, double, dynamic_cast, else, enum, explicit, export,
extern, false, float, for, friend, goto, if, inline, int, long, mutable,
namespace, new, noexcept, not, not_eq, nullptr, operator, or, or_eq, private,
protected, public, reflexpr, register, reinterpret_cast, requires, return,
short, signed, sizeof, static, static_assert, static_cast, struct, switch,
synchronized, template, this, thread_local, throw, true, try, typedef, typeid,
typename, union, unsigned, using, virtual, void, volatile, wchar_t, while, xor,
xor_eq
```

### Secuencias de escape

Como el lenguaje hace uso de variedad de caracteres especiales, es necesario
tener un mecanismo para diferenciarlos de los casos en los que se refieran al
literal y no a su significado dentro del lenguaje. El mecanismo de escape se
basa en la inclusión de un caracter, `\`, que activa una interpretación
especial del siguiente.

El ejemplo más evidente es la definición de las cadenas de caracteres, que ya
se analizará en profundidad más adelante. En C++ una cadena de caracteres se
indica entre comillas dobles, de la siguiente manera:

``` cpp
"Esto es una cadena de caracteres"
```

Pero si una cadena de caracteres quisiera incluir unas comillas dobles en su
interior, el compilador creería que la cadena termina antes de lo debido:

``` cpp
"Esta cadena está "rota""
               // ^ terminaría aquí.
```

Para evitarlo, basta con añadir el caracter de escape justo antes de las
comillas dobles.

``` cpp
"Esta cadena no está \"rota\""
               //     ^     ^  se consideran literales
```

Así, la cadena de caracteres del último ejemplo contendría `Esta cadena no está
"rota"` literalmente.


Además de este funcionamiento, existen algunas secuencias que activan la
inserción de caracteres especiales que son difíciles de introducir de otro
modo. Por ejemplo `\n`, línea nueva, o `\t` tabulación y, por supuesto, `\\`
para que el caracter de escape pueda representarse a sí mismo de forma literal.


## Tipos fundamentales

Considerando un ejemplo heredado de las matemáticas y convertido a instrucción
del lenguaje:

``` cpp
x = y + f(2);
```

Para poder convertir este ejemplo con éxito a un programa C++ cada variable
debe estar debidamente declarada. Debe especificarse que las entidades `x`, `y`
y `f` existen y de qué tipo deben ser para que los operadores de asignación
(`=`), suma (`+`) y llamada a la función (`()`) tengan sentido.

Cada identificador en un programa de C++ tiene un tipo asociado. Por ejemplo:

``` cpp
float x;      // Una variable numérica de coma flotante
int y = 7;    // Una variable numérica de tipo entero inicializada en valor 7
float f(int); // Una función que recibe in valor entero y retorna un valor de
              // coma flotante
```

Estas instrucciones declaran la existencia de los indicadores en el programa y
les asignan un tipo para que el resto del programa sea capaz de conocerlos y
usarlos.

Declarando estas variables de ese modo previamente, el ejemplo superior
comenzaría a tener sentido dentro de C++.


### Booleano

Los booleanos pueden tener valor `true` o `false` y se declaran con la palabra
reservada `bool`:

``` cpp
bool a;
```

Normalmente se utilizan en funciones que comprueban si algo es cierto.

Por definición, los valores booleanos se convierten a valores enteros asignando
`1` a los valores `true` y `0` a los `false` y considerando cualquier valor
distinto de `0` un valor `true` en caso de la transformación opuesta.

``` cpp
bool a=9; // 9 es diferente de 0, por lo que se asigna `true`.
```

### Caracteres

Los caracteres se definen mediante la palabra reservada `char` generalmente, la
cual almacena los caracteres básicos de la propia implementación. Normalmente
tienen un tamaño de un byte.

Existen otros tipos de caracter también: `signed char` interpreta el primer bit
como signo, pudiendo almacenar valores positivos o negativos; `unsigned char`
funciona igual que `char` pero asegura que el dato no tenga signo; `wchar_t`
se usa para los caracteres más grandes que la implementación pueda tener (como
Unicode) y, finalmente `char16_t` y `char32_t` existen con el fin de gestionar
caracteres de 16 y 32 bits respectivamente (como podrían ser UTF-16 y UTF-32).

Dependiendo de la implementación, `char` puede funcionar como un `signed char`
o como un `unsigned char`, siendo más común lo segundo.

Los caracteres literales en C++ se indican con apóstrofes
(`'`). A continuación una definición de un caracter `ch` que toma el valor
literal `a`:

``` cpp
char ch = 'a';
```

La gestión de caracteres es un tema complejo, principalmente por la cantidad de
idiomas y diferentes tipos de codificación existentes. Aunque no se tratará en
este documento, cabe reseñar que los caracteres de tipo `char` están diseñados
para gestionar los caracteres de la propia implementación, por lo que incluye
cualquier caracter que el lenguaje pueda usar y que puede que no aparezcan en
el alfabeto del idioma de la implementación (por ejemplo: `{`, `\`...).

Normalmente los caracteres tipo `char` son algún tipo de variante del estándar
ISO-646.

Respecto a las versiones con signo o sin signo de los caracteres, es posible
llegar a casos extraños como el siguiente:

``` cpp
char ch = 0xFF;  // 255 en hexadecimal o 11111111 en binario
int i = ch;      // asignar el valor a un número entero
```

Aunque aún no se han visitado los números enteros, con entender que `int` hace
referencia al tipo numero entero es suficiente para ver que no es posible saber
qué valor tiene `i` en este caso. Si `char` se comporta como un `unsigned char`
en esta implementación `i` debería ser `255`, si no es así, debería ser `-127`.
Como no existe modo de saber qué decisión de diseño tomó la implementación, es
muy probable que este código no sea portable de una implementación a otra.

A pesar de que los tres tipos sencillos de caracteres `char`, `unsigned char` y
`signed char` se comportan de modo similar, los punteros[^char-pointer] a ellos
no son intercambiables y muchas librerías aceptan sólo los de un tipo.

[^char-pointer]: Se estudiará más adelante.

#### Caracteres literales

Los caracteres literales se definen entre apóstrofes (`'`) como ya se ha
explicado. Lo que no se ha aclarado es que los literales serán siempre de tipo
`char`. Usar caracteres literales solventa problemas de portabilidad, ya que
éstos siempre se convertirán a su valor entero correspondiente en función de su
propia implementación.

Algunos caracteres deben definirse, como ya se adelantó en la sección sobre
secuencias de escape, mediante más de un caracter. Es el caso de los caracteres
de nueva línea, que deben definirse del siguiente modo: `'\n'`

Existen modos para definir caracteres mediante su representación octal y
hexadecimal también aunque quedan fuera del posible contenido de este
documento.

### Enteros

Los números enteros se declaran con la palabra reservada `int` generalmente,
pero, de nuevo, igual que con los `char`, existen otros subtipos.

Como es predecible, los tipos `unsigned int` y `signed int` existen y funcionan
tal y como sus hermanos `char`. Lo que no es tan predecible es que los `int`
siempre tienen signo, a diferencia de los `char` donde depende de la
implementación.

Además, los números enteros pueden tomar varios tamaños distintos. Existen los
`short int`, `long int` y los `long long int` más allá de los propios `int`
«normales», todos ellos con sus versiones `signed` o `unsigned`. La palabra
`int` puede obviarse en todos estos casos y sus combinaciones.

Los enteros largos (`long`) y cortos (`short`) sirven a las diferentes
implementaciones de mecanismos para crear variables de números enteros de
diferente longitud, sin embargo, en algunas implementaciones con limitaciones
de contorno es posible que no exista tanta variedad de éstos. Por si esto fuera
poco, ninguna de estas palabras claves indica concretamente la longitud que
cualquiera de estos casos debe tener, por lo que puede ser distinta según la
implementación.

Como estos números enteros se complican bastante, es posible utilizar la
librería estándar `cstdint` que recoge definiciones exactas de diferentes tipos
de enteros de longitudes claras. Por ejemplo, llamando `int64_t` a un entero
con signo de 64 bits de longitud.

#### Enteros literales

Los enteros literales pueden expresarse en su representación decimal, octal o
hexadecimal y se representan tal y como se hace en otros entornos: comenzando
por `0` para los octales, `0x` o `0X` para los hexadecimales y sin prefijos
especiales para los decimales.

Los sufijos `L` y `U` pueden usarse para indicar que el literal debe
interpretarse como `long` o `unsigned`, respectivamente.

### Números de coma flotante

En C++ hay tres tipos de números de coma flotante disponibles: `float` para
precisión simple, `double` para precisión doble y `long double` para precisión
extendida.  El significado de los diferentes tipos de precisión se define por
parte de la implementación.  Usar el tipo adecuado requiere un conocimiento
avanzado sobre el uso de computación con coma flotante[^float].

[^float]: Se pueden dar muchas incongruencias numéricas debido a pequeñas
  desviaciones en la precisión a la hora de trabajar con números de coma
  flotante.  Es todo un tema de estudio en sí mismo, sobre todo en entornos que
  requieren una precisión concreta, como la banca, por ejemplo.


#### Literales de coma flotante

Por defecto, todo literal con coma flotante se considera un `double`. Existen,
como en el caso de los `int`, sufijos que cambian su lectura así como cierta
variedad de formas de expresar los números de coma flotante, directamente
heredadas de las matemáticas: `1.45`, `0.2`, `.2`, `1e-78`, etc. Cualquiera de
las alternativas para representar un literal de coma flotante debe escribirse
**sin espacios**, si los tuviera se considerarían elementos distintos.

Si se quiere expresar un número `float` es necesario añadir `F` o `f` como
sufijo. Lo mismo para `long double` con `L` o `l`.

### El vacío

El vacío, `void`, es un tipo fundamental también. Usado únicamente para
declarar funciones que no retornan ningún valor o como puntero de algún objeto
de tipo desconocido, el vacío es un tipo peculiar ya que no pueden crearse
objetos de tipo `void`.


### Tamaños

El tamaño de los tipos básicos está completamente controlado por la
implementación, como ya se ha visto en los apartados previos. En ocasiones,
sobre todo en las que implican algún interés por la portabilidad de los
programas, es necesario tenerlo en cuenta.

Lo único que C++ garantiza es que los tamaños están ordenados tal y como se ha
contado, pero con la posibilidad de que sean iguales. Es decir, un `short` será
siempre más pequeño o igual que un `int`, que a su vez es más pequeño o igual
que un `long`.

El operador `sizeof` sirve para obtener el tamaño de los objetos en múltiplos
del tamaño de un `char`, así que:

``` cpp
sizeof(char) == 1      // true
```


## Declaraciones

Para poder usar cualquier identificador, éste debe ser declarado. La
declaración sirve para decirle al compilador qué tipo de entidad es el
identificador. Con ello, el compilador podrá comprobar si los tipos son
coherentes o si existe alguna incongruencia.

Muchas declaraciones (*declaration*) son además definiciones (*definition*).
Las definiciones son un tipo de declaración que describen la entidad declarada
en su totalidad. Una regla sencilla para diferenciarlas es:

> Si requiere memoria es una definición

Las declaraciones sirven para indicar que algo existe y, por tanto, pueden
usarse repetidas veces. Las definiciones, por contra, describen las entidades y
sólo pueden usarse en una ocasión en el programa. Definir dos veces el mismo
indicador lanzará un error en tiempo de compilación.

Es comprensible que a estas alturas no se entienda con claridad qué es una
declaración y en qué contextos puede ser útil. Se conocerá en detalle más
adelante. Mientras tanto, pensar en que la indicación de qué parámetros de
entrada recibe una función es una declaración puede servir como referencia:

``` cpp
int suma_enteros( int x, int y){
              //  ^^^^^^^^^^^^  Son declaraciones
    return x + y;
}
```

No se debe considerar que una definición requiere la inicialización del
identificador, aunque siempre que éste se inicialice se tratará de una
definición:

``` cpp
int a = 1; // Crea una variable `a` con valor `1`
int b;     // Crea una variable `b`
```

En ambos casos del ejemplo el programa creará una variable, por lo que estará
actuando sobre la memoria. Siguiendo la norma escrita previamente, como ambas
requieren memoria, ambas son definiciones.

Volviendo al ejemplo de la función, por contra, su declaración sí que necesita
memoria para la propia función, pero tanto `x` como `y` no se construirán
cuando la función aparezca, sólo avisan de que, cuando se la llame, la función
recibirá dos variables `int` que estarán definidas de antes, así que son
declaraciones.

Este mecanismo es mucho más necesario de lo que parece, así que conviene
conocerlo en detalle.

### Sintaxis de las declaraciones

Como ya viene siendo habitual, las declaraciones son realmente complejas. Se
componen de cinco partes principales:

1. Prefijos opcionales como `static`, `virtual` o `extern`. También llamados
   especificadores (*specifiers*), estos prefijos añaden atributos a lo
   declarado, que no tratan sobre su tipo. Por ejemplo, `extern` sirve para
   indicar que lo que se declara está definido en otro lugar.
2. El tipo base, como puede ser `int` o `const char`.
3. El declarador que puede tener opcionalmente un nombre. Los declaradores
   también son complejos, ya que pueden servir para declarar punteros (`*`),
   arrays (`[]`), referencias (`&`), funciones (`()`), y para añadir
   calificadores como `const` y `volatile`. Todos estos operadores pueden
   combinarse: `char *argv[]` es un array de punteros a caracteres, por
   ejemplo.
4. Sufijos adicionales asignables a funciones.
5. Un inicializador o cuerpo de la función.

Excepto en las definiciones de funciones o namespaces[^defs_f_n], todas las
declaraciones terminan en punto y coma (`;`).

Es posible declarar múltiples variables en una única declaración separándolas
con una coma, pero hay que tener en cuenta en todo caso que los operadores del
declarador sólo se aplican a su nombre y las declaraciones realizadas
comparten únicamente tipo base.

``` cpp
int x, y;       // Equivale a: `int x; int y;`
int *x, y;      // Equivale a: `int *x, int y;`. Cuidado: no es `int *y`
int v[10], *pv; // Equivale a: `int v[10]; int* pv;`
```

[^defs_f_n]: Las definiciones de funciones y namespaces tienen un cuerpo que
  agrupa múltiples sentencias y se delimita con llaves (`{`, `}`). En los
  casos que se añaden llaves, en general, el lenguaje no requiere de `;` para
  delimitar la sentencia, porque las llaves le sirven de delimitador. No
  confundirse con las declaraciones de funciones que, al no llevar cuerpo, sí
  que requieren el delimitador `;`.

### Nombres

Los nombres deben cumplir unas normas para que sean compatibles con el resto
del lenguaje y se puedan identificar por el compilador de forma correcta. Las
normas son, por una vez, sencillas.

Los nombres son un conjunto de letras y dígitos que deben empezar por una
letra. Se considera que la barra baja (`_`) es una letra.

En principio, C++ no tiene límite en la longitud de los nombres, aunque las
implementaciones pueden llegar a tenerla.

El compilador diferencia entre mayúsculas y minúsculas, pero se recomienda no
añadir nombres que sólo difieran por este detalle.

Las diferentes implementaciones pueden y suelen inyectar variables en los
programas que definen detalles de la implementación, lo harán siempre en el
contexto global y comenzando con `_` por lo que evitar usar `_` como primer
caracter en las variables es una buena idea.

Además, los nombres que comienzan por doble `_` o `_` y después una letra en
mayúscula están reservados.

> Elegir buenos nombres es un arte que se debe tomar en serio ya que en los
> nombres reside gran parte de la legibilidad del programa.

### Scope

Para que el código pueda aislar diferentes módulos, los lenguajes de
programación hacen uso del concepto conocido como *scope*. El *scope* es el
espacio del programa donde se conoce un identificador o, dicho de otra forma,
el espacio del programa donde una declaración tiene efecto.

Recordando que cada identificador sólo puede definirse en una ocasión, las
reglas del *scope* cobran cierto sentido: es mucho menos probable crear dos
definiciones para la misma variable si éstas tienen un radio de actuación
limitado.

En C++ el *scope* se limita de varios modos:

- **Bloque**: un bloque de sentencias es cualquier conjunto de sentencias
  limitadas con llaves (`{}`). Cualquier declaración realizada en un bloque
  será accesible desde que se realiza hasta el final del bloque en el que se
  encuentra.
- **Clase**: el *scope* de las declaraciones realizadas a nivel de clase se
  extiende desde la apertura de la clase hasta su cierre, de `{` a `}`. Con *a
  nivel de clase* se quiere decir dentro de ésta pero fuera de cualquier
  declaración interna de la clase.
- **Namespace**: los *namespaces*[^namespace] son un mecanismo de separación de
  nombres. Las declaraciones dentro de ellos, pero que se realicen fuera de
  otras definiciones internas, se extienden desde su declaración hasta el final
  del *namespace*, como ocurre con las declaraciones de bloque.
- **Global**: Las definiciones globales son las que ocurren fuera de cualquier
  grupo de los que aquí se listan. Las declaraciones de tipo global son
  accesibles desde que aparecen hasta el final del archivo en el que se
  encuentran. En realidad funcionan como un namespace propio del archivo actual
  a pesar de no ser declarado de forma explícita.
- **Sentencia**: Las declaraciones a nivel de sentencia aparecen en los
  paréntesis (`()`) de las sentencias `for`, `if`, `while` y `switch`. El
  *scope* de éstas comienza en el lugar de la declaración y termina en el final
  de la sentencia.
- **Función**: las etiquetas dentro de una función se conocen desde su
  declaración hasta el final de la función.

[^namespace]: En español *espacio de nombres*, pero no se les suele conocer por
  su nombre en español. Normalmente se usa el original en inglés.

Una declaración puede esconder[^shadow] declaraciones de su *scope* padre,
haciendo que los cambios sólo ocurran en el *scope* actual y que al terminarlo
se restaure el valor del *scope* previo. Cuando una variable global ha sido
ocultada, es posible acceder al *scope* global y alterar sus valores
directamente usando `::` como prefijo en el nombre de la variable a acceder:

``` cpp
int g = 1;
int main(){
    int x = 0;      // Define la variable `x` con valor `0`

    {               // Abre nuevo bloque
        int x;      // Esconde la x previa
        x = 10;     // Asigna el valor 10 a la variable x actual

        int g = 50; // Esconde la g global con una g de valor 10
        ::g = 10;   // Asigna el valor 10 a la variable g global
    }               // Cierra el bloque

    // x vale 0 aquí
    // g vale 10 aquí
}
// g vale 10 aquí
```

No existe mecanismo para recuperar una variable local (no-global) que haya sido
ocultada, así que en el ejemplo es imposible acceder a la `x` de `main` desde
el bloque interior.

[^shadow]: Se conoce como *shadow*: tapar u ocultar en inglés.


### Inicialización

Aunque durante los ejemplos previos se ha utilizado el símbolo `=` para
inicializar valores, C++ dispone de mecanismos más complejos para esto. Desde
C++11, existe un modo de inicializar, la inicialización directa mediante una
lista inicializadora, entre llaves (`{}`), que se recomienda por encima de
cualquier otro:

``` cpp
tipo nombre { lista_inicializadora };
```

Sin embargo, siguen siendo válidos otros:

``` cpp
tipo nombre = expresión;
tipo nombre( lista_de_expresiones );
```

La diferencia entre ellos es notable a pesar de que en muchas ocasiones se usen
de forma intercambiable. La forma recomendada, la inicialización directa
mediante lista inicializadora, apareció en C++11 así que las otras tres
aparecerán más probablemente en código antiguo.  El uso del símbolo `=`, la
inicialización por copia, es probablemente la más común debido a que es la
única forma de inicialización que puede encontrarse en C.

La inicialización mediante `{}` aporta seguridad a la hora de inicializar
valores por no aplicar *narrowing*. El *narrowing* es una conversión de un tipo
a uno con menor precisión o tamaño, como puede ser de `long` a `int` o de
`double` a `float`. En definitiva, a cualquier tipo que no pueda representar el
valor con total exactitud. Estas limitaciones aseguran que los valores
asignados a las variables no se han corrompido en conversiones innecesarias y
obligan a quien desarrolla a inicializar las variables con un valor del tipo
adecuado.

> NOTA: Al usar `auto`, el indicador de tipo que deduce el tipo desde su
> inicializador, es más adecuado usar `=` ya que la sintaxis del inicializador
> de lista se interpreta como una lista y puede asignar un tipo inesperado.

El tercer método de inicialización descrito, el que hace uso de los paréntesis
(`()`) es una ejecución directa del constructor, también se considera
inicialización directa, pero sí que aplica narrowing. Se estudiará en el
apartado sobre clases, pero cabe adelantar que el valor a enviar al constructor
puede no ser el valor que se contiene en la variable. Un ejemplo evidente es
que en muchos constructores de vectores o listas en general, el valor requerido
por el constructor es su longitud, no el contenido de la lista.

Los tipos complejos pueden inicializarse usando `{}` siempre que se añadan los
diferentes elementos separados por coma en una inicialización de lista, por
ejemplo:

``` cpp
int a[] { 1, 2 };
```

Esta llamada crearía un `array` de dos enteros de valor `1` y `2`. Los tipos
definidos durante el programa se rellenan de forma similar, llamando a su
constructor con los argumentos introducidos dentro de los paréntesis.

#### Inicialización vacía

Para asignar el valor por defecto es posible usar la inicialización directa con
una lista vacía:

``` cpp
int a {};
```

El valor asignado a la variable será el valor por defecto de ese tipo, en una
variable de tipo entero el valor será `0`, en otras, será el equivalente al
cero. Por ejemplo, en una variable de tipo puntero será `nullptr`. En tipos
definidos por el programa, el valor por defecto debe definirse en el
constructor de modo que pueda usarse después.

#### Variables sin inicializar

En los objetos estáticos (*static*), globales o de *namespace* C++ asigna el
valor por defecto de forma automática aunque no se inicialicen. Sin embargo,
los objetos dinámicos no se asigna ningún valor concreto así que no está
determinado qué valor tendrán.

### Constantes

El prefijo `const` indica que la declaración es constante, lo que implica que
el valor declarado no puede alterarse en su *scope*.

Las definiciones de constantes deben inicializar el valor declarado.

``` cpp
const int a = 1;
a = 2;              // ERROR: No se puede alterar una variable constante
```

### Deducción de tipos

Existen dos mecanismos para deducción de tipos:

- En las inicializaciones, la palabra clave `auto` puede deducir el tipo a
  declarar desde su valor de inicio:
  ``` cpp
  auto a = 1; // Deduce que es un `int`
  ```
- Para casos más complejos en los que se desee obtener el tipo del resultado de
  la ejecución de una expresión existe `decltype()` al que entregándole una
  expresión es capaz de deducir el tipo de su resultado.


### Alias

Es posible crear alias para los nombres de los tipos, cosa que puede ser útil
por diferentes razones: facilita la centralización de un tipo, de modo que
puede cambiarse posteriormente en todo el programa sin problemas y facilita la
simplificación de nombres complejos, entre otras.

La palabra clave `using` es la encargada de la declaración de alias. En el
pasado, por herencia de C, se utilizaba la palabra clave `typedef` de una forma
similar. Las siguientes sentencias se pueden considerar equivalentes:

``` cpp
using int32_t = int;
typedef int int32_t;
```

Es necesario recordar que los alias no crean un tipo nuevo, sólo lo renombran.


## Acceso a memoria

C++, al igual que C, requieren que quien programe se haga cargo también de la
memoria. Es una tarea compleja que suele conllevar una atención especial al
detalle y suele ser la mayor fuente de errores si no se conocen los mecanismos
del lenguaje en detalle. Este apartado tiene como finalidad principal describir
estos mecanismos y su fundamento para que deje de ser un problema y la atención
se centre en lo importante: el algoritmo.

La memoria es un tema extremadamente interesante porque afecta de modo radical
al rendimiento de los programas. Esta es la razón por la que en algunos
lenguajes, como C++, se deja en manos del ser humano y no de un
algoritmo[^garbage-collector].

[^garbage-collector]: En otros lenguajes la memoria se controla de forma
  automática con un proceso conocido como *garbage-collector*.

### Modos de creación y vida útil

Antes de entrar a trabajar con las herramientas de gestión de memoria es
necesario conocer cómo gestiona la memoria el propio lenguaje.

En primer lugar, aunque la palabra objeto es una nomenclatura que ya se ha
utilizado a lo largo del documento, es necesario definirla de formalmente.

Los objetos son «algo en la memoria». Es un concepto genérico pero
extremadamente útil del que a continuación se extraen categorías en función de
su vida útil.

- *Automatic*: Generalmente, a no ser que se especifique lo contrario, las
  declaraciones de variables dentro de funciones son de tipo *automatic*. Se
  trata de variables que se eliminan de la memoria cuando salen de *scope*.
  Este tipo de objetos normalmente se almacenan en el *stack*.
- *Static*: Las declaraciones en el *scope* global, en *namespaces* o las
  declaradas con el prefijo `static` se crean e inicializan en una única
  ocasión a lo largo del programa y su posición en la memoria permanece
  inalterada durante la ejecución del programa.
- *Free Store*: Este tipo de objetos se construyen y eliminan manualmente
  haciendo uso de las operadores `new` y `delete`.
- *Temporary*: Se trata de objetos temporales como los resultados intermedios
  de ejecución. Su vida útil depende de su uso. Si un objeto temporal sólo se
  usa como valor en una declaración, su vida terminará en cuanto la declaración
  termine. Si se extrae la referencia de este objeto y ésta continúa en uso a
  lo largo del programa, el objeto se mantiene en memoria hasta que la
  referencia se elimine. Normalmente estos objetos son automáticos.
- *Thread-local*: Objetos asociados a un hilo (*thread*). Éstos objetos
  declarados con el modificador `thread_local` se crean cuando su hilo asociado
  comienza y se eliminan cuando termina.

En los objetos formados por varios sub-objetos la vida útil de los últimos
viene definida por la vida útil de los objetos de los que son parte.

Además de por su vida útil, los objetos en C++ pueden clasificarse por sus
capacidades. Antes de C++11, los objetos tenían dos capacidades distintas que
se definían con dos categorías: *rvalue* y *lvalue*. Los nombres, que comienzan
por *r* y *l* de *right* (derecha) y *left* (izquierda) hacen referencia a los
lugares que normalmente ocupan estos valores en una asignación:

``` cpp
int a = 1;  // `a` es un lvalue y `1` es un rvalue
```

Estas dos categorías se diferencian principalmente por la capacidad de los
objetos por ser identificados. Los *lvalues* son cualquier objeto identificable
a través de un nombre, como en el ejemplo es `a`. Mientras que los *rvalues*
son objetos que no son *lvalues*, normalmente valores intermedios en ejecución,
literales y este tipo de elementos.

Desde C++11 con la inclusión de lo que se conoce como *move semantics* es
necesario extender la categorización a algo más complejo incluyendo todas las
diferentes posibilidades entre estas dos diferentes capacidades:

- Ser identificable, como los *lvalues* en la descripción previa.
- Ser movible, un concepto nuevo introducido por *move semantics*, define la
  capacidad de un objeto para ser movido a un lugar en la memoria distinto
  dejando el objeto original en un estado válido pero no especificado sin
  afectar al resto del programa.

Con las diferentes combinaciones de estas características se definen los
siguientes grupos:

- Identificable: *glvalue* (*generalized lvalue*)
    - Identificable pero no movible: *lvalue*
    - Identificable pero movible: *xvalue* (*expiring value*)
- Movible: *rvalue*
    - Movible pero no identificable: *prvalue* (*pure rvalue*)
    - Movible pero identificable: *xvalue* (repetida previamente en la
      categoría «identificable»)

Generalmente pensar en la separación movible vs. no-movible (o *lvalue* vs
*rvalue*) es más que suficiente para comprender el comportamiento de los
valores, pero es necesario mencionar el resto debido a que la realidad es más
compleja y en entornos que requieren gran nivel de control es necesario llegar
a este nivel de detalle.

En los próximos apartados se define en mayor detalle el comportamiento de los
*lvalues* y los *rvalues* y en ellos se encontrará el verdadero valor de esta
diferenciación.

### Punteros

Los punteros son el instrumento básico de C++. Son una variable que no indica
un valor, sino que guarda una referencia a un espacio en memoria donde se
almacena uno.

Los punteros deben conocer el tipo al que están haciendo referencia porque, de
otro modo no serían capaces de leer ese contenido. Por esa razón, la
declaración de un puntero parte de la declaración del tipo al que apunta y se
le añade un asterisco (`*`) de la siguiente manera:

``` cpp
char a;   // Un caracter
char *b;  // Un puntero a un caracter
```

Para que los punteros tengan cierto sentido es necesario describir la operación
que permita obtener la referencia de un valor. En C++ esta operación se realiza
con el operador `&`. Se aplica del siguiente modo:

``` cpp
char a;       // Un caracter
char *b = &a; // Un puntero llamado `b` hace referencia a `a`
    //    ^^  obtiene la referencia a `a`
```

El asterisco también sirve para *de-referenciar* un puntero, es decir, para
acceder al valor al que hace referencia:

``` cpp
char  a = 'A';  // Un caracter
char *b = &a;   // Un puntero que hace referencia al caracter previo
bool  c = *b == 'A'; // `true` porque `b` hace referencia a `a`, que es 'A'
       // ^^  Es 'A'
```

Aunque esta sintaxis parezca sencilla, es fácil entender por qué debe
complicarse en algunos casos, como en los punteros a funciones, ya que es
necesario indicar con corrección qué parte es a la que se le debe aplicar el
indicador de que se trata de un puntero. Aunque aún no se ha hablado de cómo se
definen funciones, el siguiente ejemplo lo ilustra con bastante claridad:

``` cpp
int *i;               // Puntero a integer
int *function(int);   // Función que recibe un integer y devuelve un puntero a
                      // un integer
int (*function)(int); // Puntero a función que recibe un integer y devuelve
                      // otro
int *(*function)(int*);  // Puntero a función que recibe un puntero a un
                         // integer y devuelve un puntero a un integer
int **i;                 // Puntero a un puntero a un integer
```


#### Los punteros y la memoria

Los punteros hacen uso del mecanismo de memoria de la máquina que ejecuta el
programa. Cuando se habla de referencias, se habla literalmente de direcciones
de memoria y acceso a ésta. Cuando se declara una variable se le asigna un
espacio de memoria, espacio que tiene una dirección asociada. Cuando se aplica
el operador `&` la referencia obtenida es la dirección asociada a esa variable.

Un puntero no es una excepción. Como variable que son, su creación conlleva
la reserva de un espacio en memoria para ellos. Normalmente, se dedica ese
espacio para almacenar la dirección de algún objeto, haciendo uso del operador
`&`. De este modo, el puntero no es más que una variable que en lugar de
contener un valor *lógico*, contiene la dirección de dónde se encuentra éste.

Como las direcciones de la memoria son simples números que identifican
secciones de ésta, es posible aplicar aritmética sobre ellas y saltar de una
dirección a otra aunque eso conlleve un problema adicional: cada dirección
almacena un tipo distinto, y cada uno de ellos ocupa un tamaño distinto en la
memoria y se podrían leer de forma incompleta si no se hiciese correctamente.

El hecho de que los punteros sepan el tipo al que acceden facilita la labor
aritmética para varias aplicaciones como pueden ser los *array*.

#### Punteros vacíos

Existe la posibilidad de usar direcciones sin conocer a qué tipo apuntan, sobre
todo para casos en los que se acceda a la memoria de forma genérica. Para ello
pueden utilizarse los *punteros vacíos* o `void*`. Éstos permiten asignarles
cualquier tipo de dirección y podrán después ser asignados a un puntero con
tipo para interpretar la dirección a la que apunten como si de un tipo se
tratara.

En este caso, aplicar algún tipo de aritmética es imposible, ya que al no
conocer el tipo al que se va a acceder, el compilador no sabría qué tipo de
aritmética aplicar en ellos.

#### Punteros nulos

Para asignar un valor inicial a los punteros que no apunte a ningún lugar se
utiliza el literal `nullptr`. Es un literal de tipo puntero así que sólo puede
asignarse a variables puntero:

``` cpp
void *a = nullptr;
char *b = nullptr;
int   c = nullptr;     // ERROR: `c` no es de tipo puntero.
```

El mismo `nullptr` es válido para cualquier variable de tipo puntero. Es
lógico, ya que un puntero siempre será del mismo tamaño, el identificador de
tipo sólo sirve para cuando el puntero se *de-referencie*.

> NOTA: antiguamente se usaba el valor 0 para esta labor, pero éste no
> comprobaba los tipos de la asignación, así que es más adecuado usar
> `nullptr`.

#### Punteros constantes

Cuando se usan punteros existen dos objetos: el puntero y el valor al que
accede. Al indicarse como constante mediante `const` sólo se indica el que el
valor es constante y no el puntero, por lo que la referencia del puntero podría
cambiarse por otra.

Pueden crearse punteros constantes usando `*const` en lugar de `*`.

`const*` no existe, por temas de legibilidad, añadir `const` después del tipo
es equivalente a añadirlo antes.

``` cpp
const int * a;          // Puntero a un integer constante
int const * b;          // Puntero a un integer constante (equivalente a `a`)
int *const c;           // Puntero constante a un integer
const int *const d;     // Puntero constante a un integer constante
int const *const e;     // Puntero constante a un integer constante
                        // (equivalente a d)
```

### Arrays

Los *array*[^array], son agrupaciones ordenadas y  contiguas de datos del mismo
tipo que facilitan el acceso a los elementos por su posición.

[^array]: En español la palabra *array* se traduce como vector, pero en C++
  también existe el tipo `vector` en la librería estándar, así que se ha
  preferido usar el nombre en inglés para evitar confusiones e introducir la
  terminología adecuada.

En C++ los *array* se gestionan a través de punteros y de operaciones
aritméticas sobre éstos.

Analizándolo desde la perspectiva de la memoria, un `array` de *n* elementos
ocupará *n* veces el tamaño de un elemento y usará espacios contiguos con lo
que, si ocupan `m` espacios de memoria cada uno, la dirección del segundo será
la dirección del primero más `m`.  Generalizando, la posición en memoria de
cada elemento respecto al inicio del *array* será `m*i`, siendo `i` la posición
del elemento en el *array*.

Para no tener que realizar estas operaciones matemáticas, C++ dispone de lo que
se conoce como *pointer arithmetic*, aritmética de punteros, y ciertas ayudas
de sintaxis que facilitan la creación y el acceso a los valores de los *array*.

Para declarar un *array* basta con indicarle la cantidad de elementos que se
desean construir y, evidentemente, su tipo:

``` cpp
float f[10];  // Array de 10 elementos de tipo `float`
char *c[100]; // Array de 100 punteros a caracter
```

El tamaño del *array* debe conocerse en tiempo de compilación, para que el
compilador sepa cuanto tamaño asignarles. Usar una variable en la asignación de
tamaño resulta en un fallo de compilación.

El operador `[]`, *subscript operator*, también sirve para acceder a la
posición correspondiente del *array*:

``` cpp
float f[10];
f[0] = 0.0;             // asignar valor a la posición `0`
f[1] = 1.0;             // asignar valor a la posición `1`
f[2] = 2.89;            // asignar valor a la posición `2`
float a = f[2];         // obtener el elemento de la posición `2` del array:
                        // a == 2.89
```

> NOTA: Las posiciones empiezan en `0`.

Como C++ usa punteros para gestionar *array*s, es posible usar lógica de
punteros para accederlos. **El nombre del *array* no es más que la dirección de
la primera posición de éste (la posición `0`)**. A partir de ahí es posible,
pero no recomendable, aplicar la aritmética de punteros para acceder.

``` cpp
float f[10];
*f = 0.0;               // asignar valor a la posición `0`
*(f+1) = 1.0;           // asignar valor a la posición `1`
*(f+2) = 2.89;          // asignar valor a la posición `2`
float a = *(f+2);       // obtener el elemento de la posición `2` del array:
                        // a == 2.89
```

La aritmética de punteros es capaz de saltar a la dirección de memoria adecuada
para cada tipo, de modo que sea indiferente que se trate de un `float` como en
el ejemplo o de un tipo de diferente tamaño.

> NOTA: También es posible acceder a posiciones fuera de un *array*, por lo que
> es necesario conocer la longitud del *array* en todo momento ya que el acceso
> siempre funcionará, pero no se puede garantizar que la lectura y la escritura
> se comporten debidamente, ya que se estaría accediendo a secciones de la
> memoria que escapan al control del programa.

Los *array*s son una base, evidentemente rudimentaria, para construir
estructuras más complejas por lo que debe comprenderse al detalle aunque en
producción sea más recomendable usar estructuras de datos avanzadas como
`array`[^array-array] o `vector` de la librería estándar.

[^array-array]: De nuevo un problema terminológico. *Array* es el nombre que el
  idioma inglés utiliza para denominar lo tratado aquí: un conjunto de valores
  con una posición. Existen, sin embargo, diferentes maneras de aplicar este
  concepto. La recientemente explicada es la función básica de C++ para
  gestionar *array*s, sin embargo dispone de otras implementaciones más
  avanzadas, probablemente construidas sobre ésta, en la librería estándar.
  Fueron llamadas `array` y `vector` porque implementan el mismo concepto, pero
  no son parte de la funcionalidad básica del lenguaje y no deben confundirse
  con ella.

#### Arrays multidimensionales

Es posible declarar *arrays* de más de una dimensión, como si de un *array* de
*arrays* se tratara:

``` cpp
int matrix[10][10]; // Array bidimensional de 10x10 elementos
```

Sin embargo, los *arrays* multidimensionales se construyen como un *array*
único que alberga todos los elementos del *array*. Lo que lo convierte en un
*array* multidimensional es su forma de acceder.


#### Inicialización de arrays

Los *arrays* pueden inicializarse como cualquier otro tipo:

``` cpp
int i[3] {2,5,98};
```

Si el literal tuviera más campos que el array declarado saltaría un error, ya
que no podrían asignarse. Si faltaran espacios, éstos serían rellenados con el
valor `0` o un valor vacío equivalente.

No existe un modo automático de copiar el contenido de un *array* en otro así
que tampoco es posible hacer *paso por valor* con *array*s.  Por eso es más
conveniente usar los tipos indicados previamente: `array` y `vector`.

Los *array*s de caracteres pueden inicializarse con literales de cadenas de
caracteres (*string*).

##### Cadenas de caracteres (*string*)

Los *string*s son conjuntos de caracteres, un *array* de ellos si se piensa a
bajo nivel, que sirven para representar texto.

Debido a la utilidad de los *strings*, en C++ pueden escribirse *strings*
literales aunque no exista un tipo específico para ellos. Al igual que en C,
los strings terminan en el caracter nulo (`\0`), así que son más largos de lo
que parecen.

Hay que tener cuidado porque se trata de literales inmutables (`const char[]`),
para poder alterarlos deben usarse para inicializar una variable alterable (un
*array* de `char`, por ejemplo).

``` cpp
char string[10] = "Hola"; // Se crea un array de 10 elementos pero se le
                          // asignan los primeros 5.

// La orden anterior es equivalente que hacer lo siguiente:
char string[10] {'H','o','l','a','\0'};
```

Como los *strings* son muy comunes existen diferentes maneras de delimitarlos.
Es posible tratarlos como texto crudo (*raw*) evitando que evalúen caracteres
de escape, separarlos en múltiples líneas para facilitar su lectura e incluso
usar delimitadores alternativos cuando el *string* se complica. Quedan fuera
del alcance de documento.

Al igual que con los `float` y los `int` literales, los *string* pueden recibir
prefijos para indicar su tipo. Se usará `L` cuando se trate de caracteres
anchos `u8` para referirse a *strings* UTF-8, etc.

### Referencias

Los punteros son una herramienta extremadamente útil ya que permiten acceder a
datos desde diferentes lugares sin necesidad de copiarlos. Sin embargo, su
sintaxis es compleja y no es uniforme con el resto del lenguaje
(dereferenciación, obtención de referencias, aritmética de punteros, uso de
sintaxis especial en los datos de tipo *struct*[^struct-ptr]...) y su uso puede
complicarse ya que pueden hacer referencia a diferentes objetos durante su vida
útil.

Estas complicaciones, que a veces son útiles, no son siempre necesarias por lo
que C++ aporta un mecanismo más sencillo y menos potente para gestionar el
acceso a los datos sin requerir de copias manteniendo el mismo nivel de
rendimiento que los punteros: las referencias (*reference*).

Las referencias se comportan de forma idéntica a un puntero, pero las
siguientes limitaciones han supuesto que puedan usarse con la misma sintaxis
que si del objeto original se tratara:

- Las referencias siempre hacen referencia al objeto del que se crearon. No
  pueden cambiarse.
- No existe la referencia nula. Toda referencia hace referencia aun objeto.

Se puede considerar que una referencia es un nombre alternativo para un objeto,
un alias.

[^struct-ptr]: Aún no se ha descrito en el documento. Se describe más adelante.

#### Referencias a *lvalue*

La creación de referencias a *lvalues* se realiza con el símbolo `&` de la
siguiente manera:

``` cpp
int a  = 10;
int &b = a; // `b` es ahora una referencia a `a`
b = 1;      // también altera `a` porque son el mismo objeto.
```

Toda referencia debe ser inicializada, como en el ejemplo, pero no debe
confundirse su inicialización con que se le asigne un valor ya que ningún
operador es capaz de afectar a una referencia (sólo afectan al dato
subyacente).

Este tipo de referencias es muy usado, sobre todo cuando se comparten datos con
funciones. En lugar copiar los datos, es más fácil usar una referencia dentro
de las funciones así no se pierde legibilidad y se obtienen mejoras de
rendimiento y capacidad para alterar los valores de entrada.

#### Referencias constantes

En ocasiones es interesante evitar alterar los valores de entrada en funciones
o se pretende dar acceso de sólo lectura a una variable, manteniendo acceso de
edición en otra zona del programa. Las referencias constantes aportan esa
funcionalidad:

``` cpp
int a = 10;
int &b = a;   // Referencia a `a`
const &c = a; // Referencia constante a `a`

c = 1;        // Error: asignación en referencia de sólo lectura
b = 1;        // Correcto, asigna `1` a `b` y, por consecuencia, a `a` y `c`
```

#### Referencias a *rvalue*

Las referencias a *rvalues* tienen como objetivo facilitar las alteraciones de
valores temporales que se supone que nunca más van a ser accedidos.

La razón principal por la que usar una referencia a un *rvalue* es la de
convertir una operación de copia en un cambio de referencia con el fin de
mejorar el rendimiento de un programa. Esta operación sólo tiene sentido si el
objeto es un valor temporal y no va a estar accesible en el futuro, debido a
que este tipo de operación es destructiva.

Las referencias a *rvalues* se definen con el símbolo `&&`:

``` cpp
int a = 19;
int &&b = a;      // ERROR: `a` es un lvalue
int &&b = a + 1;  // Correcto, a+1 es un rvalue
```

El uso de la referencia al *rvalue* es idéntica a como se haría con una
referencia a un *lvalue*, lo único que cambia es su declaración.

En caso de tratarse de un tipo deducido (con `auto`, por ejemplo), esta
notación indica una referencia de reenvío (*forwarding reference*), también
conocidas como referencias universales (*universal reference*), que puede ser
de un *lvalue* o un *rvalue* y tiene como objetivo el facilitar el reenvío de
argumentos de entrada a otras funciones.

``` cpp
int a = 1;

auto &&b = 2; // referencia a rvalue
auto &&c = a; // referencia a lvalue
```

La inclusión de referencias a *rvalues*, en C++11, habilita el concepto
conocido como [*Move semantics*], que permite convertir copias costosas en
simples movimientos de objetos y no se recomienda su uso fuera de ese contexto.

## Tipos compuestos

Gran parte del interés de usar lenguajes como C++, es la definición de tipos
nuevos. Los mecanismos propuestos en este apartado son los más sencillos para
creación de tipos compuestos y son el fundamento para comprender los tipos
complejos que son característicos de C++: las clases.

Los tipos descritos en este apartado son herencia directa de C, donde son el
único modo del lenguaje de implementar tipos compuestos.

### Estructuras

Las estructuras (*structure*) son agrupaciones de datos que pueden ser de
diferentes tipos. A diferencia de los *arrays* donde su declaración era
evidente, las estructuras deben declarar qué tipo de datos quieren poder usar.
La palabra reservada `struct` construye una nueva estructura del siguiente
modo:

``` cpp
struct person {
    const char * name;
    const char * address;
    int age;
    bool driver;
    float height;
    float weight;
};
```

Una vez definida la estructura, puede usarse como cualquier otro tipo:

``` cpp
person aiden;  // Declara una nueva variable, `aiden`, de tipo `person`
```

Los diferentes campos de la estructura pueden accederse mediante el operador
punto `.` tanto para alterarlos como para leerlos.

``` cpp
person aiden;
aiden.name   = "Aiden Pearce";
aiden.age    = 42;
aiden.height = 1.85;
aiden.driver = true;
```

#### Estructuras anónimas

Las estructuras que sólo van a usarse en una ocasión pueden obviar el nombre
del tipo y usarse directamente para declarar una variable. Tomando el ejemplo
anterior:

``` cpp
struct {
    const char * name;
    const char * address;
    int age;
    bool driver;
    float height;
    float weight;
} aiden;
```

De este modo, no es posible hacer referencia a la estructura que crea a
`aiden`, pero evita asignar nombres que no van a volver a utilizarse.

No es posible crear una estructura anónima que no cree ninguna variable.

#### Inicialización

Las estructuras se pueden inicializar con el inicializador de listas, como se
describió en su momento. En este caso es necesario seguir el orden declarado en
la estructura:

``` cpp
person aiden { "Aiden Pearce", "No Valid Address", 42, true, 1.85, 89 };
```

#### Punteros y referencias a estructuras

Puede hacerse referencia a una estructura mediante un puntero, para esos casos
existe una sintaxis alternativa para acceder a los campos de la estructura:
`puntero_a_struct->campo` es equivalente a `(*puntero_a_struct).campo`

Las referencias, sin embargo, son completamente transparentes y se usan como si
de la propia estructura se tratase. Como ya se adelantó en la sección sobre
referencias, ésta es una de la razones de su existencia.

#### Tamaño

Los campos de las estructuras se cargan en memoria en orden, ocupando el mínimo
tamaño posible. Tal y como ocurriría con un *array*. Pero las diferentes
limitaciones que la máquina pueda tener pueden crear espacios vacíos en las
estructuras si se intercalan datos de tamaño menor a una palabra de memoria con
otros que requieren una palabra completa. Agrupar los datos de mayor a menor
ayuda a reducir la cantidad de huecos en la estructura.

El operador `sizeof()` es capaz de calcular el tamaño final de la estructura
así que no es necesario hacer cálculos. Unos cálculos que serían poco
inteligentes si se busca crear código portable, ya que cada implementación
tendrá sus propias características.

#### Nombres

El nombre de la estructura está disponible tan pronto como se lee así que
pueden usarse punteros a la propia estructura en el cuerpo de la misma (es así
cómo se construyen las *linked lists*). Lo que no es posible es declarar
estructuras de forma recursiva.

Es posible cruzar estructuras de modo que una contenga un campo de otra
estructura y viceversa. Para lograr esto es necesario declarar una estructura
(pero no definirla) previamente, así la segunda conocerá el nombre de la
estructura a utilizar, pero deberá esperar a su definición antes de poder crear
un objeto de su tipo:

``` cpp
struct B; // Declara B

struct A{
    B campo;  // B está declarado, no hay error de sintaxis
};

struct B{     // Define B
    A campo;  // A está definido previamente, no hay error
};
```

Por herencia del lenguaje C es posible declarar variables que sean iguales que
el nombre de una estructura existente en el mismo *scope*. Para romper la
ambigüedad debe usarse `struct` por delante del nombre cuando se refiera a la
estructura. Por defecto C++ interpretará el nombre como el nombre de la
variable. Lo mismo ocurre con los tipos `union` y `enum` que se definirán a
continuación.

#### Relación con las clases

A nivel de implementación, las estructuras son una clase cuyos campos son
públicos. Esto implica que pueden definirse métodos (y más concretamente
constructores y destructores) en ellas y pueden heredarse de otras clases.

Como aún no se han tratado ni las funciones ni las clases no tiene sentido ir
más lejos en este punto.

#### Equivalencias

Dos estructuras se consideran tipos distintos independientemente de que
dispongan de los mismos campos exactos.

#### Campos reducidos

Las estructuras también pueden limitar la cantidad de bits usados para cada
campo. El objetivo de esto es el de usar estructuras para hacer referencia a
datos binarios estructurados que vienen definidos por la aplicación. Por
ejemplo, en la siguiente implementación de una cabecera del protocolo
IPv4[^ip], pueden apreciarse las reducciones en los campos `ihl` y `version`:

``` cpp
struct iphdr
  {
    unsigned int ihl:4;
    unsigned int version:4;
    uint8_t tos;
    uint16_t tot_len;
    uint16_t id;
    uint16_t frag_off;
    uint8_t ttl;
    uint8_t protocol;
    uint16_t check;
    uint32_t saddr;
    uint32_t daddr;
  };
```

[^ip]: Código obtenido del archivo `ip.h` del apartado `netinet` de la librería
  estándar de C del proyecto GNU conocida como Glibc, en su versión 2.29. El
  contenido mostrado ha sido alterado mínimamente para retirar conceptos que
  aún no han sido tratados.

Podría pensarse que el objetivo es reducir el tamaño de las estructuras, pero
la realidad es que, aunque lo hace, el tamaño del programa aumenta ya que es
necesario acceder a los campos por vías distintas a la habitual: al no ocupar
palabras completas de memoria, se requiere más procesamiento.

Existe una limitación fundamental en estos casos y es que las estructuras con
campos limitados no pueden obtener las direcciones de éstos. Es una limitación
lógica debido a que no tienen una dirección.


### Unions

El tipo `union` es un tipo concreto de `struct` cuyos campos son excluyentes.
Es decir, su tamaño es, como máximo, el del campo de tipo más grande.

Su comportamiento es similar al de las estructuras así que no se profundizará
en absoluto en las `union`. Basta con decir que son un mecanismo muy utilizado
en entornos con recursos limitados de memoria o en el lenguaje C, pero menos
comunes en C++. Entre los usos habituales de las `union` se encuentran la
simulación de  *polimorfismo* en un lenguaje sin clases, como es C, y la
re-interpretación de bits. Ambos casos pueden resolverse de formas más
elegantes con C++, mediante el uso de mecanismos del lenguaje diseñados para
ese fin por lo que no se profundizará en esos trucos.

### Unions anónimas

Existe la posibilidad de crear `unions` anónimas o sin nombre, pero a
diferencia de en las `struct`, las `union` anónimas pueden no crear ninguna
variable de su tipo:

``` cpp
union {
    int a;
    char b;
};
```

El ejemplo mostrado no parece tener sentido ya que se construye un tipo de dato
al que no se puede hacer referencia (no tiene nombre) y tampoco se construye
ninguna variable de su tipo al momento, así que tampoco se usa en una
operación.

En el caso de las `union`, y a diferencia de las `struct`, los campos internos
de éstas se exponen al *scope* padre cuando se declaran de forma anónima como
en el ejemplo. Así que `a` y `b` se comportarían como variables declaradas de
forma independiente con la particularidad de que sólo se reserva espacio para
la más grande de las dos.


### Enumeraciones

En C++ existen dos tipos distintos de enumeraciones, o `enum`, aunque ambas
tienen un fin similar: servir de un método sencillo para identificar nombres
con un número asignado. Una enumeración agrupa nombres con sus respectivos
números enteros asociados llamados *enumeradores* (*enumerator* en inglés).

Las enumeraciones tipo `enum class` son el formato de enumeración recomendado,
ya que fueron añadidas a C++ con el fin de solventar algunos errores que las
`enum` clásicas heredadas de C provocaban.

Las `enum class` aislan los nombres internos del *scope* principal, haciendo
que sólo sean accesibles dentro de ellas y sus valores no se convierten de
forma automática.

``` cpp
enum class Color {rojo, amarillo, verde};  // Declara la enum class Color
int b = rojo;                              // Error: `rojo` desconocido
int a = Color::rojo;                       // Error: imposible convertir
Color a = Color::amarillo;                 // Correcto


1 == Color::rojo                           // Error: tipos no comparables
a == Color::amarillo                       // true
a == Color::rojo                           // false
```

Las `enum` clásicas actúan del modo opuesto: sus nombres internos son
accesibles desde el *scope* actual y sus valores se convierten de forma
automática a otros tipos.

``` cpp
enum Color {rojo, amarillo, verde};  // Declara la enum Color
Color a = amarillo;                  // Correcto, `amarillo` en scope actual
int b = rojo;                        // Correcto, conversión automática
```

El problema que se puede apreciar fácilmente en el caso de las enumeraciones
clásicas es que los identificadores de una enumeración pueden colisionar con
otra o incluso con variables, resultando en un error. Además, las conversiones
automáticas complican el trabajo con enumeraciones, haciéndolo más
impredecible. Por esto, se recomienda usar `enum class`, pero mucho código C++
antiguo (C++98) y C está plagado de `enum` clásicas así que es necesario
conocer su comportamiento.

#### Opciones

El tipo del *enumerador*, aunque existe un algoritmo complejo para seleccionar
el tipo, puede seleccionarse manualmente en ambos tipos de enumeración:

``` cpp
enum Color : char{ rojo, amarillo, verde };
```

El algoritmo de selección de tipo de *enumerador* es complejo porque necesita
conocer la cantidad de valores internos y su valor asignado, que puede
seleccionarse manualmente. El algoritmo de selección de tipo de *enumerador* es
el siguiente:

- Cuando los *enumeradores* son positivos el rango de la enumeración es de 0
  (inclusive) a la potencia menor potencia de dos (`2`) capaz de albergar el
  valor máximo de la enumeración (no inclusive). Es decir: [0, X], donde X es
  una potencia de 2.
- Cuando los *enumeradores* tienen algún valor negativo, el algoritmo busca la
  potencia de dos más baja capaz de albergar todos los valores desde su valor
  negativo (inclusive) hasta su valor positivo (no inclusive). Es decir [-X,
  X), donde X es una potencia de 2.

El algoritmo busca, por tanto, el menor espacio posible para el *enumerador*.

Para asignar un *enumerador* concreto a cada valor, basta con añadirlo usando
el símbolo `=`:

``` cpp
enum Color {rojo = 100, amarillo = 2000, verde = 9000000};
```

Las enumeraciones pueden declararse sin definirse, y definirse más adelante.
Para esto es necesario que su tipo esté declarado.

``` cpp
enum Color : char;   // Declara sin definir
```

Las enumeraciones clásicas (`enum`) pueden crearse de forma completamente
anónima, puesto que sus campos internos se exponen al *scope* actual. De esta
forma se comportan como un grupo de constantes enteras.


## Sentencias

En los lenguajes imperativos, o basados en la imperatividad [^imperatividad],
como C++, las sentencias (*statement*) son una sección de código fuente que
implica la ejecución de una acción.

En C++ existe un conjunto amplio de sentencias, algunas complejas, que deben
conocerse para poder comprender el flujo del programa. Las declaraciones, por
ejemplo, que ya han sido introducidas con anterioridad son un tipo de
sentencia.

[^imperatividad]: La imperatividad implica que el lenguaje se basa en describir
  órdenes que deben ejecutarse en un orden concreto. Otros lenguajes, por
  contra, describen transformaciones o condiciones lógicas que deben cumplirse,
  pero no se basan en órdenes para llegar a su objetivo.

### Básicos

El símbolo punto y coma, `;` ,es una sentencia por sí mismo, la sentencia
vacía.

Es posible agrupar sentencias en bloques (*block*) usando las llaves `{}`, lo
que crea un nuevo *scope* dentro de éstas. También se conoce como sentencia
compuesta (*compound statement*). En cualquier caso, el bloque se comporta como
si de una única sentencia se tratase.

Las declaraciones son sentencias. Exceptuando las variables `static`, las
variables se declaran en el momento en el que la ejecución llega a su
declaración. Es posible que el compilador reordene las sentencias con el fin de
aplicar algún tipo de optimización, pero el resultado debe permanecer, en todo
caso, inalterado.

Las llamadas a funciones o asignaciones son expresiones, no sentencias.

Algunas sentencias incluyen condiciones para su funcionamiento. Las condiciones
pueden tomar dos formas:

1. Expresiones que evalúen a un valor
2. Declaraciones

Si se trata de una declaración, el valor declarado será el que se compruebe en
la condición. En ambos casos, las condiciones se convertirán, si es posible
hacerlo, a un valor `bool`.

Como se introduce en el apartado sobre el *[Scope]*, el *scope* de las
declaraciones realizadas dentro de la condición de las sentencias se extiende
por el cuerpo de toda la sentencia. Esto es especialmente útil en la sentencia
`ìf`, por ejemplo, que al tener dos ramas posibles que suelen declarase como
bloques, acaban aislando su *scope*. Añadir la declaración en la condición
solventa esta separación permitiendo a ambas ramas compartir las variables
declaradas.

Podría sin embargo realizarse la declaración fuera de la sentencia para
conseguir el mismo objetivo pero, de hacerlo, la variable declarada
pertenecería al *scope* padre, así que podría colisionar con alguna otra. Como
la recomendación es siempre usar las variables en el *scope* más reducido
posible, declarar variables dentro de las condiciones de las sentencias es más
que razonable.


### Sentencias de selección

Las sentencias de selección sirven para separar el flujo del programa en
diferentes ramas. Se suelen conocer como *condicionales*.

#### Sentencia `if`

Las sentencias `if` se definen con dos formatos:

- `if (` *Condición* `)` *Sentencia*
- `if (` *Condición* `)` *Sentencia* `else` *Sentencia*

El primero aplica la sentencia del cuerpo sólo en caso de que la condición sea
cierta y el segundo añade la sección `else` con una sentencia que se ejecutará
si la condición no es cierta.

Es habitual construir ramas usando sentencias compuestas (bloques definidos
entre llaves `{}`). Recordar que estos bloques crean un nuevo *scope* así que
si se declara una variable en una de las ramas la otra rama no podría acceder a
ella.

En sentencias `if` cuyo cuerpo no sea un bloque no está permitido hacer
declaraciones.

#### Sentencia `switch`

La sentencia `switch` elige entre un conjunto de etiquetas `case` en función de
una expresión. La expresión en las etiquetas `case` debe ser una expresión
constante de tipo básico o tipo `enum`. Los valores de las etiquetas `case` no
pueden repetirse.

La etiqueta `default` captura los casos no especificados con etiquetas `case`.

Las etiquetas se comportan tal y como se introdujo en el apartado sobre la
sintaxis general del lenguaje: sólo sirven para marcar una orden concreta. Con
esto, la sentencia `switch` es capaz de saltar a la sección correspondiente.

Como se trata de simples etiquetas, no existe ninguna limitación entre una y
otra así que es necesario romper la ejecución (usando la sentencia
`break`[^break]) si no se desea que los casos se ejecuten de forma
secuencial[^fallthrough]:


``` cpp
enum class Color {rojo, verde, azul};

Color c {Color::rojo};

switch(c){
    case Color::rojo:
        cout << "1";
    case Color::azul:
        cout << "2"; // Este caso también ocurrirá
}


switch(c){
    case Color::rojo:
        cout << "1";
        break;       // Rompe la ejecución de la sentencia switch aquí
    case Color::azul:
        cout << "2"; // Este caso no ocurrirá
}
```

[^break]: La sentencia `break` rompe el bucle actual, aunque también funciona
  para la sentencia `switch` (ver *[Rupturas de bucles]*).

[^fallthrough]: En general, el compilador avisará de que los casos se
  ejecutarán de forma secuencial, ya que es un comportamiento que pocas veces
  se aplica intencionadamente. Desde C++17, para acallar el aviso puede usarse
  el atributo `[[fallthrough]]` en el lugar donde se situaría el `break`
  aplicado sobre una sentencia vacía: `[[fallthrough]];`

Es posible declarar variables dentro de la sentencia `switch` pero sus
declaraciones no pueden saltarse por la propia funcionalidad de la sentencia si
van a usarse en otra sección ya que habría variables no inicializadas o no
declaradas.

Si se desea crear un nuevo *scope* para los diferentes casos, basta con añadir
una sentencia compuesta usando las llaves `{}`.

Aunque poco recomendable, también es posible saltar a las etiquetas usando la
sentencia `goto` que se estudiará más adelante.

### Sentencias de iteración

Existen dos sentencias básicas para crear bucles en C++, de las que se obtienen
cuatro alternativas diferentes. Los bucles tipo `for` facilitan la iteración en
los casos en los que existe una variable sobre la que iterar, las sentencias
basadas en `while`, sin embargo, facilitan la iteración para los casos en los
que no existe una variable sobre la que iterar y se controlan con una
condición. De todos modos, todas estas sentencias pueden intercambiarse unas
por otras alterando sus condiciones y variables de iteración, pero los más
adecuado es usarlas para la razón por la que se concibieron para que su
aplicación sea más sencilla, legible y fácil de optimizar para el compilador.

#### Sentencia `for`

Como se ha mencionado, la sentencia `for` dispone de dos formatos diferentes:

- `for (` *Sentencia de inicialización* `;` *[Condición]* `;` *[Expresión]* `)`
  *Sentencia*
- `for (` *Declaración de inicialización* `:` *Expresión* `)` *Sentencia*

El primero de ellos, se trata del formato clásico heredado desde C y el segundo
es un formato más moderno, introducido en C++11, diseñado para iterar sobre una
colección, elemento por elemento, de forma automática.

El `for` clásico, ampliamente conocido por quien ya ha programado en otros
lenguajes como C y otros más parte de una sentencia de inicialización, que
sirve para inicializar la variable de iteración. Añade después dos campos
opcionales separados por punto y coma (`;`). El primero de ellos, la condición,
es una condición que se evaluará en todas las ocasiones que se ejecute el
cuerpo de la sentencia, si la condición sigue cumpliéndose, el cuerpo volverá a
ejecutarse. La segundo es una expresión que debe aplicarse a la variable de
iteración para actualizarla cada vez que una ejecución termine.

El ejemplo clásico de uso de una sentencia `for` es el de iterar sobre los
elementos de un *array*:

``` cpp
const int length {10};
char vals[length];

for(int i = 0; i != length; ++i)
    vals[i] = 'a';
```

Es necesario recordar que las variables declaradas sólo existirán en el cuerpo
de la sentencia y que el cuerpo puede, y suele, ser una sentencia compuesta
delimitada por llaves (`{}`).

Respecto al `for` aplicable sobre colecciones, su uso es incluso más sencillo.
La declaración de inicialización indica qué variable debe usarse para la
iteración y la expresión después de los dos puntos (`:`) indica sobre qué
colección se desea iterar. El cuerpo del `for` se ejecutará tantas veces como
elementos tenga la secuencia, asignando el elemento actual a la variable que se
haya introducido en el campo de inicialización.

La clase `vector` es un tipo de secuencia que puede utilizarse en este tipo de
bucles `for`.

``` cpp
// (Se obvia la inclusión de la librería vector)
vector<int> vals(10, 10); // Construye un vector de 10 elementos de valor 10
int sum = 0;
for (int i : vals)
    sum += i;
```

En el ejemplo, `i` tomaría los valores del vector `vals` y no hay necesidad de
controlar la longitud de éste de forma manual.

En C++ una secuencia es cualquier objeto `x` en el que se puedan llamar los
métodos `x.begin()`, `x.end()` o, en su defecto, `begin(x)` y `end(x)`. Por
tanto, la expresión al lado derecho de los dos puntos (`:`) (`vals` en el
ejemplo) debe tener como resultado un objeto con esa capacidad.

En cualquier caso, la variable controlada (`i` en el ejemplo) será como copiar
el elemento de la colección correspondiente. Si se desea alterar su valor es
necesario usar una referencia en la declaración de la variable controlada:

``` cpp
vector<int> vals(10, 10);
for (int &i : vals)
    i = 22;
```

Es evidente que este segundo tipo de `for` es mucho más sencillo que el otro,
ya que aporta menos flexibilidad. Esto no es un problema ya que convertir de
uno a otro resulta sencillo, teniendo en cuenta su funcionamiento interno. El
ejemplo anterior, obviando algunos detalles de implementación, convertido a la
sentencia `for` clásica sería el siguiente:

``` cpp
vector<int> vals(10, 10);
for (auto p = vals.begin(); p!=vals.end(); ++p)
    *p == 22;
```

#### Sentencia `while`

La sentencia `while` es de las más sencillas, comprobará la condición y si es
cierta ejecutará el cuerpo de la sentencia y repetirá el proceso. Si la
condición es falsa su ejecución terminará.

- `while (` *Condición* `)` *Sentencia*

#### Sentencia `do`

La sentencia `do` es similar a la sentencia `while` la única diferencia es que
primero se ejecuta el cuerpo y posteriormente se comprueba la condición.

- `do` *Sentencia* `while (` *Condición* `)`

#### Rupturas de bucles

Aunque cada bucle tiene sus propias condiciones para terminar la iteración, en
ocasiones es necesario romper los bucles o alterarlos de forma manual. Para
ello C++ dispone de dos sentencias principales: `break` y `continue`.

La sentencia `break` rompe la ejecución de la sentencia de bucle (o `switch`,
ver *[Sentencia `switch`]*)
actual, mientras que `continue` termina la ejecución de la iteración actual
como si el cuerpo del bucle hubiera terminado comprobando la siguiente
condición y continuando con la ejecución de la sentencia de iteración.

Existen varias sentencias adicionales capaces de romper bucles: `return`,
`throw` y `goto`; y también es posible terminarlos con la llamada a la función
`exit()`, que termina la ejecución del programa de forma incondicional. Las
sentencias adicionales serán analizadas en su preciso momento, ya que `return`
es útil para usar funciones, que aún no se han descrito, `throw` es un modo de
activar excepciones, que se describirán más adelante, y `goto` altera el flujo
de programa de un modo que requiere una sección completa.

### Sentencia de salto: `goto`

C++ incluye la sentencia `goto`, probablemente por herencia del lenguaje C.
Como ya se ha adelantado en otras ocasiones, la sentencia `goto` tiene el
siguiente formato

- `goto` *Etiqueta* `;`

Las etiquetas, como se describe en la introducción a las *[Sentencias]*, se
indican con la siguiente estructura:

- *Etiqueta* `:` *Sentencia*

La sentencia `goto` saltará directamente a la sentencia marcada con la etiqueta
seleccionada.

Recordando que el *scope* de las etiquetas está limitado a nivel de función,
podemos observar que es posible saltar al interior o al exterior de un bucle.
Sólo existen dos limitaciones:

1. No es posible saltar más allá de una inicialización, ya que provocaría que
   algunas variables no estuvieran definidas correctamente.
2. No es posible saltar directamente a un gestor de excepciones. Esta
   limitación se debe, principalmente, a que se estaría accediendo a un gestor
   de excepciones sin una excepción que gestionar, un problema muy similar al
   caso anterior.

La sentencia `goto` ha sido causa de controversia desde los años sesenta, sobre
todo con la publicación de Edsger Dijkstra [*«Go to considered
harmful»*][^goto-harmful], que hasta creó un estilo para este tipo de
publicaciones, debido a que complica sobremanera la comprensión de los
programas. Si se usa, debe hacerse de forma controlada y legible.

[^goto-harmful]: Edsger W. Dijkstra, Communications of the ACM, Marzo de 1968,
  Vol. 11 No. 3, Páginas 147-148


### Resumen

El siguiente resumen cubre todos los tipos de sentencia de C++, de los cuales,
algunos no se han introducido en este capítulo aunque se hará más adelante.

Igual que en sus descripciones por sección, el texto marcado en fuente
`monospace` indica texto literal, mientras que el marcado en *cursiva* debe
interpretarse. Las secciones marcadas con corchetes (*[]*) implican que se
trata de una sección opcional. Las secciones más complejas se han separado más
abajo marcadas con un nombre propio. Otras, como *Declaración* o *Expresión* se
detallan en otras secciones del documento.

- *Sentencias*:
    - *Declaración*
    - *[Expresión]* `;`
    - `try` *Sentencia compuesta* *Lista de gestores de excepción*
    - `case` *Expresión constante* `:` *Sentencia*
    - `default :` *Sentencia*
    - `break ;`
    - `continue ;`
    - `return` *[Sentencia]* `;`
    - `goto` *Identificador* `;`
    - *Identificador* `:` *Sentencia*
    - *Sentencia compuesta*
    - *Sentencias de selección*
    - *Sentencias de iteración*

- *Sentencia compuesta*
    - `{` *[Lista de sentencias]* `}`

- *Sentencias de selección*
    - `if (` *Condición* `)` *Sentencia*
    - `if (` *Condición* `)` *Sentencia* `else` *Sentencia*
    - `switch (` *Expresión constante* `)` *Sentencia*

- *Sentencias de iteración*
    - `while (` *Condición* `)` *Sentencia*
    - `do` *Sentencia* `while (` *Condición* `)`
    - `for (` *Sentencia de inicialización* `;` *[Condición]* `;`
    *[Expresión]* `)` *Sentencia*
    - `for (` *Declaración de inicialización* `:` *Expresión* `)`
    *Sentencia*

- *Condición*
    - *Expresión*
    - *Declaración*

- *Gestor de excepción*
    - `catch (` *Declaración de excepción* `)` *Sentencia compuesta*


## Expresiones

Las expresiones son entidades sintácticas que pueden evaluarse para obtener su
valor. Dicho de otro modo, son secciones de código fuente que pueden
sustituirse por el valor resultante de su ejecución. Por ejemplo, `2+2` puede
sustituirse por su valor: `4`. Las sentencias, sin embargo, no disponen de un
valor asociado, puesto que su razón de ser es la de indicar a la computadora
qué debe hacer en cada momento.

Durante todo el documento se han usado variedad de expresiones dando por hecho
que eran legibles y comprensibles, esta sección, sin embargo, tratará de
recuperar esos ejemplos y ponerlos en contexto, aportándoles el fundamento
teórico necesario.

En C++ las asignaciones, las llamadas a funciones, la construcción de objetos y
muchas otras operaciones complejas son expresiones. Esto significa que
cualquiera de esos casos debe evaluarse a un valor. Puede que el más
sorprendente sea el de la asignación:

``` cpp
int a;
int b = (a = 10) + 1;  // asigna 10 a `a` y asigna 11 a `b`
```

### Precedencia y asociatividad

En el ejemplo previo se hace uso de los paréntesis (`()`) dando por hecho
demasiados conceptos. En la programación, al igual que en las matemáticas,
existen normas de precedencia que indican en qué orden deben evaluarse las
expresiones. En matemáticas, la multiplicación y la división deben ejecutarse
antes que la suma y se usan los paréntesis para indicar que lo que está entre
ellas debe evaluarse de forma prioritaria.

Por suerte, en C++ ocurre lo mismo.

Por desgracia, la realidad es mucho más compleja. Existen gran variedad de
normas de precedencia. Lo más adecuado trabajar con cuidado, y comprobando el
estándar o la documentación de la plataforma en la que se trabaja y evitar
expresiones especialmente complejas en las que el orden de ejecución sea
crítico.

Es primordial no confundir la precedencia y la [asociatividad][associativity]
de las operaciones con el orden de evaluación. En C++ existen muy pocos casos
en los que el orden de evaluación esté definido por el estándar. La precedencia
y asociatividad están relacionadas con cómo se procesarán las expresiones (es
un concepto de tiempo de compilación), no con qué orden de ejecución tendrán
sus operandos.

[associativity]: https://es.wikipedia.org/wiki/Asociatividad_(%C3%A1lgebra)

### Operadores

Existen variedad de operadores, que pueden agruparse en distintas categorías.
Todos siguen una lógica similar por lo que no se explicarán en detalle.


#### Conversiones implícitas

Cabe destacar que los operadores aplican conversiones de tipos de forma
automática en muchas ocasiones. Por ejemplo, al aplicar una operación
matemática a un valor `float` el otro valor se convertirá automáticamente a
`float` en caso de no ser de un tipo de coma flotante.

Este tipo de conversiones pueden alterar los valores implicados o no hacerlo.
El compilador tratará a toda costa de no perder información pero a veces
aplicará transformaciones que afecten al valor. Se dice que una
conversión no altera el valor si es posible volver a convertir al tipo inicial
y que éste mantenga el valor original. Si esto no ocurre se trata de una
conversión que altera el valor. Este efecto se conoce como *narrowing*.

Un ejemplo de conversión que altera el valor es el siguiente:

``` cpp
unsigned char c = 1023;
```

`1023` es `2` elevado a `10` menos `1`, es decir: `1111111111` en binario. Si
`char` es de un byte, el valor máximo que puede albergar es de 8 bits, es
decir: `2` elevado a `8` menos `1`: `255` (`11111111` en binario). Al hacer
esta igualdad, se aplica *narrowing* al valor `1023` eliminando los bits que
no pueden representarse en esa variable y se almacena `255`.

Estas conversiones ocurren de forma automática con ciertos operadores como el
de asignación, por eso es interesante asignar mediante `{}`, que no aplica
*narrowing*, aunque es imposible hacerlo en todas las ocasiones. Por tanto, es
necesario prestar atención a las posibles conversiones.

El compilador tratará en todo momento de evitar el *narrowing* y de avisar si
está ocurriendo, pero no es infalible.

Existen reglas de conversión que es interesante tener en cuenta pero se han
dejado fuera de éste documento porque son relativamente extensas y no tiene
sentido estudiarlas en detalle en este momento. Existen suficientes referencias
*online*[^online-conversion] para comprobarlas siempre que sea necesario. La
práctica y el sentido común facilitará su asimilación.

[^online-conversion]: Por ejemplo:
  <https://en.cppreference.com/w/c/language/conversion>

#### Operadores aritméticos

| Operador | Operación                  |
|----------|----------------------------|
| `+a`     | Plus unario                |
| `-a`     | Minus unario               |
| `a + b`  | Suma                       |
| `a - b`  | Resta                      |
| `a * b`  | Multiplicación             |
| `a / b`  | División                   |
| `a % b`  | Resto                      |
| `~a`     | Negación por bit           |
| `a & b`  | AND por bit                |
| `a | b`  | OR por bit                 |
| `a ^ b`  | XOR por bit                |
| `a << b` | Rotación izquierda por bit |
| `a >> b` | Rotación derecha por bit   |


#### Operadores de asignación

Aunque previamente se introdujo el concepto de la asignación, el símbolo `=` es
un operador, y la operación de asignación una expresión cuya evaluación
equivale al valor asignado.

| Operador | Operación  |
|----------|------------|
| `a = b`  | Asignación |

Partiendo de este operador, C++ define nuevos operadores que simplifican el
trabajo de resignación de variables, por ejemplo `a += b` es equivalente a `a =
a + b`. Con esta misma lógica los operadores que reciben dos argumentos de
entrada definidos en el apartado anterior pueden convertirse a su versión de
asignación:

| Operador  | Operación                  | Equivalencia |
|---------- |----------------------------|--------------|
| `a += b`  | Suma                       | `a = a + b`  |
| `a -= b`  | Resta                      | `a = a - b`  |
| `a *= b`  | Multiplicación             | `a = a * b`  |
| `a /= b`  | División                   | `a = a / b`  |
| `a %= b`  | Resto                      | `a = a % b`  |
| `a &= b`  | AND por bit                | `a = a & b`  |
| `a |= b`  | OR por bit                 | `a = a | b`  |
| `a ^= b`  | XOR por bit                | `a = a ^ b`  |
| `a <<= b` | Rotación izquierda por bit | `a = a << b` |
| `a >>= b` | Rotación derecha por bit   | `a = a >> b` |

#### Operadores de incremento y decremento

Similares a los operadores de asignación, los operadores de incremento y
decremento asignan un nuevo valor a la variable a la que afectan. Estos
operadores incrementan o decrementan la variable con la unidad (el valor `1`).
La diferencia entre la versión *pre* y *post* radica en el valor de retorno de
la expresión aunque en ambos casos el valor contenido en la variable es el
mismo:

- El caso *pre* primero altera y luego retorna el resultado del incremento o
  decremento.
- El caso *post* retorna el valor previo a la operación.

| Operador  | Operación           |
|-----------|---------------------|
| `++a`     | Pre-incremento      |
| `--a`     | Pre-decremento      |
| `a++`     | Post-incremento     |
| `a--`     | Post-decremento     |


#### Operadores lógicos

Los operadores lógicos evalúan a `bool`, su principal razón de ser es la
combinación de expresiones lógicas.

| Operador  | Operación         |
|-----------|-------------------|
| `!a`      | Negación lógica   |
| `a && b`  | AND lógico        |
| `a || b`  | OR lógico         |

Los operadores lógicos `&&` y `||` evalúan en primer lugar el operando del lado
izquierdo y si éste es suficiente para conocer el resultado final de la
expresión completa (`true` en el caso del `||` y `false` en el caso de `&&`) no
evalúan el segundo. Este concepto se conoce como *short-circuit evaluation*
(evaluación de cortocircuito). Si se sobrecargan estos operadores perderán esta
capacidad[^overload].

[^overload]: Este concepto se visita más adelante.


#### Operadores de comparación

Estos operadores retornan valores booleanos indicando si la condición propuesta
es cierta o falsa.

| Operador  | Operación                                 |
|---------- |-------------------------------------------|
| `a == b`  | Igualdad                                  |
| `a != b`  | Desigualdad                               |
| `a < b`   | Menor que                                 |
| `a > b`   | Mayor que                                 |
| `a <= b`  | Menor o igual que                         |
| `a >= b`  | Mayor o igual que                         |

El operador de comparación a tres sentidos es una excepción, ya que necesita de
una tercera posibilidad para hacer su labor:

- Evalúa a un valor `> 0` cuando `a > b`
- Evalúa a un valor `< 0` cuando `b < a`
- Evalúa a un valor `== 0` cuando `a == b`

| Operador  | Operación                                 |
|---------- |-------------------------------------------|
| `a <=> b` | Comparación a tres sentidos (desde C++20) |


#### Operadores de acceso

Todos estos operadores se describen en el apartado sobre *[Punteros]* y en el
apartado sobre *[Estructuras]* de forma general. Sirva esta lista como
recordatorio de los apartados mencionados así como de recordatorio de que se
trata de operadores y, como operadores que son, se rigen por sus normas.

Los operadores de punteros a miembro se tratan junto con las clases, más
adelante.

| Operador | Operación                      |
|----------|--------------------------------|
| `a[b]`   | *Subscript*                    |
| `*a`     | De-referencia                  |
| `&a`     | Obtención de Referencia        |
| `a->b`   | Acceso a miembro de puntero    |
| `a.b`    | Acceso a miembro de objeto     |
| `a.*b`   | Puntero a miembro de objeto    |
| `a->*b`  | Puntero a miembro de puntero   |


#### Otros operadores

Los siguientes operadores son difíciles de clasificar en otras categorías.

El operador de llamada a función, que se estudiará en detalle más adelante,
sirve para ejecutar funciones u objetos que se comporten como funciones.

El operador coma u operador de secuenciación secuencia varias expresiones y
aporta como resultado de su evaluación el resultado entregado por la última de
ellas, descartando el resto. No debe confundirse con la coma introducida en
*arrays* o llamadas a funciones, ya que esa coma no es un operador. En esos
contextos la expresión de secuenciación debe marcarse entre paréntesis para
diferenciarla del resto.

El operador condicional, conocido popularmente como operador ternario, evalúa
el resultado booleano de la primera expresión y, en función de su resultado,
evalúa la segunda (`true`) o la tercera (`false`).

| Operador   | Operación            |
|------------|----------------------|
| `a(...)`   | Llamada a función    |
| `a, b`     | Secuenciación        |
| `a ? b: c` | Operador condicional |
| `a::b`     | Resolución de scope  |

Además de éstos, existen otros operadores difíciles de clasificar, algunos de
ellos como `sizeof` han sido introducidos en apartados previos. Aunque no se
listarán como se ha hecho con el resto, los más interesantes, como `new` y
`delete`, serán descritos más adelante en secciones propias.

##### Expresiones no-evaluadas

Los operadores `typeid`, `sizeof`, `noexcept`, y `decltype` no evalúan sus
operandos ya que sólo necesitan de información en tiempo de compilación, por lo
que si recibieran una expresión, ésta no se evaluaría.

#### Representaciones alternativas

C++ requiere de símbolos que quedan fuera del estándar ISO-646 (`{`, `}`, `[`,
`]`, `#`, `\`, `^`, `|`, `~`). Para poder ser compatible con el estándar, C++
define símbolos alternativos para ellos y en caso de que se trate de
operadores, aporta nombres alternativos para el operador: por ejemplo, el
operador `||` puede representarse como `or`.

Se anima a quien lo requiera a analizar el estándar de C++ para encontrar las
equivalencias mencionadas, pero no se incluyen por brevedad y porque no suelen
ser necesarias en la práctica.

### Expresiones constantes `constexpr`

Una expresión constante es una expresión que el propio compilador puede
evaluar. Como es lógico, no puede usar valores que no se conozcan en tiempo de
compilación y no puede tener efectos secundarios (como escritura en disco,
acceso aleatorio, etc.).

Las expresiones constantes pueden usar tipos enteros, de coma flotante y
enumeraciones, pueden aplicar cualquier operador que no altere el estado (por
ejemplo, `=` y `--` alteran el estado) y pueden combinarse con literales.

El especificador `constexpr` puede aplicarse en las declaraciones para marcar
ese valor o función como expresión constante siempre que cumpla las reglas. De
esta forma, el compilador será quien los evalúe en los lugares donde aparezcan.

Se puede considerar que las expresiones constantes son un tipo de mini-lenguaje
para el compilador, que permite ejecutar código en momento de compilación.

#### Nombres simbólicos

El objetivo del uso de constantes en el código no es más que centralizar el uso
de literales para evitar repetirlos por el programa y tener que editarlos en
todas las posiciones donde aparezcan más adelante. Podría decirse, además, que
asignar un nombre a las constantes ayuda a identificarlas por su significado
convirtiéndose en nombres simbólicos en lugar de ser simples valores. Al fin y
al cabo, el objetivo es hacer código mantenible.

`constexpr` permite crear funciones que sean capaces de mezclar constantes de
modo que no tenga que hacerse manualmente. Esta capacidad es extremadamente
útil para crear código mantenible y comprensible sin complicarse.

#### `const` vs `constexpr`

Las valores `const` no son del todo compatibles con `constexpr` ya que expresan
conceptos distintos. El prefijo `const` sirve para indicar que una declaración
es inmutable en el *scope* actual, pero puede inicializarse mediante una
variable no-constante y ser, por tanto, desconocida en tiempo de compilación.
Las variables declaradas como `constexpr`, por contra, deben ser conocidas en
tiempo de compilación por lo que `constexpr` es más restrictivo.

Sin embargo, es posible crear una expresión constante mediante `constexpr`
usando variables declaradas como `const` siempre que éstas sean conocidas en
tiempo de compilación.

#### Tipos literales

Las expresiones constantes pueden afectar a tipos definidos manualmente siempre
que éstos sean lo suficientemente simples. Las clases con un constructor
`constexpr` se consideran *tipos literales*. Para que un constructor pueda ser
`constexpr` debe cumplir las normas de las expresiones constantes (no tener
efectos secundarios, etc.), que los constructores no suelen
cumplir[^constructores] ya que suelen inicializar miembros de la clase.

En resumen, pueden crearse tipos adicionales que se comporten como tipos
literales, siempre que su construcción parta de literales o `constexpr`.

[^constructores]: Constructores y clases se describen más adelante. Por el
  momento, se puede considerar que una clase es como una estructura (`struct`)
  pero algo más compleja y un constructor es la función asociada a ésta que se
  ejecuta al crear una nueva variable del tipo descrito por la clase.

#### Referencias en expresiones constantes

Las expresiones constantes pueden trabajar con referencias siempre que éstas
sean a una constante. Sin embargo, la aritmética de punteros no es posible ya
que las direcciones de memoria sobre las que opera no se conocen en tiempo de
compilación ya que se construyen en tiempo de enlazado.

## Funciones

Las funciones son agrupaciones de instrucciones con un nombre que las
identifica. De este modo, es posible llamar a las funciones por su nombre
durante el programa y éstas ejecutarán las instrucciones con las que se
definieron. Para poder aplicar las funciones en diferentes casos, éstas pueden
parametrizarse mediante argumentos de entrada. Las funciones retornan además un
valor (que puede ser `void`) de modo que la ejecución de una función
(recordando que una llamada a una función es una expresión) evalúa a su valor
retornado.

Las funciones son un tipo más así que deben declararse antes de poder usarse,
tal y como se describe en el apartado sobre *[Declaraciones]*. Eso sí, las
declaraciones de funciones son algo más complejas que una declaración de una
variable debido a su propia naturaleza.

### Declaración de funciones

La declaración de las funciones consta de varios apartados que, de forma
general y obviando algunos detalles pueden agruparse del siguiente modo:

- **Tipo de retorno**: Tipo del valor que retornará la función (puede ser
  `auto`).  Puede añadirse como prefijo (más común) o sufijo (útil en casos
  concretos pero requiere usar `auto` como prefijo) de la declaración.
  Requerido.
- **Nombre** de la función. Requerido.
- **Lista de argumentos** de la función entre paréntesis (`()`) y separados por
  coma (`,`). Requerido aunque puede estar vacío (`()`).
- **Atributos**. Ver *[Sentencias]*. Opcional.
- **Lista de especificadores** separados por espacio. Definen comportamientos
  especiales como `inline` o `constexpr`. Ver *[Especificadores]*. Opcional.

Visto en detalle, sin embargo, y recuperando la sección [Sintaxis de las
declaraciones], las funciones declaran su tipo de retorno como si de su tipo
base se tratara y añaden, si fuera necesario, especificadores a modo de prefijo
como `static` o `virtual`. Posteriormente se añade el declarador de la función
(su nombre), que en este caso sí se requiere, y el operador necesario para el
caso de la función (`()`) que incluye su lista de argumentos en el interior. Al
declarador pueden se le pueden asignar, como ya se ha hecho anteriormente,
diferentes calificadores. En las funciones, los más comunes son `const` y
`volatile`. Opcionalmente puede añadirse el cuerpo de la función y un conjunto
de especificadores a modo de sufijo, como `noexcept`.

#### Especificadores

En este apartado se listan los especificadores más comunes en el uso de
funciones. Sirva como referencia futura más que como un apartado que deba
estudiarse en detalle en una primera lectura.

A modo de prefijo, los especificadores más usados son los siguientes:

- `inline`: declara si se desea que el compilador sustituya las llamadas a la
  función por el cuerpo de ésta. Opcional.
- `constexpr` (desde C++11): declara que es posible evaluar el valor de la
  variable o función en tiempo de compilación como parte de una expresión
  constante. Ver *[Expresiones constantes `constexpr`]*.
- `consteval` (desde C++20): declara la función o plantilla de función como
  *inmediata* (*immediate function*), lo que significa que la llamada a la
  función debe resolverse a una constante en tiempo de compilación. Implica
  `inline`. Una función inmediata es una función `constexpr` así que los mismos
  requerimientos aplican.
- `virtual`: indica que la función puede sobreescribirse por una clase derivada.
- `static`: indica que la función no está asociada a un objeto en particular.
- `friend`: aporta acceso a una función a los campos protegidos o privados de
  la clase en la que se añade.

A modo de sufijo, por otro lado son los siguientes:

- `noexcept` (desde C++11): indica si la función puede o no puede lanzar
  excepciones.
- `final` (desde C++11): indica que la función no puede sobreescribirse por una
  clase derivada.
- `override` (desde C++11): indica que la función virtual a la que se le aplica
  debe sobreescribir una función de la clase base.

Muchos de ellos tienen relación con funciones que actúan como miembro de una
clase, concepto que se conocerá más adelante.

#### Atributos

El atributo más común y útil a la hora de declarar funciones es el siguiente:

- `[[noreturn]]`: indica que la función no retornará mediante el procedimiento
  habitual (sentencia `return`): la función no termina, termina sólo con
  sentencia `exit`, sólo termina mediante excepciones...

### Definición

De forma similar a lo que ocurre con los tipos básicos, una definición de una
función es una declaración que incluye el cuerpo de la función. Las mismas
normas de las definiciones aplican, por lo que las funciones deben definirse
una única vez y deben estar definidas en algún lugar para poder utilizarse.

Las definiciones de funciones y todas sus declaraciones deben tener los mismos
tipos de argumentos de entrada y retorno. Los nombres de éstos, sin embargo, no
forman parte de la declaración de la función así que pueden cambiarse de una
declaración a otra.

Los nombres de los argumentos de entrada en la declaración de la función son
opcionales (no así en su definición), la única razón para añadirlos es
simplificar la documentación añadiendo nombres que insinúen el uso que se le
dará a éstos argumentos.

``` cpp
// Dos declaraciones de la misma función. Son equivalentes
int func(int a, int b); // Con nombres de argumentos
int func(int, int);     // Sin nombres de argumentos

// Definición de la función
int func(int a, int b){
    // Los nombres son necesarios en la definición, para poder usarlos
    return a*b;
}
```

### Valores de retorno

Las funciones deben declarar qué tipos de dato retornan y retornarlos (existen
excepciones: los constructores, las funciones de conversión de tipo y la
función `main`, que es una función especial). Esta declaración puede hacerse en
modo prefijo, como se ha realizado históricamente o a modo de sufijo mediante
el operador `->` y usando `auto` como tipo de dato retornado en el prefijo.

``` cpp
int suma(int a, int b);             // Modo prefijo
auto suma(int a, int b) -> int;     // Modo sufijo
```

Aunque el modo sufijo pueda usarse en cualquier función, se creó
específicamente para los casos en los que el tipo de retorno depende de los
argumentos de entrada de ésta. Sólo tiene sentido en el uso de plantillas.

Si una función no retorna ningún valor, se usa el concepto del vacío, ya
descrito anteriormente, mediante el nombre `void`.

La sentencia `return;` es la encargada de terminar la ejecución de la función
usando su valor como valor de retorno. Por ejemplo:

``` cpp
int pow(int base, unsigned int exp){
    int result = 1;
    for (int i = 0; i != exp; i++){
        result *= base;
    }
    return result;
}

pow(3,3) == 27; // true
```

Cada función puede incluir cualquier cantidad de sentencias `return;`. La
sentencia `return;` inicializará una variable del tipo de retorno indicado en
la función y la llenará con el valor retornado, este proceso es idéntico a
cualquier otra inicialización, por lo que las conversiones necesarias
aplicarán, tal y como ocurre cuando se inicializa una variable manualmente.

Cada vez que se llama una función, se construye una copia de sus argumentos de
entrada y de sus variables locales automáticas, cuando la función termina, su
memoria se libera por lo que las referencias a variables locales no estáticas
no deben retornarse.

La sentencia `return;` es una forma de terminar la ejecución de una función,
pero no es la única. A continuación se listan todas, con sus diferentes
posibles valores de retorno.

1. Sentencia `return;`: la función retorna el valor indicado.
2. La ejecución llega al final del cuerpo de la función: la función retorna
   como una llamada a `return;` sin un valor de retorno. Sólo es válido si la
   función se define como `void` o en la función `main`.
3. Lanzar una excepción: la función termina por un mecanismo distinto, las
   reglas de retorno no aplican.
4. Invocar una llamada a sistema que nunca retorne.


#### Funciones recursivas

En el cuerpo de las funciones se conoce el identificador de éstas por lo que es
posible llamar a una función dentro de sí misma. Este mecanismo es útil pero
peligroso. Como ya se ha indicado, las funciones crean copias de su entorno al
lanzarse. Si no existe una vía de escape (como un `return` en el caso de que
una condición se cumpla) al bucle de llamadas recursivas, las copias del
entorno llenan la memoria resultando en un fallo de ejecución del programa.

#### La función `main`

La función `main` es especial, ya que su valor de retorno siempre debe marcarse
como `int` pero no suele incluir ninguna sentencia `return` en su interior.

Esto se debe a que ese `int` se asocia al valor de retorno del programa.
Cualquier valor diferente de `0` se considera un fallo. Si no se retorna nada,
el sistema considerará que no ha habido ningún error y que la ejecución del
programa ha sido exitosa. La interpretación que el sistema operativo haga sobre
el código de error puede variar en función de la plataforma. Los sistemas
basados en UNIX suelen usar el valor, mientras que otros suelen ignorarlo.

En sistemas tipo UNIX es posible extraer el valor retornado por el programa de
la variable `$?` en la shell de sistema.

``` bash
$ programa
$ echo $?
0
$
```

### Variables locales

Las variables declaradas en el cuerpo de las funciones se conocen como
variables locales. Éstas se construyen cuando el hilo de ejecución llega a su
definición, a no ser que hayan sido declaradas como `static`. Las variables
`static` se comparten de una ejecución de la función a la siguiente, y se
construyen en la primera ocasión que se accede a su definición, aunque no se
eliminan al terminal la ejecución de la función.

Las variables locales estáticas, por tanto, permiten a las diferentes llamadas
a una función compartir información sin necesidad de asignar variables
globales, manteniendo el *scope* global limpio.

Las reglas del *scope* descritas previamente afectan a las funciones, así que
deben recordarse. En particular, las etiquetas están disponibles en el cuerpo
de la función en la que se declararon, aunque no son una herramienta que deba
usarse demasiado.

También cabe recordar que el cuerpo de las funciones suele declararse
mediante una sentencia compuesta (entre llaves `{}`), por lo que el *scope* de
bloque aplica.

### Argumentos de entrada

La lista de argumentos de entrada de la definición de las funciones declara los
argumentos con los que la función trabajará. Estos argumentos, que se declaran
como cualquier otra variable, también se asignan del mismo modo que si de una
inicialización de variables se tratara, aplicando las conversiones necesarias.

A no ser que la variable enviada sea una referencia, las funciones reciben
copias de los argumentos, por lo que cualquier alteración que se haga en ellas
no altera la variable original.

#### Referencias y punteros como argumento

Las referencias son, como ya se conoce, una forma transparente de compartir
acceso a un valor desde diferentes variables del programa, por lo que enviar
referencias como argumento de entrada sí que permite alterar valores en el
exterior de la función.

``` cpp
void increment(int &val){
    ++val;
}
int a = 10;
increment(a);  // No devuelve nada pero ahora `a` == 11
```

Este mecanismo es poderoso, pero peligroso. Las funciones definen valores de
retorno con el fin de que éstos se usen para indicar el resultado de su
ejecución, en cambio, si se alteran los argumentos de entrada, como en el
ejemplo, es difícil comprender su funcionamiento a simple vista.

Las referencias como argumento son interesantes desde otro punto de vista: son
mucho más eficientes que realizar una copia de los argumentos, sobre todo en
variables complejas. Pero, en caso de que se usen con este fin, declarar las
referencias como `const` ayuda a que no se cometa el error de alterar el valor
por accidente y a representar a quien lea el programa que la función en
cuestión no alterará el valor.

El caso de los punteros es similar porque a través de ellos también es posible
alterar el valor asociado. Sin embargo los punteros tienen dos diferencias con
las referencias: la sintaxis y la existencia de `nullptr` para describir
punteros *a ninguna parte*. Se recomienda que, siempre que se quiera describir
la alternativa de no enviar ningún objeto a la función se usen punteros por
ésta segunda capacidad que no es posible realizar con las referencias.

#### Arrays como argumento

En caso de enviar un *array* a una función, se enviará un puntero a éste, en
lugar de una copia del *array*. No es sorprendente, ya que el nombre del
*array* es un puntero al mismo.

El tamaño de los *array* debe enviarse a las funciones junto con los datos, ya
que no es posible deducirlo en la mayoría de los casos[^array-size-deduction].

Es posible recibir los datos como referencia a un *array*, pero en ese caso
debe indicarse la longitud esperada, lo que reduce drásticamente la
flexibilidad.

[^array-size-deduction]: En los strings estilo C el último caracter es el
  caracter nulo (`\0`) por lo que sí es posible, pero en general no lo será.

#### Listas como argumento

Las listas (`{}`) han aparecido a través del documento de modo fugaz pero
intenso, principalmente en la sección sobre *[Inicialización]* de variables. Las
listas pueden enviarse a una función que recoja cualquier tipo que pueda ser
inicializado con ellas. Lógico, puesto que como ya se ha mencionado, la
asignación de los argumentos de entrada ocurre como una inicialización por
copia.

#### Número no especificado de argumentos

En ocasiones es posible declarar funciones que reciban un número no
especificado de argumentos de entrada.

Existen tres mecanismos principales para conseguir que una función en C++
reciba un número indeterminado de argumentos de entrada:

1. Enviar los argumentos como una lista, tal y como se describe en el apartado
   previo.
2. Usando plantillas *variadic* (*variadic templates*).
3. Usando la elipsis `...` y las macros definidas en `<cstdarg>`. Este último
   método es el único de los que puede usarse en C, aunque es el menos seguro
   de todos al no hacer una comprobación de tipos tan fuerte como el resto.

Los tres puntos requieren de conceptos que aún no se han explicado, como las
plantillas y las macros, pero se considera importante mencionarlos aquí para
facilitar la consulta.

#### Argumentos por defecto

Es posible añadir valores por defecto a los argumentos de entrada, de modo que
éstos pueden ignorarse en la llamada a la función. Para definirlos, basta con
añadir qué valor tomarán por defecto en la declaración de la función, pero no
deben incluirse en la definición de la misma.

Como los argumentos de entrada tienen una posición asociada, sólo pueden
definirse como argumentos por defecto aquellos situados a la derecha:

``` cpp
float log(float x, float base =10);   // Correcto
float log(float base = 10, float x);  // Incorrecto
```

Si una función dispone de más de un argumento por defecto, deben ordenarse por
prioridad ya que no existe modo alguno de llenar argumentos situados más a la
derecha dejando huecos a la izquierda. En el siguiente ejemplo, no es posible
llamar a la función con `a` y/o `b` como usando los argumentos por defecto si
se desea enviar `c`.

``` cpp
void take_args(int a = 0, int b = 0, int c = 0);

take_args(1,2,3) // Llamada con a = 1, b = 2 y c = 3
take_args(1,2)   // Llamada con a = 1, b = 2 y c = 0
take_args(1)     // Llamada con a = 1, b = 0 y c = 0
```

Las declaraciones de funciones con argumentos por defecto deben ser coherentes,
de lo contrario se producirán errores de compilación.

#### Sobrecarga de funciones

Para C++ los argumentos de entrada de las funciones (pero no sus valores de
retorno) son parte de su identificación, por lo que pueden existir dos
funciones con el mismo nombre si no reciben el mismo tipo o número de
argumentos de entrada. A esto se le conoce como sobrecarga de funciones
(*function overloading*).

Un ejemplo sencillo de esto es el operador `+`, que puede usarse tanto para
sumar números de coma flotante como enteros. Aunque el operador no es una
función, pronto trataremos que también es posible sobrecargar operadores y que,
en realidad, se comportan como funciones.

Aunque pueda parecer innecesaria, la sobrecarga de funciones es muy interesante
porque permite a quien programa expresarse de forma clara, delegando el uso de
los tipos a los argumentos de entrada y no al nombre de la función. Como
ejemplo de lo contrario, C no dispone de sobrecarga de funciones, por lo que
las librerías como `<math.h>` (accesible desde C++ en `<cmath>`) deben realizar
trucos como el que sigue:

``` c
double floor(double x);
float floorf(float x);
long double floorl(long double x);
```

Este mismo ejemplo en C++ podría resolverse con sobrecarga de funciones
pudiendo usar el mismo nombre en todas ellas:

``` cpp
double floor(double x);
float floor(float x);
long double floor(long double x);
```

Para resolver las sobrecargas de funciones C++ debe comprobar el tipo
introducido en la llamada y encontrar la función que le corresponda. Esta tarea
puede ser más compleja de lo que a simple vista puede parecer en el caso, por
ejemplo de que se llame a `floor(1)` habiendo únicamente declarado las
funciones `floor` del ejemplo previo.

Para este tipo de casos, C++ sigue una serie de pasos que aseguran encontrar la
función correcta o que, al menos, sirvan como guía para ayudar a quien programa
a definir las funciones que va a necesitar.

1. En primer lugar trata de encontrar la función que use el tipo exacto con el
   que se la llama.
2. Si no lo encuentra, trata de *promocionar* el tipo, convirtiéndolo a un tipo
   sin pérdidas: `bool` a `int`, `char` a `int`, etc.
3. Si no es posible, trata de realizar conversiones estándar: `int` a `double`,
   `int` a `unsigned int`, etc.
4. Si tampoco es posible trata de resolver conversiones definidas por el
   usuario.
5. Si no es posible, trata de resolver mediante la elipsis `...`.
6. Si ninguna de ellas es posible la ejecución falla.

En casos en los que la resolución sea compleja, siempre puede resolverse de
forma manual, convirtiendo los argumentos de entrada al tipo que interese.

### Llamada a funciones

Para llamar a una función se aplica el operador `()` con los argumentos de
entrada que se deseen enviar. Esto forma una expresión que se evaluará al valor
de retorno de la función.

Sin embargo, las funciones no son el único tipo de dato al que se le puede
aplicar el operador de llamada:

- *Los constructores*, aunque técnicamente no son funciones, pueden llamarse
  como si de una función se tratara. Sin embargo no retornan valores y su
  dirección no puede obtenerse.
- *Los destructores* también pueden llamarse como funciones pero no pueden
  sobrecargarse, y no es posible obtener su dirección.
- *Los objetos-función* no son funciones, técnicamente son objetos, y no pueden
  sobrecargarse pero pueden llamarse. Su operador `()`, sin embargo, sí es una
  función.
- *Las expresiones lambda*, son funciones con una declaración más escueta,
  diseñadas para utilizarse como literales, y también pueden llamarse.

### Expresiones Lambda

Las expresiones lambda, introducidas en C++11, también conocidas como funciones
lambda o simplemente lambdas, son una forma reducida de crear un objeto función
anónimo.

En el apartado sobre clases se comprenderá en detalle de qué se trata cuando se
habla de un objeto en estos términos, pero tampoco es estrictamente necesario
para comprender las funciones lambda.

La forma más sencilla de entender las funciones lambda es tratarlas como
cualquier otro literal, que resulta comportarse como una función. Las
funciones, tal y como se han descrito requieren de un nombre al construirse, lo
que significa que las funciones no tienen en realidad una forma de describirse
como un literal. Sólo pueden asociarse a un nombre y usarse a través de éste.

Las expresiones lambda sin embargo son una expresión que resuelve a una función
(un objeto-función, técnicamente), de modo que pueden llamarse sin usar ningún
nombre, simplemente aplicando el operador de llamada `()` al resultado de la
expresión.

Las funciones lambda son *closures*, funciones capaces de capturar variables
del *scope* en el que se declaran[^closures]. Esta característica no está
disponible en las funciones tradicionales, por lo que no se puede decir que
sean exactamente lo mismo.

[^closures]: Las *closures*, en realidad, son una técnica para implementar el
  *scope semántico* en lenguajes donde las funciones se tratan como ciudadanos
  de primera clase. Esto explica las diferencias entre las funciones clásicas y
  las *closures* en C++, pero queda fuera del objetivo de este texto definir
  estos conceptos en detalle. Con las palabras clave de esta nota al pie es
  posible hallar información detallada al respecto.

#### Sintaxis

Una expresión lambda consta de un conjunto de apartados:

- Una lista de captura (*capture list*) obligatoria que puede estar vacía,
  delimitada por corchetes `[]`. Esta lista de captura define qué variables del
  *scope* actual estarán disponibles dentro de la lambda, es decir, qué
  variables pertenecerán a la *closure*.
- Una lista opcional de parámetros de plantilla (desde C++20) delimitados con
  los símbolos de mayor que y menor que `<>`.
- Una lista opcional de parámetros delimitada por paréntesis `()`. No incluirla
  es equivalente a incluir una lista vacía.
- Algún especificador opcional como `mutable`, `constexpr` (desde C++17) o
  `consteval` (desde C++20).
- Especificadores de excepción como `noexcept`.
- La declaración opcional del tipo de retorno. Si no se indica se toma como en
  el caso de una función declarada con `auto`. Se representa del siguiente
  modo: `->` *tipo*. Sólo puede obviarse en caso de que la función tenga una
  única sentencia de retorno, lo que permite la deducción de tipo de salida. Si
  se añade es obligatorio añadir la lista de argumentos, aunque esté vacía.
- Un cuerpo delimitado por llaves `{}`.

Por ejemplo

``` cpp
[](int a, int b){return a+b;}
```

Nótese que la lista de captura es obligatoria. El primer caracter de una
función lambda siempre es `[`, es lo que le permite al compilador saber que se
trata de una expresión lambda. La lista de argumentos de entrada es opcional.

#### Captura de variables

Normalmente las funciones actúan sobre argumentos de entrada. El caso de las
funciones lambda es un poco distinto.

Las funciones lambda pueden capturar variables del *scope* en el que se
definieron y, en el momento de su ejecución (o sus ejecuciones), usarlas o
alterarlas independientemente de los parámetros de entrada definidos.

La lista de captura puede tomar varias formas:

- `[]`: No captura ninguna variable.
- `[&]`: Captura implícita de todas las variables como referencia.
- `[=]`: Captura implícita de todas las variables como valor.
- `[`*lista de captura*`]`: Captura explícita de variables por nombre. Las
  variables marcadas con el prefijo `&` se capturan por referencia, el resto
  por valor. La lista de captura puede contener `this` o `...`.
- `[&,` *lista de captura por valor* `]`: Captura implícitamente todas las
  variables como referencia y como valor las indicadas en la lista de captura.
  La lista de captura no puede incluir capturas por referencia (no indicar las
  variables en la lista ya las capturaría como referencia).
- `[=,` *lista de captura por referencia* `]`: Captura por valor de forma
  implícita las variables no listadas. Las variables listadas se capturan por
  referencia.

``` cpp
int a = 1;
[]{a++;};      // Error: la función lambda no captura la variable
[=]{a++;};     // Error: la función lambda intenta alterar una variable
               //   recibida como valor

[&]{a++;};     // Correcto: la función lambda incrementará la variable `a` cada
               //   vez que se ejecute
```

#### `mutable`

Las lambdas mutables son capaces de alterar el valor de su estado. Esto
significa que pueden cambiar el valor de variables capturadas por valor, pero
sin afectar al contexto del cual las capturaron:

``` cpp
int a = 1;
auto f = [=] mutable {return ++a;};

f(); // Retorna 2
f(); // Retorna 3

// Pero `a` sigue siendo 1.
```

Esto no es equivalente a capturar mediante referencia por dos motivos:

- Las capturas por referencia afectan al *scope* original.
- Las capturas por referencia se ven afectadas por el *scope* original.

``` cpp
int a = 1;
auto f = [&] {return ++a;};

f(); // Retorna 2
f(); // Retorna 3

// Pero `a` es 3 ahora

a = 1;
f(); // Retorna 2 de nuevo
```

#### Vida de las funciones lambda

Las funciones lambda, como pueden circular por el programa como cualquier otra
variable, es posible que vivan durante más tiempo que el lugar en el que se
crearon (ya sea una función o una clase). En ese caso, si las variables se
capturan por referencia, al usarse en la función lambda podrían no existir, por
haberse limpiado el *scope* al que correspondían.

Si se quiere crear una función lambda que potencialmente sobreviva a su
contexto, es necesario capturar las variables por valor, para que se copien
dentro de ésta y pueda así acceder a ellas a pesar de que las variables
originales se hayan eliminado.

#### Tipo asociado

Si se desea asignar la lambda a una variable o poder enviarla a una función
como argumento, es necesario conocer el tipo de ésta para poder hacer las
declaraciones pertinentes.

Con el fin de que cada implementación pueda realizar las optimizaciones que
necesite, las lambdas no tienen un tipo definido, aunque sí que se especifica
que las lambdas deben ser algún tipo de función conocido como *closure type*.
Este tipo es único para cada lambda (así que dos lambdas nunca compartirán
tipo), y se considera que ha sido creado en el *scope* más reducido en el que
la lambda se encuentre. El *closure type* es un tipo abstracto en el sentido
no-técnico de la palabra, es una designación conceptual para el tipo que las
diferentes implementaciones aplicarán.

Este *closure type* debe ser una clase local con un constructor (el que se
encarga de hacer las capturas de variables) y una función miembro `const` que
define el funcionamiento del operador de llamada: `operator()`. Estos conceptos
describen el comportamiento de las funciones lambda, pero no cómo asignarlas a
variables o enviarlas a funciones a modo de argumento, ya que ese tipo *closure
type* realmente no puede usarse en los programas al ser únicamente un concepto.

Para poder asignar una función lambda a una variable puede usarse `auto`, pero
eso no resolvería el caso de declarar una función que reciba una lambda como
argumento, ya que las funciones no pueden deducir sus argumentos de entrada.
C++ define el tipo `std::function` para esta labor.

El tipo `std::function` es capaz de almacenar, copiar o invocar cualquier tipo
de objeto llamable (*callable*), es decir, que implemente el operador `()`,
incluyendo, por supuesto, las funciones lambda, ya que son un tipo de objeto
llamable.

El tipo `std::function` debe usarse incluyendo el tipo de argumentos de retorno
y de entrada en su declaración a modo de argumento de plantilla (*template*),
concepto que se discute más adelante, con la forma `std::function<R(LE)>`
siendo `R` el tipo de retorno y `LE` la lista de argumentos de entrada.

En el siguiente ejemplo es más lógico usar simplemente `auto`, pero sirva de
referencia de cómo usar el tipo `std::function`.

``` cpp
#include<functional> // Debe incluirse `functional` para tener acceso a
                     // `std::function`

int main (){
    int a = 1;
    std::function<int(int)> f = [&a](int b){return a += b;};

    // `f` puede llamarse
    f(10);      // retorna 11 y actualiza el valor de `a`
}
```

Además, en el caso de que las funciones lambda no hagan capturas pueden
asignarse a punteros de funciones, ya que se comportan como una función
cualquiera.

### Punteros a funciones

Al igual que con los datos, las funciones también se deben construir en la
memoria de algún modo y tener una dirección asociada[^function-pointers] por lo
que es teóricamente posible extraer esa dirección y asignarla a un puntero.
Normalmente, la memoria correspondiente a secciones del programa, como son las
funciones, no puede alterarse porque el sistema operativo, si lo hay, no suele
permitirlo, pero esto no es inconveniente si lo que se desea es únicamente
llamar a las funciones y compartir su dirección con otras secciones del
programa, ya que ninguna de las dos cosas altera su cuerpo.

C++ implementa este comportamiento haciendo posible que se obtenga la
referencia de una función igual que con cualquier otro identificador, usando el
operador `&` para obtener la referencia y el operador `*` para de-referenciar.

En realidad, como en C++ únicamente es posible llamar o extraer la referencia
de una función, los operadores `&` y `*` son opcionales. El compilador es
consciente de cuando un puntero a una función debe de-referenciarse para
realizar la llamada y cuándo se está tratando de obtener la referencia de una
función.

``` cpp
void funcion(int a){/*...*/}    // Define la función

void (*p)(int);  // Declara un puntero a función

p = funcion;     // Asigna la referencia al puntero.
                 // Equivalente a: `p = &funcion;`

p()              // Llama a la función `funcion`.
                 // Equivalente a: `(*p)()`
```

El ejemplo muestra además que los punteros a funciones deben declarar los tipos
de los argumentos y los tipos de retorno de la función cuya referencia van a
gestionar. Si los tipos de argumentos y de retorno no coinciden con la función
su referencia no podrá asignarse a no ser que se realice una conversión
explícita, que deberá corregirse a la hora de llamar a la función a través del
puntero para evitar comportamientos indeterminados.

En el lenguaje C no existen las clases y, por tanto, no existen ni las
funciones lambda ni los objetos-función por lo que ésta es la única forma de
enviar funciones a otras. Este estilo de programación es habitual en código C++
anterior a la existencia de las funciones lambda (que aparecieron en C++11) y
en código influenciado por C, así que, aunque pueda no parecer útil a simple
vista por la existencia de mecanismos más elegantes, no está de más conocerlo.

[^function-pointers]: Sólo que en ocasiones no se almacenan en la misma memoria
  que los datos (p.ej. arquitectura Harvard vs. arquitectura von Neumann).


## Excepciones

Las excepciones son un mecanismo de gestión de casos que requieren un
tratamiento especial, es decir, de casos excepcionales. Aunque pueden
describirse muchos tipos de excepciones a nivel teórico, la gestión de
excepciones suele estar centrada en los errores, ya que en un entorno
supuestamente controlado como es un programa, los casos que escapan al control
del programa, los casos excepcionales, se suelen considerar un error de
ejecución.

La gestión de excepciones nace con esa idea, la de gestionar casos en los que
el funcionamiento esperado no se ha producido, pero es cierto que este
mecanismo se utiliza en ocasiones con otros fines.

En lenguajes como C, que no dispone de mecanismo de gestión de excepciones, los
fallos de ejecución deben considerarse mediante otras vías, como los valores de
retorno de las funciones. Por ejemplo, una función que realice una tarea como
abrir un archivo como es `fopen`, que retorna un descriptor de fichero, un
entero positivo, gestiona los casos de error retornando `NULL` en los casos que
el archivo no haya podido abrirse, sea por la razón que sea (fallos en disco
duro, falta de permisos, etc). En este caso concreto la función `fopen` también
necesita hacer uso de `errno` para indicar el tipo del error que ha ocurrido,
ya que un `NULL` sólo indica que ocurre el error, sin poder aclarar su
naturaleza.

Esta forma de trabajar tiene implicaciones directas en el control de flujo del
programa, provocando la inclusión de quizás demasiadas sentencias condicionales
para controlar todos los posibles casos de error que el programa pueda tener.

C++ aporta un método cuya finalidad es simplificar la gestión de errores,
reduciendo la cantidad de sentencias condicionales para tratarlos.

No se dedicaría ni un segundo a explicar el método antiguo de gestión de
errores si no fuera a aparecer en cantidad de ocasiones debido a la influencia
de C en C++ y a las implicaciones rendimiento que el estilo C++ implica. Si C
no soporta este mecanismo y C++ sí, es lógico entender que en cualquier ocasión
que se requiera compatibilidad con C, que son más comunes de lo que puede
parecer, será imposible usarlo. En entornos con con requerimientos de
rendimiento muy estrictos, como el desarrollo de videojuegos, en ocasiones se
sustituye la gestión de excepciones por soluciones ad-hoc, que quedan fuera de
los objetivos de este texto.

### Motivación

La gestión de excepciones que C++ presenta pretende llevar información del
error ocurrido al lugar donde éste se trata. Esto es importante en un contexto
en el que se trate con librerías, piezas de software genéricas usadas en un
entorno concreto. Las librerías son capaces de detectar fallos en tiempo de
ejecución, pero en muchas ocasiones, al no tener conocimiento del contexto
concreto de la aplicación no son capaces de dar una respuesta concreta a éstos.

C++ construye una metodología capaz de *lanzar* (*throw*) esos errores a una
capa superior del programa que sí que sepa cómo gestionarlos arrastrando
con ellos información de dónde y cómo ocurrieron, acercando así el lugar donde
ocurre el error al lugar en el que éste se trata.

### Funcionamiento

Cuando ocurre un error en el programa, éste llega a un estado inesperado, por
lo que su ejecución, en caso de continuar, se ve comprometida por el estado en
el que el programa se encuentra.

Si una excepción ocurre en mitad de una función se puede considerar temerario
continuar con la ejecución de la función hasta que ésta termine para poder
entregar la excepción a la pieza del programa que la llamó.

La sentencia `throw` corta la ejecución del programa entregando a modo de
excepción el valor que se le entregue a la sentencia `throw`. Si la excepción
se encuentra en una función que no la capture, la ejecución de la función
termina, sin entregar un valor de retorno, como si la llamada a la función se
sustituyera por la emisión de la excepción.

La excepción se propagará en la pila de llamadas hasta encontrarse con un
bloque `try-catch` interesado en capturarla. Si la excepción ha llegado al
programa principal y éste no la captura, se ejecutará `std::terminate()` y la
ejecución del programa se verá interrumpida.

Cuando la excepción se encuentra dentro de un bloque `try`, éste termina su
ejecución como ocurriría en una función, sin ejecutar las sentencias
siguientes, y salta automáticamente al bloque `catch` interesado en la
excepción.

Cada bloque `catch` puede indicar el tipo de excepción en el que se interesa.
De modo similar a la sobrecarga de funciones, la excepción será capturada por
el bloque `catch` cuyo tipo declarado sea el mismo que el de la excepción. El
cuerpo del bloque se ejecutará y se continuará la ejecución del programa
después del bloque `try-catch`.

### Gestión de excepciones

La sentencia try («intentar» en inglés) tiene el siguiente aspecto:

- `try` *Sentencia compuesta* *Lista de gestores de excepción*

Siendo la lista de gestores de excepción un conjunto de bloques `catch`
(«capturar» en inglés) con el siguiente aspecto:

- `catch (` *Declaración de excepción* `)` *Sentencia compuesta*

Cabe destacar que la sentencia `try` como sentencia que es puede usarse en
cualquier lugar donde pueda usarse una sentencia como, por ejemplo, en el
cuerpo de una función. Además, el cuerpo de la sentencia `try` así como el de
los bloques `catch` ha de ser obligatoriamente una sentencia compuesta (`{`
*[lista de sentencias]* `}`), que puede estar vacía, y, tal y como las reglas
del *scope* especifican, esto resulta en que ambos tienen crean un nuevo
contexto para las variables.


#### Captura de excepciones

El aspecto de la declaración de excepción para el bloque `catch` es el mismo
que un declarador de argumento en una función. Además es posible que el
declarador sea `...` de modo que el bloque capturaría todos los tipos de
excepciones posibles.

Como es posible añadir cualquier número de bloques `catch` con declaradores más
genéricos o más concretos, es posible que una excepción pueda gestionarse por
más de un bloque `catch`. En ese caso, los `catch` más cercanos al bloque `try`
tienen preferencia, por esta razón es lógico añadir los bloques `catch` más
genéricos al final. Una vez la excepción ha sido capturada, sólo se tratará por
el bloque `catch` actual, ya que el objeto-excepción se elimina al salir de
éste. Para que más de un bloque `catch` pueda gestionar la excepción, ésta debe
ser re-lanzada.

Al igual que para encontrar la función sobrecargada adecuada, existe un
algoritmo sencillo para encontrar la captura de excepción adecuada. En orden de
prioridad, éstas son las comprobaciones que se realizan para la selección:

1. Que el tipo capturado sea exactamente el tipo lanzado
2. Que el tipo capturado sea una clase base del tipo lanzado
3. Si ambos son son punteros, se aplican los casos 1 o 2 para los tipos a los
   que éstos hacen referencia.
4. Si el tipo capturado es una referencia, se aplican 1 o 2 para los tipos a
   los que la referencia se refiere.

Si los bloques `catch` estuvieran ordenados de modo que alguno no pudiera
recibir excepciones (p.ej. existe un bloque `catch(...)` con mayor prioridad)
el compilador puede darse cuenta y lanzar un error.

Ordenar los bloques de forma jerárquica no sólo impide ese tipo de errores de
compilación, sino que facilita la comprensión del programa y simplifica la ya
difícil tarea de gestionar casos de error.

### Emisión de excepciones

La expresión `throw` puede tomar dos formas:

- `throw` *expresión*
- `throw`

La primera construye un objeto-excepción a partir de la expresión indicada y lo
lanza para que pueda capturarse por un bloque `try-catch`.

La segunda, re-lanza la excepción actual para que pueda tratarse por el
siguiente bloque `catch` capaz de gestionar la excepción actual.

Normalmente la excepción se copia al lanzarse así que no es recomendable
incluir muchos datos en ella. De todas formas, diferentes optimizaciones pueden
aplicar a este caso que quedan en mano de la implementación.


### Gestión de recursos

El programa puede capturar recursos que debe liberar de forma adecuada (cerrar
archivos o conexiones abiertas, etc). Como la expresión `throw` corta la
ejecución del programa, éste puede perder la ocasión de liberar esos recursos.
Para esta labor otros lenguajes usan un bloque opcional adicional en las
sentencias `try` conocido como *finally*. Este bloque sería llamado
independientemente de si se captura o no se captura una excepción en el bloque
try, una vez éste y la gestión de todas sus posibles excepciones terminen. De
esta forma es posible liberar los recursos se hayan o no producido errores.

C++ no incluye bloque *finally* porque su diseño se basa en que cada objeto es
responsable de liberar sus recursos. C++ inventó un patrón de diseño con este
fin, conocido como RAII, *Resource Adquisition is Initialization*, que podría
traducirse como «adquirir recursos es inicializar». Este patrón de diseño
implica que cuando los objetos son eliminados (porque su *scope* termina, por
ejemplo) deben encargarse ellos mismos de limpiar los recursos mediante su
*destructor* haciendo innecesario introducir bloques *finally* en el lenguaje.

El concepto de los destructores se estudia más adelante, junto con las clases.

### Tipos de excepciones

Es posible lanzar cualquier tipo con una sentencia `throw`, incluso tipos
básicos. Se recomienda, sin embargo, lanzar tipos definidos de la forma más
local posible para evitar capturar excepciones inesperadas, por ejemplo
provenientes de librerías externas.

Como en ocasiones puede resultar tedioso declarar los tipos de todas las
excepciones posibles, `<stdexcept>` incluye distintos tipos de excepciones
básicas predefinidas para su uso en casos sencillos.

### Funciones sin excepciones

Es posible usar el especificador `noexcept` junto con el operador del mismo
nombre para indicar que una función no lanza excepciones o que sólo las lanza
en casos concretos.

El especificador `noexcept` facilita el uso de la gestión de excepciones,
permitiendo a quien programa no incluir las funciones `noexcept` en bloques
`try`. Esto sirve para reducir la carga mental que pueda conllevar el control
de errores, así que se recomienda marcar las funciones de forma adecuada.

Si una función se marca con `noexcept` indicando que no lanzará excepciones y
trata de lanzar una, se terminará la ejecución del programa automáticamente
resultando en un error de ejecución. Con el fin de evitar semejante situación,
es útil usar el `catch(...)` en su interior.



## Recapitulación

Este capítulo, aunque parezca largo y denso, no es más que el conjunto de
herramientas mínimas del lenguaje. Ya es evidente a estas alturas que existen
otras herramientas adicionales, como las clases, que son las que hacen que C++
sea el lenguaje que es.

Hasta este punto, se han descrito el subconjunto mínimo de sentencias y
expresiones de C++ que permiten escribir programas en C++, pero aún no se han
descrito los mecanismos abstractos en los que el lenguaje se sustenta para
aportar lo que se ha introducido aquí.

No debe suponer un problema, sin embargo, ya que con lo aquí estudiado se
adquiere un conocimiento general de muchos conceptos que se necesitarán más
adelante. 

Mucho del conocimiento de este apartado es, además, extrapolable al lenguaje C,
cosa que no se cumplirá a medida que el nivel de abstracción suba, así que es
base interesante para aprenderlo (tarea que no se puede dejar de recomendar).
En realidad, este apartado cubre casi todo el lenguaje C así que es material
más que suficiente para entender un lenguaje completo. Incluso algunos detalles
particulares descritos en este apartado no existen en C (las referencias, las
excepciones, las expresiones lambda y `bool`, por ejemplo).  Así que debe haber
cierto poso de satisfacción, llegados a este punto.

Dicho de otra forma, se ha analizado un lenguaje de programación completo
aunque de forma quizás poco gratificante porque no se ha enseñado a aplicar
lo aprendido. El próximo capítulo se encargará de eso.

Volviendo a la retrospectiva, y de cara a enfocar el estudio futuro, este
apartado puede dividirse conceptualmente en dos niveles:

El primero trata sobre características concretas del lenguaje que se deben
memorizar de forma literal, como pueden ser los tipos básicos o las sentencias
de control de flujo. Tampoco es recomendable estudiarlas en detalle a estas
alturas. La práctica hará que se afiancen en la memoria y, mientras se van
afianzando, siempre es posible acudir a este capítulo a consultar lo que
necesario.

El segundo, mucho más importante, trata de los diferentes elementos que pueden
encontrarse en un programa: sentencias, expresiones, literales, funciones,
punteros, *scope*... Todos estos conceptos son extremadamente importantes y
tenerlos claros facilitará la programación y el aprendizaje de nuevos
lenguajes. Por ejemplo, ser consciente de que una sentencia compuesta actúa
como cualquier sentencia pero que tiene un *scope* propio ayuda a comprender el
cuerpo de las funciones, de los `if`, `for`, etc.

Se recomienda tratar, en la medida de lo posible, de centrar la atención en el
segundo conjunto pues es el que traerá mayor beneficio en el estudio y
probablemente en la práctica futura, sea cual sea el lenguaje con el que se
trabaje.
