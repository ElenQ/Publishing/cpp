# Proyectos de software

El proceso de compilación se ha descrito en un apartado previo, pero con total
seguridad, ha generado cierta desilusión a la hora de empezar a trabajar con el
código ya que, a pesar de detallar el proceso de compilación, no aclara en
absoluto cómo aplicar ese conocimiento a nivel práctico.

Por su parte, aunque recién se hayan explicado las bases de la programación en
C++, también existe un nivel de desencanto debido a que no se ha aclarado cómo
se debe construir un proyecto de software avanzado.

Ambos puntos están muy relacionados.


## Compilación independiente

Como se describe en el apartado *[Compilación y Enlazado]*, C++ define que cada
archivo de C++ puede compilarse de forma independiente y luego combinarse con
otros archivos compilados para generar el ejecutable o librería final. Esto,
tal y como ya se dijo, es una ventaja porque facilita que el proyecto no deba
compilarse completamente para actualizar ficheros independientes y facilita la
modularización del código.

Todo identificador debe ser declarado antes de utilizarse por lo que la
compilación no puede ser tan independiente si se quieren compartir
identificadores de un archivo a otro.

En C se diseñó un mecanismo sencillo para resolver este problema, que ha sido
heredado en C++: se trata de separar las declaraciones a un archivo separado de
forma que puedan ser incluidas por cada archivo que va a compilarse de forma
independiente. Estos archivos de declaraciones se conocen como *header* files
(o ficheros de cabecera), y suelen marcarse con la extensión `.h`, aunque en
C++ también se utiliza la extensión `.hpp`, mientras que los *source files* (o
ficheros de código fuente) suelen marcarse con la extensión `.cpp`
[^extensions].

No existe ningún tipo de limitación de qué debe incluirse en cada tipo de
archivo, ambos son ficheros C++, con todas sus capacidades. Simplemente se
separa el código de esa manera para facilitar el trabajo. Si en algún momento
cambia la implementación de una función, por ejemplo, su declaración no tiene
por qué cambiar (aunque su definición sí que cambiará), por lo que el fichero
de cabecera se mantendrá inalterado. Esto permite que todos los archivos de
código que requieran la declaración de la función para compilarse no deban ser
recompilados, ya que únicamente cargan el archivo de cabecera.

[^extensions]: En C son comunes las extensiones `.h` para los ficheros de
  cabecera y `.c` para los de código, mientras que en C++ se usan
  principalmente `.hpp` y `.cpp` aunque también es común que usen las de
  C,`.hxx` y `.cxx` o incluso `.H` y `.C` (en mayúsculas). En ocasiones, como
  los archivos de cabecera suelen ser compatibles con ambos lenguajes, usan
  `.h` para C y C++ pero sí que se diferencian los archivos de funcionalidad
  mediante `.c` y `.cpp`.


### Inclusión de cabeceras

Tanto C como C++ usan el preprocesador para realizar las inclusiones de
archivos necesarias. La directiva `#include` es la encargada de realizar la
inclusión de los ficheros necesarios. Al encontrar una directiva `#include` el
preprocesador (etapa que se ejecuta antes de compilar), incluye de forma
literal el contenido del archivo de cabecera incluido en el archivo actual.
Este tipo de directivas trabajan sobre el texto del programa, así que no hacen
chequeos, simplemente insertan el texto.

Cuando se han realizado todas las directivas, el programa ya es código C++
puro y pasa a compilarse. A este archivo intermedio se le conoce como
*translation unit* (unidad de traducción) ya que es el fichero que se compilará
(o traducirá) realmente.

Cuando se habla de que la compilación es independiente, no se refiere a las
fuentes, sino a las unidades de traducción.

### Ejemplo

Teniendo un archivo `suma.cpp` con este contenido:

``` cpp
int suma3( int a, int b, int c ){ // Definición de la función
    return a + b + c;
}
```

Si quiere exponerse esta función a otro archivo sería necesario incluirla en él
directamente en su cuerpo, por lo que se rompería la programación
independiente.

El método adecuado para hacerlo es crear un fichero de cabecera asociado a
`suma.cpp` llamado `suma.hpp` con el siguiente contenido:

``` cpp
int suma3(int,int,int); // Declaración de la definición
```

De este modo, cuando se necesite la función suma desde otro archivo, bastará
con incluir su cabecera. Por ejemplo:

``` cpp
// Archivo `main.cpp`
#include "suma.hpp"

int main(){
    suma3(1,2,3);
}
```

Al compilar `main.cpp`, éste se preprocesará automáticamente, convirtiéndolo a
esta *translation unit*:

``` cpp
// Archivo `main.cpp`
int suma3(int,int,int); // Declaración de la función

int main(){
    suma3(1,2,3);
}
```

Este archivo ya no tiene directivas del preprocesador y conoce la declaración
de la función `suma3`, por lo que es capaz de usarla en su cuerpo. Finalmente,
éste es el archivo que se compila para generar el objeto, librería o ejecutable
correspondiente.

## Enlazado avanzado

Queda en manos de quien programa asegurarse de que las declaraciones en cada
*translation unit* sean adecuadas. Cada elemento sólo puede definirse en una
ocasión, pero declarado todas las veces necesarias, siempre que estas sean
coherentes.

Tal y como ya se ha visto, sentencias como la siguiente son definiciones, ya
que reservan memoria:

``` cpp
int a;         // Definición
```

Hasta ahora no se ha definido ninguna forma de declarar una variable sin
definirla, más allá de los argumentos de entrada de las funciones o los
selectores `catch`.

El especificador `extern` sirve para indicar que una variable ha sido definida
en otro lugar así que simplemente la declara.

``` cpp
extern int a;  // Declaración
```

Como se ha dicho, los identificadores sólo pueden definirse en una ocasión y
las declaraciones deben encajar, siendo coherentes con los tipos de su
definición. Sin embargo, si no lo son, el compilador no puede hacer nada, ya
que éste sólo trabaja sobre una *translation unit* y la definición se encuentra
en otra distinta. El *linker* sí que puede realizar algunas comprobaciones a la
hora de combinar los objetos ya compilados, pero no es infalible y no
funcionará en los casos más complejos. Además, esto depende de la
implementación en la que se esté trabajando, así que es más inteligente
trabajar con cuidado y no confiar plenamente en las herramientas.

`extern` se trata de un especificador especial, que es capaz de alterar el
enlazado tal y como se acaba de describir, pero no es el único. El
especificador `static` se encarga de lo opuesto: declara las variables como
enlazado interno, por lo que no se comparten con otras *translation units*.  Es
decir, `static`, en este contexto[^static], implica que la variable no se
comparte con otros archivos.

Las declaraciones `const`, `constexpr` e `inline` también afectan al enlazado,
provocando enlazado interno. Aunque algunas también pueden especificarse como
`extern` para cancelar este comportamiento.

Como las combinaciones son complejas, se recomienda realizar comprobaciones
siempre que se realicen enlazados complejos. Lo importante en este punto es
recordar que existen dos tipos de enlazado: externo e interno. Las definiciones
se activan como enlazado externo por defecto, haciendo que sean accesibles
desde otras *translation units*. En caso de que se quiera mantener una
definición como local, es posible usar `static`. En caso de que en lugar de
definir una variable en el *scope* global, quiera tomarse una definida en otro
fichero, debe usarse `extern`.

[^static]: Lamentablemente, en otros contextos la palabra `static` toma otros
  significados. Estos detalles que tanta confusión pueden generar son el
  resultado de una antigua herencia de C++ y, en juicio de quien escribe, de un
  mal diseño.


### Enlazado de programas en C

Es posible enlazar programas de C++ junto a otros escritos en otros lenguajes.
Al fin y al cabo, todos son lenguaje máquina. Aunque no es todo tan sencillo.
Cada lenguaje y su implementación tienen diferentes formas de llamar a
funciones, asignar sus recursos, etc. La cooperación puede ser compleja incluso
de un compilador a otro, en el mismo lenguaje.

Como C++ está muy relacionado con C y los compiladores de C++ son capaces de
compilar también código C, es posible pedirle al compilador que compile partes
específicas del programa desde C:

``` cpp
extern "C" {
    // Código C
}
```

> NOTA: La directiva `extern "C"` no tiene el mismo significado que `extern`.

La directiva `extern "C"` puede usarse para compilar cualquier pieza de código,
incluso para declarar funciones y variables. Ésta funcionará únicamente debido
a la estrecha relación entre C y C++. Este mecanismo es muy frecuente, sobre
todo en librerías compartidas entre C y C++. El siguiente ejemplo es una
versión simplificada y comentada de `<cstring>`, donde puede verse cómo se
define para que sea compatible para ambos lenguajes.

``` cpp
#ifdef __cplusplus  // Añade `extern "C"` sólo en caso de que el compilador
extern "C" {        // trabaje en C++. Ver sección Directivas del Preprocesador
#endif
    char* strcpy(char*, const char*);
    int strcmp(const char*, const char*);
    int strlen(const char*);
    // ...
#ifdef __cplusplus
}
#endif
```

El bloque `extern "C"`, que también puede ser aplicado a declaraciones únicas,
no altera el *scope* de lo que en él se declara, así que sigue estando
disponible en otros archivos, como si las llaves que lo encierra no existieran.

### Enlazado y punteros a funciones

Al tratar con punteros a funciones la cooperación entre C y C++ puede
complicarse.

Enviar un puntero de función C++ a una función escrita en C será problemático.
Pueden existir casos en los que la implementación interna sea idéntica y
funcionen, pero no se pueden asegurar. Los conceptos más abstractos como las
funciones lambda que hagan capturas son imposibles de transferir de C++ a C.


## Programas

Ya se conoce que el código fuente puede tener diferentes tipos de resultados,
librerías y programas, pero no se ha tratado cómo deben comportarse para
interactuar con el sistema en el que se ejecutan ni cómo deben compartirse para
que terceros puedan usarlos.

### Librerías

Como a la hora de compilar es necesario que los programas conozcan todos sus
identificadores, si se desean distribuir librerías es necesario distribuir los
archivos de cabecera con ellas. De este modo, aunque la fuente esté
pre-compilada en la librería correspondiente, el compilador podrá conocer qué
declaraciones dispone y así poder alertar de errores a quien esté haciendo uso
de la librería.

Además, los archivos de cabecera, usados correctamente, pueden servir como una
fuente de información de cómo usar la librería. Si las declaraciones son claras
y añaden comentarios que expliquen cómo funcionan, pueden ser más que
suficientes.

Las librerías pre-compiladas se distribuyen como librerías estáticas o
dinámicas, en función de lo que se desee. Para ello es necesario compilar el
programa con las opciones adecuadas, tal y como se describe en el capítulo
*[Compilación y Enlazado]*.

En los sistemas tipo UNIX, estos archivos de cabeceras se instalan normalmente
en `/include`, `/usr/include/` o `/usr/local/include`, mientras que el código
fuente pre-compilado de la librería suele almacenarse en `/lib`, `/usr/lib` o
`/usr/local/lib`, tanto para las librerías estáticas como dinámicas. De este
modo, cualquier programa que se quiera compilar (o ejecutar, en el caso de las
librerías dinámicas) en el sistema, dispone de librerías y cabeceras para
hacerlo correctamente.

Cada compilador dispondrá, por otro lado, un sistema para indicar dónde deben
buscarse tanto las cabeceras como las librerías. En `gcc` se utilizan las
opciones `-I` para lo primero y `-L` para lo segundo aunque también existen
variables de entorno que pueden usarse para tal fin.

### Programas ejecutables

En sistemas UNIX, los programas suelen instalarse en `/bin`, `/usr/bin`,
`/usr/local/bin` o similares. Al ejecutarlos, el sistema operativo los buscará
en la carpeta correspondiente, atendiendo a la variable de entorno `PATH`.
Aunque también es posible ejecutar programas en otras carpetas, si se
identifican usando su ruta.

A diferencia de las librerías, los programas ejecutables no necesitan compartir
sus archivos de cabecera ya que no están diseñados con el objetivo de unirse a
un programa mayor.

Es bastante acertado pensar que las librerías no son programas completos, sólo
piezas de ellos, que se combinan con un programa principal cuando sea
necesario.

Los programas completos se diferencian de las librerías de una forma sencilla:
los programas definen la función `main`.

> NOTA: Las implementaciones *freestanding* no requieren el uso de la función
> `main`, pero no puede decirse que sean programas ejecutables lo que se hace
> con ellas, ya que no existe un sistema operativo capaz de ejecutarlos. Otro
> nombre quizá más adecuado para este caso puede ser *firmware* o, en función
> del comportamiento del programa, sistema operativo.

Aunque ya se ha mencionado que la función `main` debe declarar que retorna
`int` aunque luego no se usen sentencias `return` en su cuerpo, la función
`main` también hace un uso peculiar de los argumentos de entrada. Ambos
mecanismos son importantes, ya que expresan la relación del programa con el
sistema operativo.

#### Argumentos de entrada

Normalmente la función `main` se declara del siguiente modo:

``` cpp
int main (int argc, char *argv[]){
    // CUERPO
}
```

Aunque en ocasiones los argumentos de entrada se obvian si no se utilizan.

Los argumentos de entrada `argc` y `argv`[^arg-convention] se llenan con los
argumentos de entrada que el programa recibe en tiempo de ejecución desde la
*shell* de sistema. `argc` indica el número de ellos y `argv` indica su valor,
siendo el primero de ellos el nombre del programa. La ejecución:

``` bash
$ programa argumento1 argumento2
```

Hará que `argc` tenga valor `3` y `argv` sean tres *string* estilo C (*array*s
de `char` terminados en `\0`) con los siguientes valores: `"programa"`,
`"argumento1"` y `"argumento2"`.

[^arg-convention]: Los nombres `argc` y `argv` son una convención, realmente
  puede usarse cualquiera, pero es costumbre que se utilicen éstos.


#### Terminación de programas

Existen varias formas de que la ejecución de un programa termine, algunas han
sido introducidas, como la violación de `noexcept`, o las excepciones no
controladas, aunque hay otras. Las principales son las siguientes:

1. La función `main` termina su ejecución.
2. Se llama a la función `exit()`.
3. Se llama a alguna otra forma de escape.
4. Se lanza una excepción no controlada.
5. Se lanza una excepción en una función marcada como `noexcept`.

Además de éstas, existen algunas otras dependientes de la implementación o del
propio programa, como las divisiones entre `0`, etc.

La función `exit()` es una forma controlada de abandonar la ejecución del
programa, por lo que tratará de realizar la limpieza correspondiente mediante
RAII (*Resource Adquisition Is Initialization*). El valor entregado a la
función `exit()`, que recibe un `int`, es el que servirá como valor del retorno
del programa.

Otras formas de escape, como la función `abort()` no liberan recursos.

La función `atexit()`, por su lado, permite ejecutar una función especial para
limpiar recursos. Su existencia viene de los tiempos de C, donde, al no tener
un mecanismo RAII, requiere de código adicional para realizar la liberación de
recursos.

Todas estas funciones de terminación están declaradas en `<cstdlib>`.


## Directivas del preprocesador

Ya se ha tratado la directiva `#include` y se han visto retazos de alguna otra
como `#ifdef`, pero las directivas del preprocesador son un mecanismo poderoso
y relativamente complejo. Se comportan como un lenguaje de programación,
distinto a C++, que se ejecuta por el compilador para construir las diferentes
*translation units*. Éste lenguaje sólo trabaja a nivel de texto, es decir,
transforma el contenido del programa que después se compila.

En C++ no es demasiado necesario usar directivas complejas como macros, ya que
las definiciones `constexpr` y `consteval` son código C++ capaz de ser evaluado
por el compilador en tiempo de compilación. La recomendación general es usar
las directivas de preprocesador únicamente para realizar compilación
condicional, y si es posible sólo en *include guards*.

Sin embargo, al no disponer de ese tipo de definiciones, C hace un uso avanzado
del preprocesador en incontables ocasiones. Es por esta razón que es importante
conocer el preprocesador, aunque sea de forma superficial.

Por otro lado, mecanismos avanzados de compilación como las herramientas de
compilación de GNU, CMake, etc. hacen uso del preprocesador para realizar
compilación condicional. Conocer el mecanismo permite revisar que el
funcionamiento de las herramientas sea el correcto que, sorprendentemente y
para nuestra desgracia, no siempre lo es.


### Macros

La macro más sencilla se trata de una sustitución de texto literal.

``` cpp
#define TEXTO texto de reemplazo
```

Si el preprocesador encuentra una directiva `#define` como esa, sustituirá
todas las veces que encuentre `TEXTO`[^macro-convention] por `texto de
reemplazo`, literalmente. Esto es muy habitual en C para definir constantes,
aunque en C++ puede usarse `constexpr` con el mismo fin.

``` c
#define PI 3.1416
float a = PI * 5;   // Resultará en `float a = 3.1416 * 5;` tras el
                    // preprocesado. PI no existe como variable, es un literal.
```

[^macro-convention]: Las macros se indican en mayúsculas por convención, para
  que sean fáciles de diferenciar de otros identificadores, que suelen
  declararse en minúsculas.

Las definiciones pueden recibir argumentos de entrada, que se aplicarán de
forma literal en la sustitución de texto.

``` cpp
#define SQUARE(x) x * x
```

Sin embargo, esta macro es peligrosa porque:

``` cpp
#define SQUARE(x) x * x

int a = SQUARE(1+2);  // resulta en `1+2*1+2`, es decir `5`
```

Por eso se recomienda envolver las macros en paréntesis siempre que sea
posible:

``` cpp
#define SQUARE(x) ((x) * (x))

int a = SQUARE(1+2);  // resulta en `((1+2)*(1+2))`, es decir `9`
```

Las macros no conocen sintaxis de C++, por lo que es posible que generen
sintaxis errónea. Incluso es posible generar un lenguaje adicional sobre C++
usando macros. Steven Bourne, autor de la shell conocida como Bourne Shell, de
la que después se crearía Bash (Bourne Again Shell), acostumbrado al lenguaje
Algol, se vio tan influenciado por éste tanto para el diseño de la shell como
para su implementación que decidió usar las siguientes macros, entre
otras[^bourne-shell]:

``` c
#define IF      if(
#define THEN    ){
#define ELSE    } else {
#define ELIF    } else if (
#define FI      ;}

#define BEGIN   {
#define END     }
#define SWITCH  switch(
#define IN      ){
#define ENDSW   }
#define FOR     for(
#define WHILE   while(
#define DO      ){
#define OD      ;}
#define REP     do{
#define PER     }while(
#define DONE    );
#define LOOP    for(;;){
#define POOL    }
```

Por lo que el código C de su implementación tenía el siguiente aspecto:

``` c
FOR m=2*j-1; m/=2;
DO  k=n-m;
    FOR j=0; j=0; i-=m
        DO  REG STRING *fromi; fromi = &from[i];
            IF cf(fromi[m],fromi[0])>0
            THEN break;
            ELSE STRING s; s=fromi[m]; fromi[m]=fromi[0]; fromi[0]=s;
            FI
        OD
    OD
OD
```

[^bourne-shell]: Código obtenido de la página The Unix Heritage Society.
  Consultado 9 de Junio de 2020:
  <https://www.tuhs.org/cgi-bin/utree.pl?file=V7/usr/src/cmd/sh/mac.h>

El preprocesador no es especialmente poderoso, por lo que no es posible
escribir macros recursivas y cualquier error provocado por macros resultará en
un mensaje de error extraño entregado por el compilador, probablemente difícil
de descifrar. El caso de la shell de Bourne, a pesar de ser absolutamente
genial, es un ejemplo de cómo no se debe programar si el objetivo es que otras
personas sean capaces de leer el programa y corregir sus posibles errores.

Como las macros tratan con texto literal es posible crear combinaciones de
texto literal en ellas, con el fin de que sea posible crear identificadores:

``` cpp
#define CREA_NOMBRE(a,b) a##b

int CREA_NOMBRE(num, init); // Resulta en: `int numinit;`
```

Las macros también pueden recibir cualquier número de argumentos como ocurre
con las funciones. Para ello se hace uso de la elipsis y del argumento
`__VA_ARGS__`. Por ejemplo:

``` cpp
#define eprintf(...) fprintf (stderr, __VA_ARGS__)
```

Al tratar con texto literal, esto puede ser problemático debido a las comas que
separan los argumentos en casos muy extremos. Compiladores como GCC disponen de
una herramienta para tratar las comas, pero no es estándar.

Es posible deshacer las definiciones de las macros, sobre todo con la intención
de proteger secciones del programa de macros indeseadas o con el fin de usar
las macros como variable. Esto se consigue mediante la directiva `#undef`.

``` cpp
#undef MACRO
```

A pesar de toda la complejidad disponible con la directiva `#define`, es
posible usarla simplemente para marcar un nombre como definido en tiempo de
preprocesado, sin añadir ningún tipo de conversión de texto. Por ejemplo:

``` cpp
#define USE_BIGNUM
```

Este tipo de definiciones, junto con `#undef` permiten la construcción de
estructuras más avanzadas de compilación condicional sin afectar al código
fuente de forma directa.


#### Macros por defecto

Algunas macros serán activadas por defecto por el compilador. Cada
implementación puede disponer de macros adicionales. Las principales, definidas
por el estándar son las siguientes.

- `__cplusplus` : define compilación C++, en lugar de C. Su valor es el del
  estándar actual[^__cplusplus]. Por ejemplo:
  - `199711L`: ISO C++ 1998/2003
  - `201103L`: ISO C++ 2011
- `__DATE__`: fecha de compilación en formato "yyyy:mm:dd".
- `__TIME__`: hora de compilación en formato "hh:mm:ss".
- `__FILE__`: nombre del archivo actual.
- `__LINE__`: línea actual.
- `__STDC_HOSTED__`: `1` si la implementación es *hosted* y `0` si es
  *freestanding*.

[^__cplusplus]: Algunos compiladores no cumplen el estándar así que asignan un
  valor como `1` en caso de que compilen C++ y `0` en caso de que no lo hagan.

Otras macros opcionales también están definidas por el estándar, como la
indicación de si el programa puede tener más de un hilo de ejecución. Se anima
a quien lee a investigarlas por su cuenta.

### Compilación condicional

Las directivas `#ifdef`, `#ifndef`, `#if`, `#endif`, `#else` y `#elif` son las
encargadas de la compilación condicional. Estas directivas, en caso de que su
condición se cumpla no actúan en el código, pero en caso contrario lo retiran
de la *translation unit*.

Las directivas `#ifdef` y `#ifndef` comprueban si una definición existe o no,
respectivamente. Ambas pueden tener su caso contrario definido mediante `#else`
y deben terminarse con `#endif`.

``` cpp
#ifdef USE_BIGNUM
// Código para cuando USE_BIGNUM está definido
#else
// Código para cuando USE_BIGNUM no está definido
#endif
```

El ejemplo es útil en casos, por ejemplo, donde pueda compilarse un programa
con diferentes opciones. El nombre usado en el ejemplo sugiere el caso de un
programa con soporte opcional para números grandes, como puede ser un programa
de cálculo o la implementación de un lenguaje de programación.

La directiva `#if` es algo más flexible, permitiendo la inclusión de una
condición evaluable en momento de compilación (`constexpr` o basada en macros).
Además, pueden crearse diferentes ramas usando `#elif`. Igual que con `#ifdef`,
la directiva `#if` debe terminarse con `#endif`.

``` cpp
#if MAX_COUNT > 100
// Código para cuando MAX_COUNT > 1000
#elif MAX_COUNT < 100 && MAX_COUNT > 10
// Código para cuando 10 < MAX_COUNT < 100
#else
// Código para el resto de casos
#endif
```

Un caso muy común del uso de la compilación condicional es el de la sección
*[Enlazado de programas en C]*:

``` cpp
#ifdef __cplusplus
extern "C" {
#endif

// Código C

#ifdef __cplusplus
}
#endif
```


### Inclusión de archivos

La directiva `#include`, como ya se ha explicado al comienzo de este capítulo,
sirve para incluir otros archivos de código directamente en el archivo actual.
Esta directiva puede tomar dos formas diferentes:

- `#include "archivo_a_incluir"`
- `#include <archivo_a_incluir>`

> NOTA: En las directivas `include` no es posible añadir espacios entre los
> separadores y el nombre del archivo a encontrar. `#include " archivo "` no es
> equivalente a `#include "archivo"`. Se recomienda extrema cautela con la
> sintaxis de las directivas del preprocesador, ya que su procesamiento no es
> tan potente como el del lenguaje, y sufren de estos detalles.

La primera de ellas, busca el archivo correspondiente desde el archivo actual
usando su ruta.

La segunda sirve para incluir archivos de librería estándar del lenguaje. Esta
segunda no requiere la inclusión de ninguna extensión, ya que se supone que la
propia implementación es consciente de dónde residen sus propias librerías
estándar. Además, en algunos casos, puede no significar que se incluya un
archivo literalmente, sino que active unas funcionalidades adicionales en el
compilador. Todo esto es indiferente para quien programa.

La librería estándar incluye además la librería estándar del lenguaje C, tal y
como se precisa en la sección *[Librería estándar de C]*.

#### *Include guards*

En programas grandes, es posible que la inclusión de archivos de cabeceras
lleve a incluir los mismos archivos en más de una ocasión, derivando en errores
debidos a la definición de el mismo identificador en más de una ocasión.
Además, aunque esto no sucediera, los tiempos de compilación podrían alargarse
por el hecho de crear *translation units* muy grandes, con secciones de
programa repetidas y que, por tanto, pueden obviarse.

Una alternativa es ordenar los archivos de cabecera de modo que no colisionen
entre ellos de ningún modo pero puede resultar en una tarea tediosa e incluso
imposible en proyectos demasiado grandes, o en casos en los que se usan
librerías externas, cuyo código y cabeceras no puede alterarse.

Una forma más sencilla de evitar el problema que esto conlleva es explotar las
macros y la compilación condicional del siguiente modo.

Suponiendo el archivo de cabecera `calculus.h`:

``` cpp
#ifndef CALCULUS_H
#define CALCULUS_H
// Declaraciones y definiciones
#endif // CALCULUS_H
```

Este modelo cargará la cabecera del archivo `calculus.h` sólo en caso de que la
definición `CALCULUS_H` no exista. Si existe será porque el `#define` ya ha
ocurrido, por lo que el cuerpo ha sido cargado con anterioridad, así que la
compilación condicional evitará volver a cargarlo.

Este modelo es muy común y recomendable en todos los archivos de cabeceras.

La directiva `#pragma once`, que no es estándar, pero es muy conocida y está
soportada en gran cantidad de implementaciones, realiza la misma tarea, pero
sólo requiere de una línea. Se recomienda no usar el mecanismo basado en
compilación condicional si el código pretende poder compilarse en cualquier
compilador estándar. Si el código está diseñado para un compilador que soporte
la directiva `#pragma once` y no se requiere que sea capaz de compilar en otra
plataforma, usarla no supondría un problema. De todos modos, identificar un
único archivo en el sistema es complicado, así que la directiva `#pragma once`
podría dar resultados inesperados en casos complejos (enlaces a archivos,
archivos repetidos, etc.) por lo que es mejor usarla sólo en caso de que el
proyecto en el que se trabaje esté bajo control.

### Errores

Es posible lanzar errores en tiempo de compilación mediante la directiva
`#error`. Lo más razonable que se puede hacer con esta directiva es lanzar un
error en casos particulares. Añadirla fuera de una condición producirá que el
programa no pueda compilarse.

``` cpp
#ifndef __cplusplus
#error C++ es obligatorio
#endif
```

### Control de línea

Es posible alterar los mensajes de error del compilador, indicando una línea y
un nombre de fichero especial. La directiva `#line` permite etiquetar una línea
con una posición y el resto de líneas del archivo se etiquetarán incrementando
el valor desde la directiva. Además, permite indicar un nombre de archivo
alternativo, alterando así los mensajes de error. No es una directiva muy
común.

``` cpp
#line 100 "Nombre de archivo"
a;
```

Compilar un archivo como el propuesto en el ejemplo no compilará, porque el
identificador `a` no se conoce. El error del compilador no indicará que existe
un error en la línea `2` del archivo, sino que lo marcará con la línea indicada
en la directiva `#line` y asignará el nombre del archivo a `Nombre de archivo`
en lugar del nombre que el archivo real tuviera. Ejecutando en GCC el error es
el siguiente:

```
Nombre de archivo:100:1: error: ‘a’ does not name a type
```

### Opciones del compilador

La directiva `#pragma` sirve para añadir opciones al compilador. Están
definidas por cada una de las implementaciones, así que debe comprobarse su
documentación específica.

Un ejemplo de uso de las directivas `#pragma` es el de la programación de
microcontroladores de la empresa Microchip (implementación *freestanding*)
mediante su compilador de C `xc8`.  En esta implementación, la directiva
`#pragma` sirve para configurar el estado del microcontrolador:

``` c
#pragma config WDT = ON      // Activar el watchdog timer
```

Por supuesto, esta información está disponible en el manual correspondiente.


## Espacios de nombres (*namespaces*)

C++, al igual que C, basa su modularización de código en cargar definiciones
descritas en otros archivos al archivo actual. Este proceso mezcla definiciones
de diferentes contextos en la unidad de traducción actual convirtiéndola en una
entidad completa y completamente definida. Este tipo de cargas de definiciones
externas pueden provocar colisiones de nombres, en casos que no se hayan
escogido nombres suficientemente concretos.

Normalmente, para evitar esta posibilidad, en C suelen añadirse prefijos a los
identificadores. Por ejemplo, desarrollando la librería `curl`, todos los
nombres que vayan a usarse externamente se comienzan con `curl_`[^curl],
consiguiendo así que no colisionen al cargarse con el código del programa
principal en el que se use la librería.

[^curl]: Se trata de un ejemplo real. Consultar: <https://curl.haxx.se/>

C++ introdujo los espacios de nombres, más conocidos por su nombre en inglés,
*namespace*, para evitar tener que aplicar este tipo de trucos. A parte de
esto, que ya es suficientemente valioso por sí mismo, los *namespaces* disponen
de reglas de *scope* propias y facilitan otras formas de ordenar el código de
un proyecto.

El siguiente ejemplo crea un nuevo *namespace* llamado `nombre`.

``` cpp
namespace nombre {
    // Declaraciones y definiciones
}
```

Para acceder a sus declaraciones o definiciones, es necesario usar `::`:
``` cpp
namespace numbers {
    int one = 1;
}

int a = numbers::one;
```

También es posible declarar nombres en el *namespace* y definirlos más
adelante, usando el identificador completo:

``` cpp
namespace func_collection{
    void func1(char*);
    void func2(char*);
    void func3(char*);
}

void func_collection::func1(char *str){
    // Cuerpo de la función
}
```

O incluso declarar el *namespace* en diferentes bloques, que finalmente se
unirán en uno solo.

``` cpp
namespace func_collection {
    void fun1(char*){ /*...*/ }
}

/*...*/

namespace func_collection {
    void fun2(char*){ /*...*/ }   // Se añade a `func_collection`
}
```

Pero no es posible declarar un nuevo miembro fuera del *namespace*, ni siquiera
usando el identificador completo.

Un mecanismo tan sencillo como éste está presente en todo el lenguaje, porque
facilita la modularización de código y el control del *scope*. En el apartado
sobre *[Scope]* se introduce el uso de `::` para acceder al *scope* global sin
hablar de *namespaces*, pero, en realidad, al usar `::identificador` se accede
al *namespace* global, que es donde se almacenan las variables globales. Las
clases, que se visitarán próximamente, también son *namespaces*.


### *using*

Con el fin de simplificar la escritura de los identificadores que pertenecen a
un espacio de nombres, C++ habilita la declaración (*using-declaration*) y la
directiva (*using-directive*) `using` («usando» en inglés). Éstas permiten
llevar un nombre declarado en un espacio de nombres al espacio de nombres
actual. A pesar de su nombre, ambas son técnicamente declaraciones, pero no se
comportan del mismo modo.

Rescatando el ejemplo propuesto en la sección *[Un programa sencillo]*:

``` cpp
#include <iostream>

int main(int argc, char* argv[]) {
    using namespace std;
    cout << "Hello World!\n";
    return 0;
}
```

El ejemplo muestra el uso de la directiva `using namespace` indicando `std`
como *namespace* a utilizar. Esta acción permite acceder a todas las
definiciones del *namespace* `std` (de estándar) usando su nombre sencillo.

De retirar la directiva, el ejemplo quedaría del siguiente modo:

``` cpp
#include <iostream>

int main(int argc, char* argv[]) {
    std::cout << "Hello World!\n";
    return 0;
}
```

En lugar de usar la directiva `using`, con el fin de evitar colisiones, se
recomienda usar la declaración `using`, que arrastra el identificador concreto
indicado al *scope* donde la llamada se encuentre.

``` cpp
#include <iostream>

int main(int argc, char* argv[]) {
    using std::cout;
    cout << "Hello World!\n";
    return 0;
}
```

En este nuevo ejemplo, sólo se simplifica el acceso a `std::cout`, reduciendo
las probabilidades de una colisión con el resto de los nombres que, por otro
lado, no se necesitan.

### *Namespaces* sin nombre

Es posible declarar *namespaces* sin nombre, de modo que quedan aislados del
resto del programa y no existe nombre con el que acceder a sus miembros.

``` cpp
namespace {
    // declaraciones
}
```

Para que la declaración del *namespace* no quede enorme, llena de definiciones,
los *namespaces* anónimos se activan automáticamente, como si incluyeran una
directiva `using` implícita. El ejemplo previo es equivalente a:

``` cpp
namespace X {
    // declaraciones
}
using X;
```

Con la única diferencia que el identificador `X` no puede usarse en ningún otro
lugar (porque no existe).

### Búsqueda de funciones y *namespaces*

El apartado sobre *[Sobrecarga de funciones]* aclara cómo funciona el mecanismo
de búsqueda de funciones en C++, pero no trata el caso de los *namespaces*.
Como los tipos habitualmente se declaran junto a sus funciones asociadas en el
mismo espacio de nombres, al traer un tipo al *namespace* actual mediante una
declaración `using`, sería casi obligatorio añadir todas las funciones
asociadas a la declaración. En C++ no es necesario hacer esto, ya que el
sistema de búsqueda de funciones busca de forma automática en los *namespaces*
de los argumentos de las llamadas a funciones en busca de la función a
ejecutar. De esta forma, las declaraciones `using` se simplifican mucho y el
*scope* actual sigue sin ensuciarse con el resto de declaraciones del
*namespace*.

Este funcionamiento se conoce como ADL (*Argument Dependent Lookup*).

``` cpp
namespace A {
    struct Char{
        int line;
        int pos;
        char value;
    };

    void error(Char c, const char *message){
        /*...*/
    }
}

using A::Char;

Char a {0,0,'a'};
error(a, "Error ocurred"); // llama a A::error
```

En casos complejos, puede ser sorprendente la selección realizada por el
compilador, pero es necesario recordar que siempre escogerá la función más
adecuada según sus criterios, recogidos en detalle en el estándar.


