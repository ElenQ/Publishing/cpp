template<typename T>
class Smart{
    T ptr;

    public:
    Smart(T);
    ~Smart();
    void operator=(T);
};

template<typename T>
Smart<T>::Smart(T p) : ptr{p} {
}

template<typename T>
Smart<T>::~Smart(){
    delete ptr;
}

template<typename T>
void Smart<T>::operator=(T p){
    delete ptr;
    ptr = p;
}

int main(){
    int* puntero = new int;
    Smart<int*> p (puntero);

    p = new int;
    p = new int;
    p = new int;
    p = new int;
    p = new int;
    p = new int;
    p = new int;
}
