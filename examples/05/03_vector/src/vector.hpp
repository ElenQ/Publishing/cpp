#ifndef VECTOR_H
#define VECTOR_H

template<typename T>
class vector{
    T* d;
    size_t l;

    public:
    vector(size_t size);
    ~vector();

    T& operator[](size_t pos);
    bool operator=(const vector& v);

    void push(T el);
};


template<typename T>
vector<T>::vector(size_t size){ 
    d = new T[size];
    l = size;
}

template<typename T>
vector<T>::~vector(){
    delete[] d;
}

template<typename T>
void vector<T>::push(T el){
    l++;
    T* x = new T[l];
    for(size_t i=0; i != l; i++){
        x[i] = d[i];
    }

    x[l-1] = el;

    delete[] d;
    d = x;
}

template<typename T>
T& vector<T>::operator[](size_t pos){
    if(pos > l-1){
        throw "NOPE";
    }
    return d[pos];
}

#endif
