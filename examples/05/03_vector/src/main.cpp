#include<iostream>
#include<string>
#include"vector.hpp"
using namespace std;
int main(){
    vector<float> v{10};

    v[0] = 1.0212112134;

    cout << v[0] << endl;
    cout << v[1] << endl;

    v.push(1212.1);

    cout << v[10] << endl;

    vector<int> v2{10};
    v2[0] = 1;
    cout << v2[0] << endl;


    vector<bool> v3{1};
    v3[0] = false;
    cout << v3[0] << endl;

    vector<std::string> v4{1};
    v4[0] = std::string("Hola");

    cout << v4[0];

    vector<string> v5 = v4;
}
