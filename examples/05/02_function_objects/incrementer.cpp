#include<iostream>
using namespace::std;

class Incrementer{
    int state;
    public:
    Incrementer(): state{1}{}
    int operator()(int x){
        return x + state++;
    }
};


int main(){
    Incrementer increment;
    cout << increment(1) << endl;
    cout << increment(1) << endl;
    cout << increment(1) << endl;
    cout << increment(1) << endl;
    cout << increment(1) << endl;
    cout << increment(1) << endl;
    cout << increment(1) << endl;
}
